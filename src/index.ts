import express from 'express';
import logger from 'morgan';
import { Constant } from './modules/shared/config/Constant';
import morgan from 'morgan';
import helmet from 'helmet';
import App from './App';

const app = new App({
    port: parseInt(Constant.PORT || '3550'),
    controllers: [],
    middleWares: [express.json(), morgan('combined'), helmet(), logger('development')]
});

app.listen();
