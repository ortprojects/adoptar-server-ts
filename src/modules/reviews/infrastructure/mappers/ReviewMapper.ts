import { UniqueEntityID } from '../../../shared/domain/valueObjects/UniqueEntityID';
import { BaseMapper } from '../../../shared/infrastructure/mappers/BaseMapper';
import { UserMapper } from '../../../users/infrastructure/mappers/UserMapper';
import { ReviewEntity } from '../../domain/ReviewEntity';
import { ReviewText } from '../../domain/valueObjects/ReviewText';
import { ReviewResponseMapper } from './ReviewResponseMapper';

export class ReviewMapper implements BaseMapper<ReviewEntity> {
    public static toPersistence(review: ReviewEntity): any {
        let rawResponse = {};

        if (review.props.reviewResponse) {
            rawResponse = ReviewResponseMapper.toPersistence(review.props.reviewResponse);
        }

        const raw = {
            id: review.id.toString(),
            createdByUserId: review.createdByUserId.toString(),
            text: review.text.value,
            userId: review.userId.toString(),
            createdAt: review.createdAt,
            response: rawResponse
        };

        return raw;
    }

    public static toDomain(raw: any, id?: string): ReviewEntity | null {
        const textOrError = ReviewText.create(raw.text);
        const createdByUserIdError = new UniqueEntityID(raw.createdByUserId);
        const userIdOrError = new UniqueEntityID(raw.userId);
        const createdAt = raw.createdAt.toDate();
        const response = raw.response;
        const createdBy = raw.createdBy;

        const reviewOrError = ReviewEntity.create(
            {
                text: textOrError.getValue(),
                createdByUserId: createdByUserIdError,
                createdAt,
                userId: userIdOrError,
                reviewResponse: response,
                createdBy
            },
            new UniqueEntityID(id)
        );

        return reviewOrError.isSuccess ? reviewOrError.getValue() : null;
    }

    public static toJSON(raw: ReviewEntity): any {
        let rawUser = {};
        let rawResponse = {};

        if (raw.props.createdBy) {
            rawUser = UserMapper.toJSON(raw.props.createdBy);
        }

        if (raw.props.reviewResponse.id) {
            rawResponse = ReviewResponseMapper.toJSON(raw.props.reviewResponse);
        }

        return {
            id: raw.id.toString(),
            text: raw.text.value,
            userId: raw.userId.toString(),
            response: rawResponse,
            createdBy: rawUser
        };
    }
}
