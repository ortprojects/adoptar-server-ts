import { UniqueEntityID } from '../../../shared/domain/valueObjects/UniqueEntityID';
import { BaseMapper } from '../../../shared/infrastructure/mappers/BaseMapper';
import { UserMapper } from '../../../users/infrastructure/mappers/UserMapper';
import { ReviewResponseEntity } from '../../domain/ReviewResponseEntity';
import { ReviewResponseText } from '../../domain/valueObjects/ReviewResponseText';

export class ReviewResponseMapper implements BaseMapper<ReviewResponseEntity> {
    public static toPersistence(review: ReviewResponseEntity): any {
        const raw = {
            id: review.id.toString(),
            text: review.text.value,
            createdAt: review.createdAt
        };

        return raw;
    }

    public static toDomain(raw: any, id?: string): ReviewResponseEntity | null {
        const textOrError = ReviewResponseText.create(raw.text);
        const createdAt = raw.createdAt.toDate();

        const reviewOrError = ReviewResponseEntity.create(
            {
                text: textOrError.getValue(),
                createdAt,
            },
            new UniqueEntityID(id)
        );

        return reviewOrError.isSuccess ? reviewOrError.getValue() : null;
    }

    public static toJSON(raw: ReviewResponseEntity): any {
        let rawUser = {};

        if (raw.props.createdBy) {
            rawUser = UserMapper.toJSON(raw.props.createdBy);
        }

        return {
            id: raw.id.toString(),
            text: raw.text.value,
            createdBy: rawUser
        };
    }
}
