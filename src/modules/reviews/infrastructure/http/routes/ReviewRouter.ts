import { Request, Response } from 'express';
import express from 'express';
import { RouterPath } from '../../../../shared/infrastructure/http/models/RouterPath';
import { LoggerImpl } from '../../../../shared/infrastructure';
import { createReviewContoller } from '../../../application/createReview';
import { getReviewsByUserIdContoller } from '../../../application/getReviewsByUserId';
import { createReviewResponseContoller } from '../../../application/createReviewResponse';

export class ReviewRouter {
    private static readonly routePath = '/reviews';

    public static create(): RouterPath {
        LoggerImpl.writeInfoLog('[ReviewRoute::create] Created review routes.');

        const reviewRouter = express.Router();

        reviewRouter.post('/', (req: Request, res: Response) => {
            createReviewContoller.execute(req, res);
        });

        reviewRouter.get('/:userId', (req: Request, res: Response) => {
            getReviewsByUserIdContoller.execute(req, res);
        });

        reviewRouter.post('/:userId/:reviewId/response', (req: Request, res: Response) => {
            createReviewResponseContoller.execute(req, res);
        });

        return { router: reviewRouter, path: this.routePath };
    }
}
