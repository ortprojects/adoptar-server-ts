import { FirebaseService } from '../../../shared/core/firebase/FirebaseService';
import { ReviewRepositoryImpl } from './ReviewRepositoryImpl';
import { ReviewRepository } from './ReviewRepository';

const firestore = FirebaseService.getFirestoreReference();

export const reviewRepositoryImpl: ReviewRepository = new ReviewRepositoryImpl(firestore);
