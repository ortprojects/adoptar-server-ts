import { userRepositoryImpl } from '../../../users/infrastructure/repositories';
import { ReviewEntity } from '../../domain/ReviewEntity';
import { ReviewMapper } from '../mappers/ReviewMapper';
import { ReviewResponseMapper } from '../mappers/ReviewResponseMapper';
import { ReviewRepository } from './ReviewRepository';

export class ReviewRepositoryImpl implements ReviewRepository {
    private firestore: FirebaseFirestore.Firestore;
    private reviewCollectionPath = '/userReviews';

    constructor(firestore: FirebaseFirestore.Firestore) {
        this.firestore = firestore;
    }

    async updateReview(review: ReviewEntity): Promise<boolean> {
        const reviewCollection = this.firestore.collection(this.reviewCollectionPath);

        await reviewCollection
            .doc(review.userId.toString())
            .collection('/reviews')
            .doc(review.id.toString())
            .update(ReviewMapper.toPersistence(review));

        return true;
    }

    async getReviewById(userId: string, reviewId: string): Promise<ReviewEntity> {
        const reviewCollection = this.firestore.collection(this.reviewCollectionPath);

        const review = await reviewCollection
            .doc(userId)
            .collection('/reviews')
            .doc(reviewId)
            .get();

        return ReviewMapper.toDomain(review.data(), review.id);
    }

    async getReviewsByUserId(userId: string): Promise<ReviewEntity[]> {
        const reviewCollection = this.firestore
            .collection(this.reviewCollectionPath)
            .doc(userId)
            .collection('reviews');

        const reviews = await reviewCollection.get();
        const reviewsRet: ReviewEntity[] = [];

        for (const doc of reviews.docs) {
            const user = await userRepositoryImpl.getUserByUserId(doc.data().createdByUserId);

            let response = {};
            if (doc.data().response.text) {
                response = ReviewResponseMapper.toDomain({
                    ...doc.data().response
                });
            }

            const reviewEntity = ReviewMapper.toDomain(
                { ...doc.data(), createdBy: user, response },
                doc.id
            );

            if (reviewEntity != null) {
                reviewsRet.push(reviewEntity);
            }
        }

        return reviewsRet;
    }

    async addReview(review: ReviewEntity): Promise<boolean> {
        const reviewCollectionPath = this.firestore.collection(this.reviewCollectionPath);

        await reviewCollectionPath
            .doc(review.userId.toString())
            .collection('/reviews')
            .doc(review.id.toString())
            .set(ReviewMapper.toPersistence(review));

        return true;
    }
}
