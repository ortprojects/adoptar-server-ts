import { ReviewEntity } from '../../domain/ReviewEntity';

export interface ReviewRepository {
    addReview(review: ReviewEntity): Promise<boolean>;
    getReviewsByUserId(userId: string): Promise<ReviewEntity[]>;
    getReviewById(userId: string, reviewId: string): Promise<ReviewEntity>;
    updateReview(review: ReviewEntity): Promise<boolean>;
}
