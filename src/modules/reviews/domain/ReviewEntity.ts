import { AggregateRoot } from '../../shared/domain/AggregateRoot';
import { UniqueEntityID } from '../../shared/domain/valueObjects/UniqueEntityID';
import { Result } from '../../shared/core/Result';
import { Guard } from '../../shared/core/Guard';
import { ReviewText } from './valueObjects/ReviewText';
import { UserEntity } from '../../users/domain/UserEntity';
import { ReviewResponseEntity } from './ReviewResponseEntity';

interface ReviewEntityProps {
    text: ReviewText;
    createdByUserId: UniqueEntityID;
    createdAt: Date;
    userId: UniqueEntityID;
    createdBy?: UserEntity;
    reviewResponse?: ReviewResponseEntity;
}

export class ReviewEntity extends AggregateRoot<ReviewEntityProps> {
    get id(): UniqueEntityID {
        return this._id;
    }

    get text(): ReviewText {
        return this.props.text;
    }

    get createdByUserId(): UniqueEntityID {
        return this.props.createdByUserId;
    }

    get createdAt(): Date {
        return this.props.createdAt;
    }

    get createdBy(): UserEntity {
        return this.props.createdBy;
    }

    get userId(): UniqueEntityID {
        return this.props.userId;
    }

    get reviewResponse(): ReviewResponseEntity {
        return this.props.reviewResponse;
    }

    private constructor(props: ReviewEntityProps, id?: UniqueEntityID) {
        super(props, id);
    }

    public static create(props: ReviewEntityProps, id?: UniqueEntityID): Result<ReviewEntity> {
        const guardedProps = [
            { argument: props.text, argumentName: 'text' },
            { argument: props.createdByUserId, argumentName: 'createdByUserId' },
            { argument: props.userId, argumentName: 'userId' },
        ];

        const guardResult = Guard.againstNullOrUndefinedBulk(guardedProps);

        if (!guardResult.succeeded) {
            return Result.fail<ReviewEntity>(guardResult.message);
        } else {
            const question = new ReviewEntity(
                {
                    ...props,
                    text: props.text,
                    createdByUserId: props.createdByUserId,
                    createdAt: props.createdAt,
                    userId: props.userId,
                    ...(props.reviewResponse ? { answer: props.reviewResponse } : {})
                },
                id
            );

            return Result.ok<ReviewEntity>(question);
        }
    }

    public addResponse(response: ReviewResponseEntity){
        this.props.reviewResponse = response;
    }
}
