import { Guard } from "../../../shared/core/Guard";
import { Result } from "../../../shared/core/Result";
import { ValueObject } from "../../../shared/domain/ValueObject";


interface ReviewResponseProps {
    value: string;
}

export class ReviewResponseText extends ValueObject<ReviewResponseProps> {
    get value(): string {
        return this.props.value;
    }

    private constructor(props: ReviewResponseProps) {
        super(props);
    }

    public static create(reviewResponseText: string): Result<ReviewResponseText> {
        const guardResult = Guard.againstNullOrUndefined(reviewResponseText, 'reviewResponseText');

        if (!guardResult.succeeded) {
            return Result.fail<ReviewResponseText>(guardResult.message);
        } else {
            return Result.ok<ReviewResponseText>(new ReviewResponseText({ value: reviewResponseText }));
        }
    }
}
