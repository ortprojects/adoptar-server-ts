import { Guard } from "../../../shared/core/Guard";
import { Result } from "../../../shared/core/Result";
import { ValueObject } from "../../../shared/domain/ValueObject";


interface ReviewTextProps {
    value: string;
}

export class ReviewText extends ValueObject<ReviewTextProps> {
    get value(): string {
        return this.props.value;
    }

    private constructor(props: ReviewTextProps) {
        super(props);
    }

    public static create(reviewText: string): Result<ReviewText> {
        const guardResult = Guard.againstNullOrUndefined(reviewText, 'reviewText');

        if (!guardResult.succeeded) {
            return Result.fail<ReviewText>(guardResult.message);
        } else {
            return Result.ok<ReviewText>(new ReviewText({ value: reviewText }));
        }
    }
}
