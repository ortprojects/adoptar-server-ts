import { AggregateRoot } from '../../shared/domain/AggregateRoot';
import { UniqueEntityID } from '../../shared/domain/valueObjects/UniqueEntityID';
import { Result } from '../../shared/core/Result';
import { Guard } from '../../shared/core/Guard';
import { ReviewText } from './valueObjects/ReviewText';
import { UserEntity } from '../../users/domain/UserEntity';
import { ReviewResponseText } from './valueObjects/ReviewResponseText';

interface ReviewResponseEntityProps {
    text: ReviewResponseText;
    createdAt: Date;
    createdBy?: UserEntity;
}

export class ReviewResponseEntity extends AggregateRoot<ReviewResponseEntityProps> {
    get id(): UniqueEntityID {
        return this._id;
    }

    get text(): ReviewResponseText {
        return this.props.text;
    }

    get createdAt(): Date {
        return this.props.createdAt;
    }

    get createdBy(): UserEntity {
        return this.props.createdBy;
    }

    private constructor(props: ReviewResponseEntityProps, id?: UniqueEntityID) {
        super(props, id);
    }

    public static create(props: ReviewResponseEntityProps, id?: UniqueEntityID): Result<ReviewResponseEntity> {
        const guardedProps = [
            { argument: props.text, argumentName: 'text' },
        ];

        const guardResult = Guard.againstNullOrUndefinedBulk(guardedProps);

        if (!guardResult.succeeded) {
            return Result.fail<ReviewResponseEntity>(guardResult.message);
        } else {
            const question = new ReviewResponseEntity(
                {
                    ...props,
                    text: props.text,
                    createdAt: props.createdAt,
                },
                id
            );

            return Result.ok<ReviewResponseEntity>(question);
        }
    }
}
