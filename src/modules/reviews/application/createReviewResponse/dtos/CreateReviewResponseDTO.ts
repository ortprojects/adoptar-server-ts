export interface CreateReviewResponseDTO{
    reviewId: string;
    createdByUserId: string;
    text: string;
    userId: string;
}