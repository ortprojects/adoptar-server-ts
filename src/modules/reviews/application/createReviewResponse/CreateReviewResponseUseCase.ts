import { GenericAppError } from '../../../shared/core/AppError';
import { Either, left, Result, right } from '../../../shared/core/Result';
import { UseCase } from '../../../shared/domain/UseCase';
import { UniqueEntityID } from '../../../shared/domain/valueObjects/UniqueEntityID';
import { UserRepository } from '../../../users/infrastructure/repositories/UserRepository';
import { ReviewResponseEntity } from '../../domain/ReviewResponseEntity';
import { ReviewResponseText } from '../../domain/valueObjects/ReviewResponseText';
import { ReviewText } from '../../domain/valueObjects/ReviewText';
import { ReviewRepository } from '../../infrastructure/repositories/ReviewRepository';
import { CreateReviewResponseErrors } from './CreateReviewResponseErrors';
import { CreateReviewResponseDTO } from './dtos/CreateReviewResponseDTO';

type Response = Either<
    | GenericAppError.UnexpectedError
    | CreateReviewResponseErrors.ReviewIdNotFound
    | CreateReviewResponseErrors.CreatedByUserIdNotFound
    | Result<any>,
    Result<void>
>;

export class CreateReviewResponseUseCase
    implements UseCase<CreateReviewResponseDTO, Promise<Response>> {
    private reviewRepositoy: ReviewRepository;
    private userRepository: UserRepository;

    constructor(reviewRepositoy: ReviewRepository, userRepository: UserRepository) {
        this.reviewRepositoy = reviewRepositoy;
        this.userRepository = userRepository;
    }

    async execute(request: CreateReviewResponseDTO): Promise<Response> {
        let reviewEntity;

        try {
            reviewEntity = await this.reviewRepositoy.getReviewById(request.userId, request.reviewId);
        } catch (error) {
            return left(new CreateReviewResponseErrors.ReviewIdNotFound()) as Response;
        }

        try {
            await this.userRepository.getUserByUserId(request.createdByUserId);
        } catch (error) {
            return left(new CreateReviewResponseErrors.CreatedByUserIdNotFound()) as Response;
        }

        const textOrError = ReviewResponseText.create(request.text);
        const createdAt = new Date();

        const combinedPropsResult = Result.combine([textOrError]);

        if (combinedPropsResult.isFailure) {
            return left(Result.fail<void>(combinedPropsResult.error)) as Response;
        }

        const reviewResponseOrError = ReviewResponseEntity.create({
            text: textOrError.getValue(),
            createdAt,
        });


        if (reviewResponseOrError.isFailure) {
            return left(Result.fail<ReviewResponseEntity>(reviewResponseOrError.error.toString())) as Response;
        }

        try {
            const reviewResponseEntity = reviewResponseOrError.getValue();

            reviewEntity.addResponse(reviewResponseEntity)
            
            await this.reviewRepositoy.updateReview(reviewEntity);

            return right(Result.ok<void>());
        } catch (err) {
            return left(new GenericAppError.UnexpectedError(err.message)) as Response;
        }
    }
}
