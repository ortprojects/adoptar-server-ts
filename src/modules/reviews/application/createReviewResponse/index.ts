import { userRepositoryImpl } from '../../../users/infrastructure/repositories';
import { reviewRepositoryImpl } from '../../infrastructure/repositories';
import { CreateReviewResponseController } from './CreateReviewResponseController';
import { CreateReviewResponseUseCase } from './CreateReviewResponseUseCase';

const createReviewResponseUseCase = new CreateReviewResponseUseCase(reviewRepositoryImpl, userRepositoryImpl);
const createReviewResponseContoller = new CreateReviewResponseController(createReviewResponseUseCase);

export { createReviewResponseUseCase, createReviewResponseContoller };
