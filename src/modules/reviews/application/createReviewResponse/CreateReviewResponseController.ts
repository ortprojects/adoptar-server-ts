import { BaseController } from '../../../shared/infrastructure/http/models/BaseController';
import { CreateReviewResponseErrors } from './CreateReviewResponseErrors';
import { CreateReviewResponseUseCase } from './CreateReviewResponseUseCase';
import { CreateReviewResponseDTO } from './dtos/CreateReviewResponseDTO';

export class CreateReviewResponseController extends BaseController {
    private useCase: CreateReviewResponseUseCase;

    constructor(useCase: CreateReviewResponseUseCase) {
        super();
        this.useCase = useCase;
    }

    async executeImpl(): Promise<any> {
        const dto: CreateReviewResponseDTO = this.req.body as CreateReviewResponseDTO;
        const { userId, reviewId } = this.req.params;

        (dto.reviewId = reviewId), (dto.userId = userId);

        try {
            const result = await this.useCase.execute(dto);

            if (result.isLeft()) {
                const error = result.value;

                switch (error.constructor) {
                    case CreateReviewResponseErrors.CreatedByUserIdNotFound:
                        return this.conflict(error.errorValue().message);
                    case CreateReviewResponseErrors.ReviewIdNotFound:
                        return this.conflict(error.errorValue().message);
                    default:
                        return this.fail(error.errorValue().message);
                }
            } else {
                return this.created(this.res);
            }
        } catch (err) {
            return this.fail(err);
        }
    }
}
