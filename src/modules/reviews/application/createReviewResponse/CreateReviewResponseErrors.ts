import { Result } from "../../../shared/core/Result";
import { UseCaseError } from "../../../shared/core/UseCaseError";

export namespace CreateReviewResponseErrors {
    export class CreatedByUserIdNotFound extends Result<UseCaseError> {
        constructor() {
            super(false, {
                message: `User who is creating not found`
            } as UseCaseError);
        }
    }

    export class ReviewIdNotFound extends Result<UseCaseError> {
        constructor() {
            super(false, {
                message: `Review not found`
            } as UseCaseError);
        }
    }
}
