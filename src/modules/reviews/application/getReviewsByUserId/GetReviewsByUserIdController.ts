import { BaseController } from '../../../shared/infrastructure/http/models/BaseController';
import { ReviewEntity } from '../../domain/ReviewEntity';
import { ReviewMapper } from '../../infrastructure/mappers/ReviewMapper';
import { GetReviewByUserIdDTO } from './dtos/GetReviewByUserIdDTO';
import { GetReviewsByUserIdErrors } from './GetReviewsByUserIdErrors';
import { GetReviewsByUserIdUseCase } from './GetReviewsByUserIdUseCase';

export class GetReviewsByUserIdController extends BaseController {
    private useCase: GetReviewsByUserIdUseCase;

    constructor(useCase: GetReviewsByUserIdUseCase) {
        super();
        this.useCase = useCase;
    }

    async executeImpl(): Promise<any> {
        const { userId } = this.req.params;

        const dto: GetReviewByUserIdDTO = {
            userId
        };

        try {
            const result = await this.useCase.execute(dto);

            if (result.isLeft()) {
                const error = result.value;

                switch (error.constructor) {
                    case GetReviewsByUserIdErrors.UserIdNotFound:
                        return this.conflict(error.errorValue().message);
                    default:
                        return this.fail(error.errorValue().message);
                }
            } else {
                const reviews = result.value.getValue();

                return this.ok<ReviewEntity[]>(
                    this.res,
                    reviews.map((review) => ReviewMapper.toJSON(review))
                );
            }
        } catch (err) {
            return this.fail(err);
        }
    }
}
