import { userRepositoryImpl } from '../../../users/infrastructure/repositories';
import { reviewRepositoryImpl } from '../../infrastructure/repositories';
import { GetReviewsByUserIdController } from './GetReviewsByUserIdController';
import { GetReviewsByUserIdUseCase } from './GetReviewsByUserIdUseCase';

const getReviewsByUserIdUseCase = new GetReviewsByUserIdUseCase(
    userRepositoryImpl,
    reviewRepositoryImpl
);
const getReviewsByUserIdContoller = new GetReviewsByUserIdController(getReviewsByUserIdUseCase);

export { getReviewsByUserIdUseCase, getReviewsByUserIdContoller };
