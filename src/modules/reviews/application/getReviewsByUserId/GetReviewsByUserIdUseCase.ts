import { GenericAppError } from '../../../shared/core/AppError';
import { Either, left, Result, right } from '../../../shared/core/Result';
import { UseCase } from '../../../shared/domain/UseCase';
import { UserRepository } from '../../../users/infrastructure/repositories/UserRepository';
import { ReviewEntity } from '../../domain/ReviewEntity';
import { ReviewRepository } from '../../infrastructure/repositories/ReviewRepository';
import { GetReviewByUserIdDTO } from './dtos/GetReviewByUserIdDTO';
import { GetReviewsByUserIdErrors } from './GetReviewsByUserIdErrors';

type Response = Either<
    GetReviewsByUserIdErrors.UserIdNotFound | GenericAppError.UnexpectedError,
    Result<ReviewEntity[]>
>;

export class GetReviewsByUserIdUseCase implements UseCase<GetReviewByUserIdDTO, Promise<Response>> {
    private userRepositoy: UserRepository;
    private reviewRepository: ReviewRepository;

    constructor(userRepositoy: UserRepository, reviewRepository: ReviewRepository) {
        this.userRepositoy = userRepositoy;
        this.reviewRepository = reviewRepository;
    }

    async execute(request: GetReviewByUserIdDTO): Promise<Response> {
        const { userId } = request;

        try {
            await this.userRepositoy.getUserByUserId(userId);
        } catch (error) {
            return left(new GetReviewsByUserIdErrors.UserIdNotFound()) as Response;
        }

        try {
            const reviews = await this.reviewRepository.getReviewsByUserId(userId);

            return right(Result.ok<ReviewEntity[]>(reviews));
        } catch (err) {
            return left(new GenericAppError.UnexpectedError(err)) as Response;
        }
    }
}
