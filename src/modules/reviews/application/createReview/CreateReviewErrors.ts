import { Result } from "../../../shared/core/Result";
import { UseCaseError } from "../../../shared/core/UseCaseError";

export namespace CreateReviewErrors {
    export class CreatedByUserIdNotFound extends Result<UseCaseError> {
        constructor() {
            super(false, {
                message: `User who is creating not found`
            } as UseCaseError);
        }
    }

    export class UserIdNotFound extends Result<UseCaseError> {
        constructor() {
            super(false, {
                message: `User to review not found`
            } as UseCaseError);
        }
    }
}
