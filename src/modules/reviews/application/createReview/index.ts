import { CreateReviewUseCase } from './CreateReviewUseCase';
import { CreateReviewController } from './CreateReviewController';
import { userRepositoryImpl } from '../../../users/infrastructure/repositories';
import { reviewRepositoryImpl } from '../../infrastructure/repositories';
import { mailerRepositoryImpl } from '../../../shared/core/mailer';
import { notificationRepositoryImpl } from '../../../notifications/infrastructure/repositories';

const createReviewUseCase = new CreateReviewUseCase(reviewRepositoryImpl, userRepositoryImpl, mailerRepositoryImpl, notificationRepositoryImpl);
const createReviewContoller = new CreateReviewController(createReviewUseCase);

export { createReviewUseCase, createReviewContoller };
