export interface CreateReviewDTO{
    createdByUserId: string;
    userId: string;
    text: string;
}