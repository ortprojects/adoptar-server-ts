import { NotificationEntity } from '../../../notifications/domain/NotificationEntity';
import { NotificationMessage } from '../../../notifications/domain/valueObjects/NotificationMessage';
import { NotificationRepository } from '../../../notifications/infrastructure/repositories/NotificationRepository';
import { GenericAppError } from '../../../shared/core/AppError';
import { MailerRepository } from '../../../shared/core/mailer/MailerRepository';
import { Either, left, Result, right } from '../../../shared/core/Result';
import { UseCase } from '../../../shared/domain/UseCase';
import { UniqueEntityID } from '../../../shared/domain/valueObjects/UniqueEntityID';
import { UserRepository } from '../../../users/infrastructure/repositories/UserRepository';
import { ReviewEntity } from '../../domain/ReviewEntity';
import { ReviewText } from '../../domain/valueObjects/ReviewText';
import { ReviewRepository } from '../../infrastructure/repositories/ReviewRepository';
import { CreateReviewErrors } from './CreateReviewErrors';
import { CreateReviewDTO } from './dtos/CreateReviewDTO';

type Response = Either<
    | GenericAppError.UnexpectedError
    | CreateReviewErrors.UserIdNotFound
    | CreateReviewErrors.CreatedByUserIdNotFound
    | Result<any>,
    Result<void>
>;

export class CreateReviewUseCase implements UseCase<CreateReviewDTO, Promise<Response>> {
    private reviewRepositoy: ReviewRepository;
    private userRepository: UserRepository;
    private mailerRepository: MailerRepository;
    private notificationRepository: NotificationRepository;

    constructor(
        reviewRepositoy: ReviewRepository,
        userRepository: UserRepository,
        mailerRepository: MailerRepository,
        notificationRepository: NotificationRepository
    ) {
        this.reviewRepositoy = reviewRepositoy;
        this.userRepository = userRepository;
        this.mailerRepository = mailerRepository;
        this.notificationRepository = notificationRepository;
    }

    async execute(request: CreateReviewDTO): Promise<Response> {
        let userEntity;
        let createdBy;

        try {
            createdBy = await this.userRepository.getUserByUserId(request.createdByUserId);
        } catch (error) {
            return left(new CreateReviewErrors.CreatedByUserIdNotFound()) as Response;
        }

        try {
            userEntity = await this.userRepository.getUserByUserId(request.userId);
        } catch (error) {
            return left(new CreateReviewErrors.UserIdNotFound()) as Response;
        }

        const textOrError = ReviewText.create(request.text);
        const createdByUserIdError = new UniqueEntityID(request.createdByUserId);
        const userIdOrError = new UniqueEntityID(request.userId);
        const createdAt = new Date();

        const combinedPropsResult = Result.combine([textOrError]);

        if (combinedPropsResult.isFailure) {
            return left(Result.fail<void>(combinedPropsResult.error)) as Response;
        }

        const reviewOrError = ReviewEntity.create({
            text: textOrError.getValue(),
            createdByUserId: createdByUserIdError,
            createdAt,
            userId: userIdOrError
        });

        if (reviewOrError.isFailure) {
            return left(Result.fail<ReviewEntity>(reviewOrError.error.toString())) as Response;
        }

        try {
            const reviewEntity = reviewOrError.getValue();

            await this.reviewRepositoy.addReview(reviewEntity);
        } catch (err) {
            return left(new GenericAppError.UnexpectedError(err.message)) as Response;
        }

        const notificationMessage = NotificationMessage.create(
            `${createdBy.displayName.value} realizo una nueva reseña sobre ti.`
        );

        const notificationOrError = NotificationEntity.create({
            createdAt: new Date(),
            userId: userEntity.id,
            message: notificationMessage.getValue(),
            isRead: false
        });

        if (notificationOrError.isFailure) {
            return left(
                Result.fail<NotificationEntity>(notificationOrError.error.toString())
            ) as Response;
        }

        const notificationEntity = notificationOrError.getValue();

        try {
            this.mailerRepository.sendEmail(
                userEntity.email.value,
                'Ey! tienes una nueva reseña',
                `${createdBy.displayName.value} creo que una reseña sobre ti. Ingresa a la app!`
            );

            await this.notificationRepository.addNotification(notificationEntity);
            return right(Result.ok<void>());
        } catch (error) {
            return left(new GenericAppError.UnexpectedError(error.message)) as Response;
        }
    }
}
