import { BaseController } from "../../../shared/infrastructure/http/models/BaseController";
import { CreateReviewErrors } from "./CreateReviewErrors";
import { CreateReviewUseCase } from "./CreateReviewUseCase";
import { CreateReviewDTO } from "./dtos/CreateReviewDTO";

export class CreateReviewController extends BaseController {
    private useCase: CreateReviewUseCase;

    constructor(useCase: CreateReviewUseCase) {
        super();
        this.useCase = useCase;
    }

    async executeImpl(): Promise<any> {
        const dto: CreateReviewDTO = this.req.body as CreateReviewDTO;

        try {
            const result = await this.useCase.execute(dto);

            if (result.isLeft()) {
                const error = result.value;

                switch (error.constructor) {
                    case CreateReviewErrors.CreatedByUserIdNotFound:
                        return this.conflict(error.errorValue().message);
                    case CreateReviewErrors.UserIdNotFound:
                        return this.conflict(error.errorValue().message);
                    default:
                        return this.fail(error.errorValue().message);
                }
            } else {
                return this.created(this.res);
            }
        } catch (err) {
            return this.fail(err);
        }
    }
}
