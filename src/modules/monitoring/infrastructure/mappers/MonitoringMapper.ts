import { UniqueEntityID } from '../../../shared/domain/valueObjects/UniqueEntityID';
import { MonitoringEntity } from '../../domain/MonitorinEntity';

export class MonitoringMapper {
    public static toPersistence(monitoring: MonitoringEntity): any {
        return {
            petPhotoChecked: monitoring.petPhotoChecked,
            newHouserChecked: monitoring.newHouserChecked,
            sanitaryNotebookChecked: monitoring.sanitaryNotebookChecked,
            daysControlChecked: monitoring.daysControlChecked,
            isFinished: monitoring.isFinished,
            postId: monitoring.postId.toString()
        };
    }

    public static toDomain(raw: any, id?: string): MonitoringEntity | null {
        const monitoringOrError = MonitoringEntity.create(
            {
                petPhotoChecked: raw.petPhotoChecked,
                newHouserChecked: raw.newHouserChecked,
                sanitaryNotebookChecked: raw.sanitaryNotebookChecked,
                daysControlChecked: raw.daysControlChecked,
                isFinished: raw.isFinished,
                postId: raw.postId
            },
            new UniqueEntityID(id)
        );

        return monitoringOrError.isSuccess ? monitoringOrError.getValue() : null;
    }

    public static toJSON(raw: MonitoringEntity): any {
        return {
            petPhotoChecked: raw.petPhotoChecked,
            newHouserChecked: raw.newHouserChecked,
            sanitaryNotebookChecked: raw.sanitaryNotebookChecked,
            daysControlChecked: raw.daysControlChecked,
            isFinished: raw.isFinished,
            postId: raw.postId.toString()
        };
    }
}
