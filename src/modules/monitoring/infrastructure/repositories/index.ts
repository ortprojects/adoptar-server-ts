import { FirebaseService } from '../../../shared/core/firebase/FirebaseService';
import { MonitoringRepository } from './MonitoringRepository';
import { MonitoringRepositoryImpl } from './MonitoringRepositoryImpl';

const firestore = FirebaseService.getFirestoreReference();

export const monitoringRepositoryImpl: MonitoringRepository = new MonitoringRepositoryImpl(firestore);
