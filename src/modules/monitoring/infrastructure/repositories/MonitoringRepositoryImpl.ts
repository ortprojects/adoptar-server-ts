import { MonitoringEntity } from '../../domain/MonitorinEntity';
import { MonitoringMapper } from '../mappers/MonitoringMapper';
import { MonitoringRepository } from './MonitoringRepository';

export class MonitoringRepositoryImpl implements MonitoringRepository {
    private monitoringCollectionPath = '/monitorings';
    private firestore: FirebaseFirestore.Firestore;

    constructor(firestore: FirebaseFirestore.Firestore) {
        this.firestore = firestore;
    }

    async addMonitoring(monitoring: MonitoringEntity): Promise<boolean> {
        const monitoringsCollection = this.firestore.collection(this.monitoringCollectionPath);

        await monitoringsCollection
            .doc(monitoring.postId.toString())
            .set(MonitoringMapper.toPersistence(monitoring));

        return true;
    }

    async updateMonitoring(monitoring: MonitoringEntity): Promise<boolean> {
        const monitoringsCollection = this.firestore.collection(this.monitoringCollectionPath);

        await monitoringsCollection
            .doc(monitoring.postId.toString())
            .update(MonitoringMapper.toPersistence(monitoring));

        return true;
    }

    async getMonitoringByPostId(postId: string): Promise<MonitoringEntity> {
        const monitoringsCollection = this.firestore.collection(this.monitoringCollectionPath);

        const monitoring = await monitoringsCollection.doc(postId).get();

        return MonitoringMapper.toDomain(monitoring.data(), monitoring.id);
    }
}
