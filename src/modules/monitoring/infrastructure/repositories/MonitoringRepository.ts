import { MonitoringEntity } from "../../domain/MonitorinEntity";

export interface MonitoringRepository {
    getMonitoringByPostId(postId: string): Promise<MonitoringEntity>;
    updateMonitoring(monitoring: MonitoringEntity): Promise<boolean>;
    addMonitoring(monitoring: MonitoringEntity): Promise<boolean>;
}
