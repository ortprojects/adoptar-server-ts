import { Request, Response } from 'express';
import express from 'express';
import { RouterPath } from '../../../shared/infrastructure/http/models/RouterPath';
import { LoggerImpl } from '../../../shared/infrastructure';
import { getMonitoringByPostIdContoller } from '../../application/getMonitoringByPostId';
import { updatemonitoringByPostIdContoller } from '../../application/updateMonitoringByPostId';

export class MonitoringRouter {
    private static readonly routePath = '/monitorings';

    public static create(): RouterPath {
        LoggerImpl.writeInfoLog('[MonitoringRoute::create] Created monitoring routes.');

        const monitoringRouter = express.Router();

        monitoringRouter.get('/', (req: Request, res: Response) => {
            getMonitoringByPostIdContoller.execute(req, res);
        });

        monitoringRouter.put('/:postId', (req: Request, res: Response) => {
            updatemonitoringByPostIdContoller.execute(req, res);
        });

        return { router: monitoringRouter, path: this.routePath };
    }
}
