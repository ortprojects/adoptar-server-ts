import { BaseController } from '../../../shared/infrastructure/http/models/BaseController';
import { MonitoringEntity } from '../../domain/MonitorinEntity';
import { MonitoringMapper } from '../../infrastructure/mappers/MonitoringMapper';
import { UpdateMonitoringByPostIdDTO } from './dtos/UpdateMonitoringByPostIdDTO';
import { UpdateMonitoringByPostIdErrors } from './UpdateMonitoringByPostIdErrors';
import { UpdateMonitoringByPostIdUseCase } from './UpdateMonitoringByPostIdUseCase';

export class UpdateMonitoringByPostIdController extends BaseController {
    private useCase: UpdateMonitoringByPostIdUseCase;

    constructor(useCase: UpdateMonitoringByPostIdUseCase) {
        super();
        this.useCase = useCase;
    }

    async executeImpl(): Promise<any> {
        const dto : UpdateMonitoringByPostIdDTO = this.req.body;

        const { postId } = this.req.params;

        dto.postId = postId as string

        try {
            const result = await this.useCase.execute(dto);

            if (result.isLeft()) {
                const error = result.value;

                switch (error.constructor) {
                    case UpdateMonitoringByPostIdErrors.MonitoringByPostIdNotFound:
                        return this.conflict(error.errorValue().message);
                    default:
                        return this.fail(error.errorValue().message);
                }
            } else {
                return this.created(this.res);
            }
        } catch (err) {
            return this.fail(err);
        }
    }
}
