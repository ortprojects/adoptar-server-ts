import { monitoringRepositoryImpl } from '../../infrastructure/repositories';
import { UpdateMonitoringByPostIdController } from './UpdateMonitoringByPostIdController';
import { UpdateMonitoringByPostIdUseCase } from './UpdateMonitoringByPostIdUseCase';

const updateMonitoringByPostIdUseCase = new UpdateMonitoringByPostIdUseCase(monitoringRepositoryImpl);
const updatemonitoringByPostIdContoller = new UpdateMonitoringByPostIdController(updateMonitoringByPostIdUseCase);

export { updateMonitoringByPostIdUseCase, updatemonitoringByPostIdContoller };
