import { GenericAppError } from '../../../shared/core/AppError';
import { Either, left, Result, right } from '../../../shared/core/Result';
import { UseCase } from '../../../shared/domain/UseCase';
import { MonitoringEntity } from '../../domain/MonitorinEntity';
import { MonitoringRepository } from '../../infrastructure/repositories/MonitoringRepository';
import { UpdateMonitoringByPostIdDTO } from './dtos/UpdateMonitoringByPostIdDTO';
import { UpdateMonitoringByPostIdErrors } from './UpdateMonitoringByPostIdErrors';

type Response = Either<
    GenericAppError.UnexpectedError | UpdateMonitoringByPostIdErrors.MonitoringByPostIdNotFound,
    Result<void>
>;

export class UpdateMonitoringByPostIdUseCase
    implements UseCase<UpdateMonitoringByPostIdDTO, Promise<Response>> {
    private monitoringRepositoy: MonitoringRepository;

    constructor(monitoringRepositoy: MonitoringRepository) {
        this.monitoringRepositoy = monitoringRepositoy;
    }

    async execute(req: UpdateMonitoringByPostIdDTO): Promise<Response> {
        let monitoringEntity;
        try {
            monitoringEntity = await this.monitoringRepositoy.getMonitoringByPostId(req.postId);
        } catch (error) {
            return left(
                new UpdateMonitoringByPostIdErrors.MonitoringByPostIdNotFound()
            ) as Response;
        }

        const monitoringOrError = MonitoringEntity.create(
            {
                daysControlChecked: req.daysControlChecked != undefined || req.daysControlChecked != null
                    ? req.daysControlChecked
                    : monitoringEntity.daysControlChecked,
                isFinished: req.isFinished != undefined || req.isFinished != null ? req.isFinished : monitoringEntity.isFinished,
                newHouserChecked: req.newHouserChecked != undefined || req.newHouserChecked != null
                    ? req.newHouserChecked
                    : monitoringEntity.newHouserChecked,
                petPhotoChecked: req.petPhotoChecked != undefined || req.petPhotoChecked != null
                    ? req.petPhotoChecked
                    : monitoringEntity.petPhotoChecked,
                sanitaryNotebookChecked: req.sanitaryNotebookChecked != undefined || req.sanitaryNotebookChecked != null
                    ? req.sanitaryNotebookChecked
                    : monitoringEntity.sanitaryNotebookChecked,
                postId: monitoringEntity.postId
            },
            monitoringEntity.id
        );

        if (monitoringOrError.isFailure) {
            return left(
                Result.fail<MonitoringEntity>(monitoringOrError.error.toString())
            ) as Response;
        }

        try {
            await this.monitoringRepositoy.updateMonitoring(monitoringOrError.getValue());
        } catch (err) {
            return left(new GenericAppError.UnexpectedError(err)) as Response;
        }

        return right(Result.ok<void>());
    }
}
