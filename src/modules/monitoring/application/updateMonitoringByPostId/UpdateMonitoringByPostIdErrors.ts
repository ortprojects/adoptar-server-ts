import { Result } from "../../../shared/core/Result";
import { UseCaseError } from "../../../shared/core/UseCaseError";

export namespace UpdateMonitoringByPostIdErrors {
    export class MonitoringByPostIdNotFound extends Result<UseCaseError> {
        constructor() {
            super(false, {
                message: `Couldn't find a monitoring`
            } as UseCaseError);
        }
    }
}
