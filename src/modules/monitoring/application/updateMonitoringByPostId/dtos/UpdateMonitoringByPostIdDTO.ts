export interface UpdateMonitoringByPostIdDTO{
    petPhotoChecked?: boolean;
    newHouserChecked?: boolean;
    sanitaryNotebookChecked?: boolean;
    daysControlChecked?: boolean;
    isFinished?: boolean;
    postId: string;
}