import { postRepositoryImpl } from "../../../posts/infrastructure/repositories";
import { monitoringRepositoryImpl } from "../../infrastructure/repositories";
import { GetMonitoringByPostIdController } from "./GetMonitoringByPostIdController";
import { GetMonitoringByPostIdUseCase } from "./GetMonitoringByPostIdUseCase";

const getMonitoringByPostIdUseCase = new GetMonitoringByPostIdUseCase(monitoringRepositoryImpl, postRepositoryImpl);
const getMonitoringByPostIdContoller = new GetMonitoringByPostIdController(getMonitoringByPostIdUseCase);

export {
    getMonitoringByPostIdUseCase,
    getMonitoringByPostIdContoller
}
