import { MonitoringRepository } from '../../infrastructure/repositories/MonitoringRepository';
import { GetMonitoringByPostIdErrors } from './GetMonitoringByPostIdErrors';
import { GetMonitoringByPostIdDTO } from './dtos/GetMonitoringByPostIdDTO';
import { PostRepository } from '../../../posts/infrastructure/repositories/PostRepository';
import { UseCase } from '../../../shared/domain/UseCase';
import { GenericAppError } from '../../../shared/core/AppError';
import { Either, left, Result, right } from '../../../shared/core/Result';
import { MonitoringEntity } from '../../domain/MonitorinEntity';

type Response = Either<
    GetMonitoringByPostIdErrors.PostByIdNotFound | GenericAppError.UnexpectedError,
    Result<MonitoringEntity>
>;

export class GetMonitoringByPostIdUseCase
    implements UseCase<GetMonitoringByPostIdDTO, Promise<Response>> {
    private monitoringRepositoy: MonitoringRepository;
    private postRepository: PostRepository;

    constructor(monitoringRepositoy: MonitoringRepository, postRepository: PostRepository) {
        this.monitoringRepositoy = monitoringRepositoy;
        this.postRepository = postRepository;
    }

    async execute(request: GetMonitoringByPostIdDTO): Promise<Response> {
        try {
            await this.postRepository.getPostById(request.postId);
        } catch (error) {
            return left(new GetMonitoringByPostIdErrors.PostByIdNotFound()) as Response;
        }

        try {
            const monitoring = await this.monitoringRepositoy.getMonitoringByPostId(request.postId);

            return right(Result.ok<MonitoringEntity>(monitoring));
        } catch (err) {
            return left(new GenericAppError.UnexpectedError(err));
        }
    }
}
