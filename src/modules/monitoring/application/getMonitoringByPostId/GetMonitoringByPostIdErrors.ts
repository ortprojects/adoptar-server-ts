import { Result } from "../../../shared/core/Result";
import { UseCaseError } from "../../../shared/core/UseCaseError";

export namespace GetMonitoringByPostIdErrors {
    export class PostByIdNotFound extends Result<UseCaseError> {
        constructor() {
            super(false, {
                message: `Post not found`
            } as UseCaseError);
        }
    }
}
