import { BaseController } from '../../../shared/infrastructure/http/models/BaseController';
import { MonitoringEntity } from '../../domain/MonitorinEntity';
import { MonitoringMapper } from '../../infrastructure/mappers/MonitoringMapper';
import { GetMonitoringByPostIdDTO } from './dtos/GetMonitoringByPostIdDTO';
import { GetMonitoringByPostIdErrors } from './GetMonitoringByPostIdErrors';
import { GetMonitoringByPostIdUseCase } from './GetMonitoringByPostIdUseCase';

export class GetMonitoringByPostIdController extends BaseController {
    private useCase: GetMonitoringByPostIdUseCase;

    constructor(useCase: GetMonitoringByPostIdUseCase) {
        super();
        this.useCase = useCase;
    }

    async executeImpl(): Promise<any> {
        const {
            postId,
        } = this.req.query;

        const dto: GetMonitoringByPostIdDTO = {
            postId: postId as string
        };

        try {
            const result = await this.useCase.execute(dto);

            if (result.isLeft()) {
                const error = result.value;

                console.log(error);
                switch (error.constructor) {
                    case GetMonitoringByPostIdErrors.PostByIdNotFound:
                        return this.conflict(error.errorValue().message);
                    default:
                        return this.fail(error.errorValue().message);
                }
            } else {
                const monitoring = result.value.getValue();

                return this.ok<MonitoringEntity>(this.res, MonitoringMapper.toJSON(monitoring));
            }
        } catch (err) {
            return this.fail(err);
        }
    }
}
