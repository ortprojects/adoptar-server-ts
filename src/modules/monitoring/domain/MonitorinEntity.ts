import { Guard } from '../../shared/core/Guard';
import { Result } from '../../shared/core/Result';
import { AggregateRoot } from '../../shared/domain/AggregateRoot';
import { UniqueEntityID } from '../../shared/domain/valueObjects/UniqueEntityID';

interface MonitoringProps {
    petPhotoChecked: boolean;
    newHouserChecked: boolean;
    sanitaryNotebookChecked: boolean;
    daysControlChecked: boolean;
    isFinished: boolean;
    postId: UniqueEntityID
}

export class MonitoringEntity extends AggregateRoot<MonitoringProps> {
    get petPhotoChecked(): boolean {
        return this.props.petPhotoChecked;
    }

    get newHouserChecked(): boolean {
        return this.props.newHouserChecked;
    }

    get sanitaryNotebookChecked(): boolean {
        return this.props.sanitaryNotebookChecked;
    }

    get daysControlChecked(): boolean {
        return this.props.daysControlChecked;
    }

    get isFinished(): boolean {
        return this.props.isFinished;
    }

    get postId(): UniqueEntityID {
        return this.props.postId;
    }

    private constructor(props: MonitoringProps, id?: UniqueEntityID) {
        super(props, id);
    }

    public static create(props: MonitoringProps, id?: UniqueEntityID): Result<MonitoringEntity> {
        const guardResult = Guard.againstNullOrUndefinedBulk([
            { argument: props.petPhotoChecked, argumentName: 'petPhotoChecked' },
            { argument: props.newHouserChecked, argumentName: 'newHouserChecked' },
            { argument: props.sanitaryNotebookChecked, argumentName: 'sanitaryNotebookChecked' },
            { argument: props.daysControlChecked, argumentName: 'daysControlChecked' },
            { argument: props.isFinished, argumentName: 'isFinished' },
            { argument: props.postId, argumentName: 'postId' },
        ]);

        if (!guardResult.succeeded) {
            return Result.fail<MonitoringEntity>(guardResult.message);
        }
        // const isNewUser = !!id === false;
        const monitoring = new MonitoringEntity(
            {
                ...props,
                petPhotoChecked: props.petPhotoChecked,
                newHouserChecked: props.newHouserChecked,
                sanitaryNotebookChecked: props.sanitaryNotebookChecked,
                daysControlChecked: props.daysControlChecked,
                isFinished: props.isFinished,
                postId: props.postId,
            },
            id
        );

        return Result.ok<MonitoringEntity>(monitoring);
    }
}
