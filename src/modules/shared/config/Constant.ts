import * as dotenv from 'dotenv';
dotenv.config();

export class Constant {
    static API_PATH = `/${process.env.API_PATH_BASE}/${process.env.API_VERSION}`;
    static PORT = process.env.PORT;
    static FIREBASE_DATABASE_URL = process.env.FIREBASE_DATABASE_URL;
    static FIREBASE_STORAGE_BUCKET = process.env.FIREBASE_STORAGE_BUCKET;
    static FIREBASE_PROJECT_ID = process.env.FIREBASE_PROJECT_ID;
    static FIREBASE_PRIVATE_KEY = process.env.FIREBASE_PRIVATE_KEY;
    static FIREBASE_CLIENT_EMAIL = process.env.FIREBASE_CLIENT_EMAIL;
    static SENDER_EMAIL_ADDRESS = process.env.SENDER_EMAIL_ADDRESS;
    static MAILING_SERVICE_CLIENT_ID = process.env.MAILING_SERVICE_CLIENT_ID;
    static MAILING_SERVICE_CLIENT_SECRET = process.env.MAILING_SERVICE_CLIENT_SECRET;
    static MAILING_SERVICE_REFRESH_TOKEN = process.env.MAILING_SERVICE_REFRESH_TOKEN;
}
