export interface MailerRepository {
    sendEmail(to: string, subject: string, text: string): void;
}
