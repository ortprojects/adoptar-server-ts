import nodemailer from 'nodemailer';
import Mail from 'nodemailer/lib/mailer';
import { google } from 'googleapis';
import { Constant } from '../../config/Constant';
import { MailerRepository } from './MailerRepository';
const OAuth2 = google.auth.OAuth2;

const OAUTH_PLAYGROUND = 'https://developers.google.com/oauthplayground';

const OAuth2Client = new OAuth2(
    Constant.MAILING_SERVICE_CLIENT_ID,
    Constant.MAILING_SERVICE_CLIENT_SECRET,
    OAUTH_PLAYGROUND
);

OAuth2Client.setCredentials({
    refresh_token: Constant.MAILING_SERVICE_REFRESH_TOKEN
});

const myAccessToken = OAuth2Client.getAccessToken();

export class NodeMailerRepositoryImpl implements MailerRepository {
    private transporter: Mail;

    constructor() {
        this.createTransport();
    }

    private async createTransport() {
        try {
            const token = (await myAccessToken).token;

            this.transporter = nodemailer.createTransport({
                service: 'gmail',
                secure: false,
                auth: {
                    type: 'OAuth2',
                    user: Constant.SENDER_EMAIL_ADDRESS, //your gmail account you used to set the project up in google cloud console"
                    clientId: Constant.MAILING_SERVICE_CLIENT_ID,
                    clientSecret: Constant.MAILING_SERVICE_CLIENT_SECRET,

                    refreshToken: Constant.MAILING_SERVICE_REFRESH_TOKEN,
                    accessToken: token
                },
                tls: {
                    rejectUnauthorized: false
                }
            });
        } catch (error) {
            console.log(error);
        }
    }

    public sendEmail(to: string, subject: string, text: string) {
        this.transporter
            .sendMail({
                from: Constant.SENDER_EMAIL_ADDRESS,
                to,
                subject,
                text
            })
            .catch((error) => console.log(error));
    }
}
