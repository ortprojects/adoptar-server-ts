import { MailerRepository } from './MailerRepository';
import { NodeMailerRepositoryImpl } from './NodeMailerRepositoryImpl';

const mailerRepositoryImpl: MailerRepository = new NodeMailerRepositoryImpl();

export { mailerRepositoryImpl };
