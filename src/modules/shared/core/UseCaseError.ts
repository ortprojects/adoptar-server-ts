interface UseCaseErrorRepository {
    message: string;
}

export abstract class UseCaseError implements UseCaseErrorRepository {
    public readonly message: string;

    constructor(message: string) {
        this.message = message;
    }
}
