import admin from 'firebase-admin';
import { Constant } from '../../config/Constant';
import * as dotenv from 'dotenv';

dotenv.config();

admin.initializeApp({
    credential: admin.credential.applicationDefault(),
    databaseURL: Constant.FIREBASE_DATABASE_URL,
    storageBucket: Constant.FIREBASE_STORAGE_BUCKET
});

// Set up database connection
let firestoreDb: FirebaseFirestore.Firestore | null = null;
let storage: admin.storage.Storage | null = null;
let auth: admin.auth.Auth | null = null;

// Get cached db reference or create it
function getFirestoreReference() {
    if (firestoreDb === null) {
        const firestore = admin.firestore();
        if (process.env.NODE_ENV == 'development') {
            firestore.settings({
                host: 'localhost:7550',
                ssl: false
            });
        }
        firestoreDb = firestore;
    }
    return firestoreDb;
}
function getStorageReference() {
    if (storage === null) {
        storage = admin.storage();
    }
    return storage;
}

function getAuthReference() {
    if (auth === null) {
        auth = admin.auth();
    }

    return auth;
}


export const FirebaseService = {
    getFirestoreReference,
    getStorageReference,
    getAuthReference
};
