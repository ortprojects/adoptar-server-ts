import * as dotenv from 'dotenv';
dotenv.config();

export class LoggerImpl {
    constructor() {}

    public static writeErrorLog(message: string) {
        console.error(message);
    }

    public static writeInfoLog(message: string) {
        console.info(message);
    }
}
