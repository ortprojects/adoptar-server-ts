import { Constant } from '../../config/Constant';
import { Storage } from '@google-cloud/storage';
import { format } from 'util';
import { StorageRepository } from './StorageReposity';
import { FirebaseService } from '../../core/firebase/FirebaseService';
import path from 'path'

export class StorageRepositoryImpl implements StorageRepository {
    private storageRef: Storage = null;

    private getGoogleStorage() {
        if (!this.storageRef) {
            this.storageRef = new Storage({
                projectId: Constant.FIREBASE_PROJECT_ID
            });
        }
        return this.storageRef;
    }

    public upload(folder: string, file: Express.Multer.File): Promise<string> {
        return new Promise((resolve, reject) => {
            if (!file) {
                reject('No image file');
            }

            const cleanName = path.parse(file.originalname).name

            let newFileName = `${folder}/${cleanName}_holaSin${Date.now()}`;

            const bucket = this.getGoogleStorage().bucket(Constant.FIREBASE_STORAGE_BUCKET);

            let fileUpload = bucket.file(newFileName);

            const blobStream = fileUpload.createWriteStream({
                metadata: {
                    contentType: file.mimetype
                }
            });
            

            blobStream.on('error', (error: any) => {
                console.log(error);
                reject('Something is wrong! Unable to upload at the moment.');
            });

            blobStream.on('finish', () => {
                // The public URL can be used to directly access the file via HTTP.
                const url = format(
                    `https://storage.googleapis.com/${bucket.name}/${fileUpload.name}`
                );
                resolve(url);
            });

            blobStream.end(file.buffer);
        });
    }
}
