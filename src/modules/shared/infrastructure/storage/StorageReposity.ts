export interface StorageRepository {
    upload(bucketPath: string, file: Express.Multer.File): Promise<string>;
}
