import { Router } from "express";

export interface RouterPath{
    router: Router;
    path: string;
}