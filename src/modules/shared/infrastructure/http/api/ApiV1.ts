import { Router } from 'express';
import { PostRouter } from '../../../../posts/infrastructure/http/routes';
import { UserRouter } from '../../../../users/infrastructure/http/routes';
import express from 'express';
import { ReviewRouter } from '../../../../reviews/infrastructure/http/routes/ReviewRouter';
import { AdoptionRouter } from '../../../../adoptions/infrastructure/http/routes/AdoptionRouter';
import { NotificationRouter } from '../../../../notifications/infrastructure/http';
import { MonitoringRouter } from '../../../../monitoring/infrastructure/http/MonitoringRouter';

export class ApiV1 {
    public static create(): Router {
        const v1Router = express.Router();

        // instance routes
        const routes = [
            PostRouter.create(),
            UserRouter.create(),
            ReviewRouter.create(),
            AdoptionRouter.create(),
            NotificationRouter.create(),
            MonitoringRouter.create()
        ];

        routes.forEach((route) => {
            v1Router.use(route.path, route.router);
        });

        return v1Router;
    }
}
