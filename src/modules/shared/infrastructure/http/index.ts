import { FirebaseAuthServiceImpl } from '../../../users/services/FirebaseAuthServiceImpl';
import { FirebaseService } from '../../core/firebase/FirebaseService';
import { Middleware } from './utils/middlewares/Middleware';

const authRef = FirebaseService.getAuthReference();

const authService = new FirebaseAuthServiceImpl(authRef);

const middleware = new Middleware(authService);

export { middleware };
