import { Response } from 'express';
import { AuthServiceRepository } from '../../../../../users/services/AuthServiceRepository';

export class Middleware {
    private authService: AuthServiceRepository;

    constructor(authService: AuthServiceRepository) {
        this.authService = authService;
    }

    private endRequest(status: 400 | 401 | 403, message: string, res: any): any {
        return res.status(status).send({ message });
    }

    public ensureAuthenticated() {
        return async (req: any, res: Response, next: any) => {
            const token = req.headers['authorization'];
            // Confirm that the token was signed with our signature.
            if (token) {
                const decoded = await this.authService.verifyTokenId(token);

                if (!decoded) {
                    return this.endRequest(
                        403,
                        'Auth token not found. User is probably not logged in. Please login again.',
                        res
                    );
                }

                req.decoded = decoded;
                return next();
            } else {
                return this.endRequest(403, 'No access token provided', res);
            }
        };
    }
}
