import { Result } from '../../../shared/core/Result';
import { ValueObject } from '../../../shared/domain/ValueObject';

export interface UserPhoneNumberProps {
    value: string;
}

export class UserPhoneNumber extends ValueObject<UserPhoneNumberProps> {
    get value(): string {
        return this.props.value;
    }

    private constructor(props: UserPhoneNumberProps) {
        super(props);
    }

    public static create(phoneNumber?: string): Result<UserPhoneNumber> {
        return Result.ok<UserPhoneNumber>(new UserPhoneNumber({ value: phoneNumber ? phoneNumber : "" }));
    }
}
