import { Result } from '../../../shared/core/Result';
import { ValueObject } from '../../../shared/domain/ValueObject';

export interface UserDniProps {
    value: string;
}

export class UserDni extends ValueObject<UserDniProps> {
    get value(): string {
        return this.props.value;
    }

    private constructor(props: UserDniProps) {
        super(props);
    }

    private static isValidDni(dni: string) {
        return dni.length == 8;
    }

    public static create(dni?: string): Result<UserDni> {
        return Result.ok<UserDni>(new UserDni({ value: dni ? dni : '' }));
    }
}
