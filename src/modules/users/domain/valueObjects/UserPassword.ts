import { Result } from "../../../shared/core/Result";
import { ValueObject } from "../../../shared/domain/ValueObject";

const PASSWORD_MAX_LENGTH = 8;
export interface UserPasswordProps {
    value: string;
}

export class UserPassword extends ValueObject<UserPasswordProps> {
    get value(): string {
        return this.props.value;
    }

    private constructor(props: UserPasswordProps) {
        super(props);
    }

    private static isValidPassword(password: string) {
        return password.length >= PASSWORD_MAX_LENGTH
    }

    private static format(email: string): string {
        return email.trim().toLowerCase();
    }

    public static create(password: string): Result<UserPassword> {
        if (!this.isValidPassword(password)) {
            return Result.fail<UserPassword>('Password not valid');
        } else {
            return Result.ok<UserPassword>(new UserPassword({ value: password }));
        }
    }
}
