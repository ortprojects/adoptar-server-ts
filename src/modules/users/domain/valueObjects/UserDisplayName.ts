import { Guard } from '../../../shared/core/Guard';
import { Result } from '../../../shared/core/Result';
import { ValueObject } from '../../../shared/domain/ValueObject';

interface UserDisplayNameProps {
    value: string;
}

export class UserDisplayName extends ValueObject<UserDisplayNameProps> {
    get value(): string {
        return this.props.value;
    }

    private constructor(props: UserDisplayNameProps) {
        super(props);
    }

    public static create(displayName?: string): Result<UserDisplayName> {
        return Result.ok<UserDisplayName>(new UserDisplayName({ value: displayName ? displayName : "" }));
    }
}
