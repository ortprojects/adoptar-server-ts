import { Result } from '../../../shared/core/Result';
import { ValueObject } from '../../../shared/domain/ValueObject';
import geohash from 'ngeohash';

export interface GeoLocation {
    latitude: number;
    longitude: number;
    geohash?: string;
}

export interface UserAddressProps {
    value: GeoLocation;
}

export class UserAddress extends ValueObject<UserAddressProps> {
    get value(): GeoLocation {
        return this.props.value;
    }

    private constructor(props: UserAddressProps) {
        super(props);
    }

    public static create(address?: GeoLocation): Result<UserAddress> {
        if (
            address &&
            address.latitude &&
            address.latitude != 0 &&
            address.longitude &&
            address.longitude != 0
        ) {
            const encodeGeohash = geohash.encode(address.latitude, address.longitude);
            address.geohash = encodeGeohash;
        } else {
            address = {
                latitude: 0,
                longitude: 0,
                geohash: ''
            };
        }
        return Result.ok<UserAddress>(new UserAddress({ value: address }));
    }
}
