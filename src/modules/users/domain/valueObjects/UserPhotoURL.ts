import { Guard } from '../../../shared/core/Guard';
import { Result } from '../../../shared/core/Result';
import { ValueObject } from '../../../shared/domain/ValueObject';

interface UserPhotoURLProps {
    value: string;
}

export class UserPhotoURL extends ValueObject<UserPhotoURLProps> {
    get value(): string {
        return this.props.value;
    }

    private constructor(props: UserPhotoURLProps) {
        super(props);
    }

    public static create(photoURL?: string): Result<UserPhotoURL> {
        return Result.ok<UserPhotoURL>(new UserPhotoURL({ value: photoURL ? photoURL : "" }));
    }
}
