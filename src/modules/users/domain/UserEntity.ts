import { Guard } from '../../shared/core/Guard';
import { Result } from '../../shared/core/Result';
import { AggregateRoot } from '../../shared/domain/AggregateRoot';
import { UniqueEntityID } from '../../shared/domain/valueObjects/UniqueEntityID';
import { UserAddress } from './valueObjects/UserAddress';
import { UserDisplayName } from './valueObjects/UserDisplayName';
import { UserDni } from './valueObjects/UserDni';
import { UserEmail } from './valueObjects/UserEmail';
import { UserId } from './valueObjects/UserId';
import { UserPhoneNumber } from './valueObjects/UserPhoneNumber';
import { UserPhotoURL } from './valueObjects/UserPhotoURL';

interface UserProps {
    email: UserEmail;
    displayName?: UserDisplayName;
    dni?: UserDni;
    address?: UserAddress;
    phoneNumber?: UserPhoneNumber;
    photoURL?: UserPhotoURL;
    isFirstTime: boolean
}

export class UserEntity extends AggregateRoot<UserProps> {
    get userId(): UserId {
        return UserId.create(this._id).getValue();
    }

    get email(): UserEmail {
        return this.props.email;
    }

    get dni(): UserDni {
        return this.props.dni;
    }

    get displayName(): UserDisplayName {
        return this.props.displayName;
    }

    get address(): UserAddress {
        return this.props.address;
    }

    get geoLocphoneNumberation(): UserPhoneNumber {
        return this.props.phoneNumber;
    }

    get phoneNumber(): UserPhoneNumber {
        return this.props.phoneNumber;
    }

    get photoURL(): UserPhotoURL {
        return this.props.photoURL;
    }

    get isFirstTime(): boolean {
        return this.props.isFirstTime;
    }

    set displayName(value: UserDisplayName) {
        this.props.displayName = value;
    }

    set address(value: UserAddress) {
        this.props.address = value;
    }

    set dni(value: UserDni) {
        this.props.dni = value;
    }

    set phoneNumber(value: UserPhoneNumber) {
        this.props.phoneNumber = value;
    }

    set photoURL(value: UserPhotoURL) {
        this.props.photoURL = value;
    }

    private constructor(props: UserProps, id?: UniqueEntityID) {
        super(props, id);
    }

    public static create(props: UserProps, id?: UniqueEntityID): Result<UserEntity> {
        const guardResult = Guard.againstNullOrUndefinedBulk([
            { argument: props.email, argumentName: 'email' }
        ]);

        if (!guardResult.succeeded) {
            return Result.fail<UserEntity>(guardResult.message);
        }
        // const isNewUser = !!id === false;
        const user = new UserEntity(
            {
                ...props,
                displayName: props.displayName,
                dni: props.dni,
                address: props.address,
                photoURL: props.photoURL,
                phoneNumber: props.phoneNumber,
                email: props.email,
                isFirstTime: props.isFirstTime
            },
            id
        );

        return Result.ok<UserEntity>(user);
    }
}
