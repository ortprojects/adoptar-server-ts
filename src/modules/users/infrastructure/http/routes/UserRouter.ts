import { Request, Response } from 'express';
import { LoggerImpl } from '../../../../shared/infrastructure';
import { RouterPath } from '../../../../shared/infrastructure/http/models/RouterPath';
import { createUserContoller } from '../../../application/useCases/createUser';
import express from 'express';
import { updateUserContoller } from '../../../application/useCases/updateUser';
import { getUserContoller } from '../../../application/useCases/getUser';
import { getPostsByUserIdContoller } from '../../../../posts/aplication/useCases/getPostsByUserId';
import { getInterestedInContoller } from '../../../application/useCases/getInterestedIn';
import { getInterestedContoller } from '../../../application/useCases/getInterested';
import { getAdoptedByUserIdContoller } from '../../../application/useCases/getAdoptedByUserId';
import { getInAdoptionByUserIdContoller } from '../../../application/useCases/getInAdoptionByUserId';
import { deleteInterestedInContoller } from '../../../application/useCases/deleteInterested';

export class UserRouter {
    private static readonly routePath = '/users';

    public static create(): RouterPath {
        LoggerImpl.writeInfoLog('[UserRoute::create] Creating user routes.');

        const userRouter = express.Router();

        userRouter.get('/:userId', (req: Request, res: Response) => {
            getUserContoller.execute(req, res);
        });

        userRouter.get('/:userId/posts', (req: Request, res: Response) => {
            getPostsByUserIdContoller.execute(req, res);
        });

        userRouter.get('/:userId/interested-in', (req: Request, res: Response) => {
            getInterestedInContoller.execute(req, res);
        });

        userRouter.get('/:userId/interested/:postId', (req: Request, res: Response) => {
            getInterestedContoller.execute(req, res);
        });

        userRouter.get('/:userId/adopted', (req: Request, res: Response) => {
            getAdoptedByUserIdContoller.execute(req, res);
        });

        userRouter.get('/:userId/in-adoption', (req: Request, res: Response) => {
            getInAdoptionByUserIdContoller.execute(req, res);
        });

        userRouter.post('/', (req: Request, res: Response) => {
            createUserContoller.execute(req, res);
        });

        userRouter.put('/', (req: Request, res: Response) => {
            updateUserContoller.execute(req, res);
        });

        userRouter.delete('/:userId/interested/:postId', (req: Request, res: Response) => {
            deleteInterestedInContoller.execute(req, res);
        });

        return { router: userRouter, path: this.routePath };
    }
}
