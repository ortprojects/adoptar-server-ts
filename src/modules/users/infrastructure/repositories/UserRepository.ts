import { UserEntity } from '../../domain/UserEntity';
import { PostEntity } from '../../../posts/domain';

export interface UserRepository {
    exists(userEmail: string): Promise<boolean>;
    getUserByUserId(userId: string): Promise<UserEntity>;
    save(userEntity: UserEntity): Promise<void>;
    updateUser(user: UserEntity): Promise<void>;
    getInterestedIn(userId: string): Promise<PostEntity[]>;
    getInterested(postId: string): Promise<UserEntity[]>;
    getAdoptedByUserId(userId: string): Promise<PostEntity[]>;
    getInAdoptionByUserId(userId: string): Promise<PostEntity[]>;
    addInteresedIn(userId: string, postId: string): Promise<boolean>;
    addInteresed(userId: string, postId: string): Promise<boolean>;
    deletePostFromInterested(postId: string): Promise<boolean>;
    deleteUserFromInterested(postId: string, userId: string): Promise<boolean>;
    deleteUserFromInterestedIn(postId: string, userId: string): Promise<boolean>;
    deletePostFromAllUsersInterestedIn(postId: string): Promise<boolean>;
}
