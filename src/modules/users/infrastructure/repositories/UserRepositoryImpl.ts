import { userRepositoryImpl } from '.';
import { PostEntity } from '../../../posts/domain';
import { postRepositoryImpl } from '../../../posts/infrastructure/repositories';
import { PostRepository } from '../../../posts/infrastructure/repositories/PostRepository';
import { PostMapper } from '../../../posts/mappers/PostMapper';
import { UserEntity } from '../../domain/UserEntity';
import { AuthServiceRepository } from '../../services/AuthServiceRepository';
import { UserMapper } from '../mappers/UserMapper';
import { UserRepository } from './UserRepository';

export class UserRepositoryImpl implements UserRepository {
    private firestore: FirebaseFirestore.Firestore;
    private userCollectionPath = '/users';
    private interestedInCollectionPath = '/interestedIn';
    private interestedCollectionPath = '/interested';
    private adoptedCollectionPath = '/adopted';
    private inAdoptionCollectionPath = '/inAdoption';
    private authService: AuthServiceRepository;

    constructor(firestore: FirebaseFirestore.Firestore, authService: AuthServiceRepository) {
        this.firestore = firestore;
        this.authService = authService;
    }

    async deletePostFromAllUsersInterestedIn(postId: string): Promise<boolean> {
        const interestedInCollectionGroup = this.firestore.collectionGroup('interestedInPosts');
        const posts = await interestedInCollectionGroup.where('postId', '==', postId).get();

        for (const post of posts.docs) {
            await post.ref.delete();
        }

        return true;
    }

    async deleteUserFromInterestedIn(postId: string, userId: string): Promise<boolean> {
        const interestedInCollection = this.firestore.collection(this.interestedInCollectionPath);

        await interestedInCollection.doc(userId).collection('interestedInPosts').doc(postId).delete();

        return true;
    }

    async deleteUserFromInterested(postId: string, userId: string): Promise<boolean> {
        const interestedCollection = this.firestore.collection(this.interestedCollectionPath);

        await interestedCollection.doc(postId).collection('interestedUsers').doc(userId).delete();

        return true;
    }

    async deletePostFromInterested(postId: string): Promise<boolean> {
        let collectionRef = this.firestore.collection(this.interestedCollectionPath);
        let query = collectionRef
            .doc(postId)
            .collection('interestedUsers')
            .limit(20);

        new Promise((resolve, reject) => {
            this.deleteQueryBatch(this.firestore, query, 20, resolve, reject);
        });

        await collectionRef.doc(postId).delete();

        return true;
    }

    async addInteresed(userId: string, postId: string): Promise<boolean> {
        const interestedCollection = this.firestore.collection(this.interestedCollectionPath);

        await interestedCollection
            .doc(postId)
            .collection('interestedUsers')
            .doc(userId)
            .create({ createdAt: new Date(), postId });

        return true;
    }

    async addInteresedIn(userId: string, postId: string): Promise<boolean> {
        const interestedInCollection = this.firestore.collection(this.interestedInCollectionPath);

        await interestedInCollection
            .doc(userId)
            .collection('interestedInPosts')
            .doc(postId)
            .create({ createdAt: new Date(), postId });

        return true;
    }

    async getInAdoptionByUserId(userId: string): Promise<PostEntity[]> {
        const inAdoptionCollection = this.firestore.collection(this.inAdoptionCollectionPath);

        const postsSnapshot = await inAdoptionCollection.doc(userId).collection('/posts').get();

        const postsRet: PostEntity[] = [];

        for (const doc of postsSnapshot.docs) {
            const post = await postRepositoryImpl.getPostById(doc.id);

            if (post != null) {
                postsRet.push(post);
            }
        }

        return postsRet;
    }

    async getAdoptedByUserId(userId: string): Promise<PostEntity[]> {
        const adoptedCollection = this.firestore.collection(this.adoptedCollectionPath);

        const postsSnapshot = await adoptedCollection.doc(userId).collection('/posts').get();

        const postsRet: PostEntity[] = [];

        for (const doc of postsSnapshot.docs) {
            const post = await postRepositoryImpl.getPostById(doc.id);

            if (post != null) {
                postsRet.push(post);
            }
        }

        return postsRet;
    }

    async getInterested(postId: string): Promise<UserEntity[]> {
        const interestedCollection = this.firestore.collection(this.interestedCollectionPath);

        const usersSnapshot = await interestedCollection
            .doc(postId)
            .collection('interestedUsers')
            .get();

        const usersRet: UserEntity[] = [];

        for (const doc of usersSnapshot.docs) {
            const user = await userRepositoryImpl.getUserByUserId(doc.id);

            if (user != null) {
                usersRet.push(user);
            }
        }

        return usersRet;
    }

    async getInterestedIn(userId: string): Promise<PostEntity[]> {
        const interestedInCollection = this.firestore.collection(this.interestedInCollectionPath);

        const postsSnapshot = await interestedInCollection
            .doc(userId)
            .collection('interestedInPosts')
            .get();

        const postsRet: PostEntity[] = [];

        for (const doc of postsSnapshot.docs) {
            const post = await postRepositoryImpl.getPostById(doc.id);
            if (post != null) {
                postsRet.push(post);
            }
        }

        return postsRet;
    }

    async exists(userEmail: string): Promise<boolean> {
        const userCollection = this.firestore.collection(this.userCollectionPath);

        const userSnapshot = await userCollection.where('email', '==', userEmail).get();

        return !userSnapshot.empty;
    }

    async getUserByUserId(userId: string): Promise<UserEntity> {
        const userCollection = this.firestore.collection(this.userCollectionPath);

        const userSnapshot = await userCollection.doc(userId).get();

        if (!userSnapshot.exists) throw new Error('User not found.');

        return UserMapper.toDomain(userSnapshot.data(), userSnapshot.id);
    }

    async save(user: UserEntity): Promise<void> {
        const userCollection = this.firestore.collection(this.userCollectionPath);

        const rawUser = await UserMapper.toPersistence(user);

        await userCollection.doc(rawUser.id).set(rawUser);

        return;
    }

    async updateUser(user: UserEntity): Promise<void> {
        const userCollection = this.firestore.collection(this.userCollectionPath);

        const rawUser = await UserMapper.toPersistence(user);

        await userCollection.doc(rawUser.id).update(rawUser);
    }

    private deleteQueryBatch(db: any, query: any, batchSize: number, resolve: any, reject: any) {
        query
            .get()
            .then((snapshot: any) => {
                // When there are no documents left, we are done
                if (snapshot.size == 0) {
                    return 0;
                }

                // Delete documents in a batch
                let batch = db.batch();
                snapshot.docs.forEach((doc: any) => {
                    batch.delete(doc.ref);
                });

                return batch.commit().then(() => {
                    return snapshot.size;
                });
            })
            .then((numDeleted: any) => {
                if (numDeleted === 0) {
                    resolve();
                    return;
                }

                // Recurse on the next process tick, to avoid
                // exploding the stack.
                process.nextTick(() => {
                    this.deleteQueryBatch(db, query, batchSize, resolve, reject);
                });
            })
            .catch(reject);
    }
}
