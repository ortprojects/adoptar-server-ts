import { postRepositoryImpl } from '../../../posts/infrastructure/repositories';
import { FirebaseService } from '../../../shared/core/firebase/FirebaseService';
import { authService } from '../../services';
import { UserRepository } from './UserRepository';
import { UserRepositoryImpl } from './UserRepositoryImpl';

const firestore = FirebaseService.getFirestoreReference();

export const userRepositoryImpl: UserRepository = new UserRepositoryImpl(firestore, authService);