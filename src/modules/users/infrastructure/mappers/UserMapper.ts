import { UniqueEntityID } from '../../../shared/domain/valueObjects/UniqueEntityID';
import { BaseMapper } from '../../../shared/infrastructure/mappers/BaseMapper';
import { UserEntity } from '../../domain/UserEntity';
import { UserAddress } from '../../domain/valueObjects/UserAddress';
import { UserDisplayName } from '../../domain/valueObjects/UserDisplayName';
import { UserDni } from '../../domain/valueObjects/UserDni';
import { UserEmail } from '../../domain/valueObjects/UserEmail';
import { UserPhoneNumber } from '../../domain/valueObjects/UserPhoneNumber';
import { UserPhotoURL } from '../../domain/valueObjects/UserPhotoURL';

export class UserMapper implements BaseMapper<UserEntity> {
    public static async toPersistence(user: UserEntity): Promise<any> {
        const raw = {
            id: user.id.toString(),
            email: user.email.value,
            displayName: user.displayName.value,
            address: {
                latitude: user.address.value.latitude,
                longitude: user.address.value.longitude,
                geohash: user.address.value.geohash
            },
            dni: user.dni.value,
            phoneNumber: user.phoneNumber.value,
            photoURL: user.photoURL.value,
            isFirstTime: user.isFirstTime
        };

        return raw;
    }

    public static toDomain(raw: any, id?: string): UserEntity | null {
        const emailOrError = UserEmail.create(raw.email);
        const displayNameOrError = UserDisplayName.create(raw.displayName);
        const dniOrError = UserDni.create(raw.dni);
        const addressOrError = UserAddress.create(raw.address);
        const photoURLOrError = UserPhotoURL.create(raw.photoURL);
        const phoneNumberOrError = UserPhoneNumber.create(raw.phoneNumber);

        const userOrError = UserEntity.create(
            {
                email: emailOrError.getValue(),
                displayName: displayNameOrError.getValue(),
                address: addressOrError.getValue(),
                dni: dniOrError.getValue(),
                photoURL: photoURLOrError.getValue(),
                phoneNumber: phoneNumberOrError.getValue(),
                isFirstTime: raw.isFirstTime
            },
            new UniqueEntityID(id)
        );

        return userOrError.isSuccess ? userOrError.getValue() : null;
    }

    public static toJSON(raw: UserEntity): any {
        return {
            id: raw.id.toString(),
            email: raw.email.value,
            displayName: raw.displayName.value,
            address: raw.address.value,
            dni: raw.dni.value,
            photoURL: raw.photoURL.value,
            phoneNumber: raw.phoneNumber.value,
            isFirstTime: raw.isFirstTime
        };
    }
}
