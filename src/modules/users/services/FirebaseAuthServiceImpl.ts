import { AuthServiceRepository } from './AuthServiceRepository';
import admin from 'firebase-admin';
import { CreateUserDTO } from '../application/useCases/createUser/dtos/CreateUserDTO';
import { UniqueEntityID } from '../../shared/domain/valueObjects/UniqueEntityID';
import { CONNREFUSED } from 'dns';

export class FirebaseAuthServiceImpl implements AuthServiceRepository {
    private auth: admin.auth.Auth;

    constructor(auth: admin.auth.Auth) {
        this.auth = auth;
    }

    async verifyTokenId(idToken: string): Promise<admin.auth.DecodedIdToken> {
        try {
            const decoded = await this.auth.verifyIdToken(idToken);
            return decoded;
        } catch (e) {
            throw new Error('User is not authenticated');
        }
    }

    async createAuthUser(user: CreateUserDTO): Promise<admin.auth.UserRecord>{
        const newUser = await this.auth.createUser({
            uid: new UniqueEntityID().toString(),
            email: user.email,
            emailVerified: false,
        });

        return newUser;
    }
}
