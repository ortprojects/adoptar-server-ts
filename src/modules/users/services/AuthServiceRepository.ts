import admin from 'firebase-admin';
import { CreateUserDTO } from '../application/useCases/createUser/dtos/CreateUserDTO';

export interface AuthServiceRepository {
    verifyTokenId(idToken: string): Promise<admin.auth.DecodedIdToken>;
    createAuthUser(user: CreateUserDTO): Promise<admin.auth.UserRecord>;
}
