import admin from 'firebase-admin';
import { FirebaseService } from '../../shared/core/firebase/FirebaseService';
import { FirebaseAuthServiceImpl } from './FirebaseAuthServiceImpl';

const authReference = FirebaseService.getAuthReference();

const authService = new FirebaseAuthServiceImpl(authReference);

export { authService };
