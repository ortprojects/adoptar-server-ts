import { BaseController } from '../../../../shared/infrastructure/http/models/BaseController';
import { UserEntity } from '../../../domain/UserEntity';
import { UserMapper } from '../../../infrastructure/mappers/UserMapper';
import { GetUserDTO } from './dtos/GetUserDTO';
import { GetUserErrors } from './GetUserErrors';
import { GetUserUseCase } from './GetUserUseCase';

export class GetUserController extends BaseController {
    private useCase: GetUserUseCase;

    constructor(useCase: GetUserUseCase) {
        super();
        this.useCase = useCase;
    }

    async executeImpl(): Promise<any> {
        const { userId } = this.req.params;

        let dto: GetUserDTO = {
            userId
        };

        try {
            const result = await this.useCase.execute(dto);

            if (result.isLeft()) {
                const error = result.value;
                switch (error.constructor) {
                    case GetUserErrors.UserWithIdNotFoundError:
                        return this.conflict(error.errorValue().message);
                    default:
                        return this.fail(error.errorValue().message);
                }
            } else {
                const user = result.value.getValue()
                return this.ok<UserEntity>(this.res, UserMapper.toJSON(user));
            }
        } catch (err) {
            return this.fail(err);
        }
    }
}
