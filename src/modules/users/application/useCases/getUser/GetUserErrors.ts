import { Result } from "../../../../shared/core/Result";
import { UseCaseError } from "../../../../shared/core/UseCaseError";

export namespace GetUserErrors {
    export class UserWithIdNotFoundError extends Result<UseCaseError> {
        constructor() {
            super(false, {
                message: `Couldn't find a user`
            } as UseCaseError);
        }
    }
}