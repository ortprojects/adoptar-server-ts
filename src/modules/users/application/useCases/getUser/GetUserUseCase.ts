import { GenericAppError } from '../../../../shared/core/AppError';
import { Either, left, Result, right } from '../../../../shared/core/Result';
import { UseCase } from '../../../../shared/domain/UseCase';
import { UserEntity } from '../../../domain/UserEntity';
import { UserRepository } from '../../../infrastructure/repositories/UserRepository';
import { GetUserDTO } from './dtos/GetUserDTO';
import { GetUserErrors } from './GetUserErrors';

type Response = Either<
    GetUserErrors.UserWithIdNotFoundError | GenericAppError.UnexpectedError,
    Result<UserEntity>
>;

export class GetUserUseCase implements UseCase<GetUserDTO, Promise<Response>> {
    private userRepositoy: UserRepository;

    constructor(userRepositoy: UserRepository) {
        this.userRepositoy = userRepositoy;
    }

    async execute(request: GetUserDTO): Promise<Response> {
        const { userId } = request;


        try {
            const userEntity = await this.userRepositoy.getUserByUserId(userId);

            if (!userEntity) {
                return left(new GetUserErrors.UserWithIdNotFoundError()) as Response;
            }

            return right(Result.ok<UserEntity>(userEntity));
        } catch (err) {
            return left(new GenericAppError.UnexpectedError(err)) as Response;
        }
    }
}
