import { userRepositoryImpl } from '../../../infrastructure/repositories';
import { GetUserController } from './GetUserController';
import { GetUserUseCase } from './GetUserUseCase';

const getUserUseCase = new GetUserUseCase(userRepositoryImpl);
const getUserContoller = new GetUserController(getUserUseCase);

export { getUserUseCase, getUserContoller };
