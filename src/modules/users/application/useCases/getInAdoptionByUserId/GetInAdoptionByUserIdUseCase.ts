import { PostEntity } from '../../../../posts/domain';
import { GenericAppError } from '../../../../shared/core/AppError';
import { Either, left, Result, right } from '../../../../shared/core/Result';
import { UseCase } from '../../../../shared/domain/UseCase';
import { UserRepository } from '../../../infrastructure/repositories/UserRepository';
import { GetInAdoptionByUserIdDTO } from './dtos/GetInAdoptionByUserId';
import { GetInAdoptionByUserIdErrors } from './GetInAdoptionByUserIdErrors';

type Response = Either<
    GetInAdoptionByUserIdErrors.UserWithIdNotFoundError | GenericAppError.UnexpectedError,
    Result<PostEntity[]>
>;

export class GetInAdoptionByUserIdUseCase
    implements UseCase<GetInAdoptionByUserIdDTO, Promise<Response>> {
    private userRepositoy: UserRepository;

    constructor(userRepositoy: UserRepository) {
        this.userRepositoy = userRepositoy;
    }

    async execute(request: GetInAdoptionByUserIdDTO): Promise<Response> {
        const { userId } = request;

        try {
            await this.userRepositoy.getUserByUserId(userId);
        } catch (error) {
            return left(new GetInAdoptionByUserIdErrors.UserWithIdNotFoundError()) as Response;
        }

        try {
            const posts = await this.userRepositoy.getInAdoptionByUserId(userId);

            return right(Result.ok<PostEntity[]>(posts));
        } catch (err) {
            return left(new GenericAppError.UnexpectedError(err)) as Response;
        }
    }
}
