import { PostEntity } from '../../../../posts/domain';
import { PostMapper } from '../../../../posts/mappers/PostMapper';
import { BaseController } from '../../../../shared/infrastructure/http/models/BaseController';
import { GetInAdoptionByUserIdDTO } from './dtos/GetInAdoptionByUserId';
import { GetInAdoptionByUserIdErrors } from './GetInAdoptionByUserIdErrors';
import { GetInAdoptionByUserIdUseCase } from './GetInAdoptionByUserIdUseCase';

export class GetInAdoptionByUserIdController extends BaseController {
    private useCase: GetInAdoptionByUserIdUseCase;

    constructor(useCase: GetInAdoptionByUserIdUseCase) {
        super();
        this.useCase = useCase;
    }

    async executeImpl(): Promise<any> {
        const { userId } = this.req.params;

        let dto: GetInAdoptionByUserIdDTO = {
            userId
        };

        try {
            const result = await this.useCase.execute(dto);

            if (result.isLeft()) {
                const error = result.value;

                switch (error.constructor) {
                    case GetInAdoptionByUserIdErrors.UserWithIdNotFoundError:
                        return this.conflict(error.errorValue().message);
                    default:
                        return this.fail(error.errorValue().message);
                }
            } else {
                const posts = result.value.getValue();
                return this.ok<PostEntity[]>(this.res, posts.map((p) => PostMapper.toJSON(p)));
            }
        } catch (err) {
            return this.fail(err);
        }
    }
}
