import { userRepositoryImpl } from '../../../infrastructure/repositories';
import { GetInAdoptionByUserIdController } from './GetInAdoptionByUserIdController';
import { GetInAdoptionByUserIdUseCase } from './GetInAdoptionByUserIdUseCase';


const getInAdoptionByUserIdUseCase = new GetInAdoptionByUserIdUseCase(userRepositoryImpl);
const getInAdoptionByUserIdContoller = new GetInAdoptionByUserIdController(getInAdoptionByUserIdUseCase);

export { getInAdoptionByUserIdUseCase, getInAdoptionByUserIdContoller };
