import { Result } from "../../../../shared/core/Result";
import { UseCaseError } from "../../../../shared/core/UseCaseError";

export namespace GetInterestedErrors {
    export class UserWithIdNotFoundError extends Result<UseCaseError> {
        constructor() {
            super(false, {
                message: `Couldn't find a user`
            } as UseCaseError);
        }
    }

    export class PostIdNotFoundError extends Result<UseCaseError> {
        constructor() {
            super(false, {
                message: `Couldn't find a post`
            } as UseCaseError);
        }
    }
}