import { postRepositoryImpl } from '../../../../posts/infrastructure/repositories';
import { userRepositoryImpl } from '../../../infrastructure/repositories';
import { GetInterestedController } from './GetInterestedController';
import { GetInterestedUseCase } from './GetInterestedUseCase';

const getInterestedUseCase = new GetInterestedUseCase(postRepositoryImpl, userRepositoryImpl);
const getInterestedContoller = new GetInterestedController(getInterestedUseCase);

export { getInterestedUseCase, getInterestedContoller };
