import { PostRepository } from '../../../../posts/infrastructure/repositories/PostRepository';
import { GenericAppError } from '../../../../shared/core/AppError';
import { Either, left, Result, right } from '../../../../shared/core/Result';
import { UseCase } from '../../../../shared/domain/UseCase';
import { UserEntity } from '../../../domain/UserEntity';
import { UserRepository } from '../../../infrastructure/repositories/UserRepository';
import { GetInterestedDTO } from './dtos/GetInterested';
import { GetInterestedErrors } from './GetInterestedErrors';

type Response = Either<
    GetInterestedErrors.UserWithIdNotFoundError | GenericAppError.UnexpectedError,
    Result<UserEntity[]>
>;

export class GetInterestedUseCase implements UseCase<GetInterestedDTO, Promise<Response>> {
    private postRepositoy: PostRepository;
    private userRepositoy: UserRepository;

    constructor(postRepositoy: PostRepository, userRepositoy: UserRepository) {
        this.postRepositoy = postRepositoy;
        this.userRepositoy = userRepositoy;
    }

    async execute(request: GetInterestedDTO): Promise<Response> {
        const { postId } = request;

        try {
            await this.postRepositoy.getPostById(postId);
        } catch (error) {
            return left(new GetInterestedErrors.PostIdNotFoundError()) as Response;
        }

        try {
            const users = await this.userRepositoy.getInterested(postId);

            return right(Result.ok<UserEntity[]>(users));
        } catch (err) {
            return left(new GenericAppError.UnexpectedError(err)) as Response;
        }
    }
}
