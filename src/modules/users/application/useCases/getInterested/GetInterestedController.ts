import { BaseController } from '../../../../shared/infrastructure/http/models/BaseController';
import { UserEntity } from '../../../domain/UserEntity';
import { UserMapper } from '../../../infrastructure/mappers/UserMapper';
import { GetInterestedDTO } from './dtos/GetInterested';
import { GetInterestedErrors } from './GetInterestedErrors';
import { GetInterestedUseCase } from './GetInterestedUseCase';


export class GetInterestedController extends BaseController {
    private useCase: GetInterestedUseCase;

    constructor(useCase: GetInterestedUseCase) {
        super();
        this.useCase = useCase;
    }

    async executeImpl(): Promise<any> {
        const { postId } = this.req.params;

        let dto: GetInterestedDTO = {
            postId
        };

        try {
            const result = await this.useCase.execute(dto);

            if (result.isLeft()) {
                const error = result.value;

                switch (error.constructor) {
                    case GetInterestedErrors.UserWithIdNotFoundError:
                        return this.conflict(error.errorValue().message);
                    default:
                        return this.fail(error.errorValue().message);
                }
            } else {
                const users = result.value.getValue();
                return this.ok<UserEntity[]>(this.res, users.map((user) => UserMapper.toJSON(user)));
            }
        } catch (err) {
            return this.fail(err);
        }
    }
}
