import { PostEntity } from '../../../../posts/domain';
import { GenericAppError } from '../../../../shared/core/AppError';
import { Either, left, Result, right } from '../../../../shared/core/Result';
import { UseCase } from '../../../../shared/domain/UseCase';
import { UserRepository } from '../../../infrastructure/repositories/UserRepository';
import { GetAdoptedByUserIdDTO } from './dtos/GetAdoptedByUserIdDTO';
import { GetAdoptedByUserIdErrors } from './GetAdoptedByUserIdErrors';

type Response = Either<
    GetAdoptedByUserIdErrors.UserWithIdNotFoundError | GenericAppError.UnexpectedError,
    Result<PostEntity[]>
>;

export class GetInterestedByUserIdUseCase
    implements UseCase<GetAdoptedByUserIdDTO, Promise<Response>> {
    private userRepositoy: UserRepository;

    constructor(userRepositoy: UserRepository) {
        this.userRepositoy = userRepositoy;
    }

    async execute(request: GetAdoptedByUserIdDTO): Promise<Response> {
        const { userId } = request;

        try {
            await this.userRepositoy.getUserByUserId(userId);
        } catch (error) {
            return left(new GetAdoptedByUserIdErrors.UserWithIdNotFoundError()) as Response;
        }

        try {
            const posts = await this.userRepositoy.getAdoptedByUserId(userId);

            return right(Result.ok<PostEntity[]>(posts));
        } catch (err) {
            return left(new GenericAppError.UnexpectedError(err)) as Response;
        }
    }
}
