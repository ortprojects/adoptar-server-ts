import { userRepositoryImpl } from '../../../infrastructure/repositories';
import { GetAdoptedByUserIdController } from './GetAdoptedByUserIdController';
import { GetInterestedByUserIdUseCase } from './GetAdoptedByUserIdUseCase';

const getAdoptedByUserIdUseCase = new GetInterestedByUserIdUseCase(userRepositoryImpl);
const getAdoptedByUserIdContoller = new GetAdoptedByUserIdController(getAdoptedByUserIdUseCase);

export { getAdoptedByUserIdUseCase, getAdoptedByUserIdContoller };
