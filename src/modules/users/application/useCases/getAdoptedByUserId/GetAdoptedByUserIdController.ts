import { PostEntity } from '../../../../posts/domain';
import { PostMapper } from '../../../../posts/mappers/PostMapper';
import { BaseController } from '../../../../shared/infrastructure/http/models/BaseController';
import { GetAdoptedByUserIdDTO } from './dtos/GetAdoptedByUserIdDTO';
import { GetAdoptedByUserIdErrors } from './GetAdoptedByUserIdErrors';
import { GetInterestedByUserIdUseCase } from './GetAdoptedByUserIdUseCase';

export class GetAdoptedByUserIdController extends BaseController {
    private useCase: GetInterestedByUserIdUseCase;

    constructor(useCase: GetInterestedByUserIdUseCase) {
        super();
        this.useCase = useCase;
    }

    async executeImpl(): Promise<any> {
        const { userId } = this.req.params;

        let dto: GetAdoptedByUserIdDTO = {
            userId
        };

        try {
            const result = await this.useCase.execute(dto);

            if (result.isLeft()) {
                const error = result.value;

                switch (error.constructor) {
                    case GetAdoptedByUserIdErrors.UserWithIdNotFoundError:
                        return this.conflict(error.errorValue().message);
                    default:
                        return this.fail(error.errorValue().message);
                }
            } else {
                const posts = result.value.getValue();
                return this.ok<PostEntity[]>(this.res, posts.map((p) => PostMapper.toJSON(p)));
            }
        } catch (err) {
            return this.fail(err);
        }
    }
}
