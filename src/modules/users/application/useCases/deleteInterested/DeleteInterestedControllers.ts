import { BaseController } from '../../../../shared/infrastructure/http/models/BaseController';
import { DeleteInterestedDTO } from './dtos/DeleteInterestedDTO';
import { DeleteInterestedUseCase } from './DeleteInterestedUseCase';
import { DeleteInterestedUserErrors } from './DeleteInterestedErrors';

export class DeleteInterestedController extends BaseController {
    private useCase: DeleteInterestedUseCase;

    constructor(useCase: DeleteInterestedUseCase) {
        super();
        this.useCase = useCase;
    }

    async executeImpl(): Promise<any> {
        const { postId, userId } = this.req.params;

        let dto: DeleteInterestedDTO = {
            postId,
            userId
        };

        try {
            const result = await this.useCase.execute(dto);

            if (result.isLeft()) {
                const error = result.value;

                switch (error.constructor) {
                    case DeleteInterestedUserErrors.PostByIdNotFoundError:
                        return this.conflict(error.errorValue().message);
                    default:
                        return this.fail(error.errorValue().message);
                }
            } else {
                return this.ok<void>(this.res);
            }
        } catch (err) {
            return this.fail(err);
        }
    }
}
