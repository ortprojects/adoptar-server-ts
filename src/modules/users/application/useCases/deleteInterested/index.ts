import { postRepositoryImpl } from '../../../../posts/infrastructure/repositories';
import { userRepositoryImpl } from '../../../infrastructure/repositories';
import { DeleteInterestedController } from './DeleteInterestedControllers';
import { DeleteInterestedUseCase } from './DeleteInterestedUseCase';

const deleteInterestedUseCase = new DeleteInterestedUseCase(userRepositoryImpl, postRepositoryImpl);
const deleteInterestedInContoller = new DeleteInterestedController(deleteInterestedUseCase);

export { deleteInterestedUseCase, deleteInterestedInContoller };
