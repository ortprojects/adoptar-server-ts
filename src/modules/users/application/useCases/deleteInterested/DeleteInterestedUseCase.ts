import { GenericAppError } from '../../../../shared/core/AppError';
import { Either, left, Result, right } from '../../../../shared/core/Result';
import { UseCase } from '../../../../shared/domain/UseCase';
import { UserRepository } from '../../../infrastructure/repositories/UserRepository';
import { UserDni } from '../../../domain/valueObjects/UserDni';
import { UserAddress } from '../../../domain/valueObjects/UserAddress';
import { UserPhoneNumber } from '../../../domain/valueObjects/UserPhoneNumber';
import { UserDisplayName } from '../../../domain/valueObjects/UserDisplayName';
import { UserPhotoURL } from '../../../domain/valueObjects/UserPhotoURL';
import { UserEntity } from '../../../domain/UserEntity';
import { DeleteInterestedUserErrors } from './DeleteInterestedErrors';
import { DeleteInterestedDTO } from './dtos/DeleteInterestedDTO';
import { PostRepository } from '../../../../posts/infrastructure/repositories/PostRepository';

type Response = Either<
    | DeleteInterestedUserErrors.PostByIdNotFoundError
    | GenericAppError.UnexpectedError
    | Result<any>,
    Result<void>
>;

export class DeleteInterestedUseCase implements UseCase<DeleteInterestedDTO, Promise<Response>> {
    private userRepositoy: UserRepository;
    private postRepository: PostRepository;

    constructor(userRepositoy: UserRepository, postRepository: PostRepository) {
        this.userRepositoy = userRepositoy;
        this.postRepository = postRepository;
    }

    async execute(request: DeleteInterestedDTO): Promise<Response> {
        try {
            await this.postRepository.getPostById(request.postId);
        } catch (error) {
            return left(new DeleteInterestedUserErrors.PostByIdNotFoundError()) as Response;
        }

        try {
            await this.userRepositoy.getUserByUserId(request.userId);
        } catch (error) {
            return left(new DeleteInterestedUserErrors.UserByIdNotFoundError()) as Response;
        }

        try {
            await this.userRepositoy.deleteUserFromInterested(request.postId, request.userId);
            await this.userRepositoy.deleteUserFromInterestedIn(request.postId, request.userId);
            return right(Result.ok<void>());
        } catch (error) {
            return left(new GenericAppError.UnexpectedError(error.message)) as Response;
        }
    }
}
