import { Result } from "../../../../shared/core/Result";
import { UseCaseError } from "../../../../shared/core/UseCaseError";

export namespace DeleteInterestedUserErrors {
    export class PostByIdNotFoundError extends Result<UseCaseError> {
        constructor() {
            super(false, {
                message: `Couldn't find a post`
            } as UseCaseError);
        }
    }

    export class UserByIdNotFoundError extends Result<UseCaseError> {
        constructor() {
            super(false, {
                message: `Couldn't find a user`
            } as UseCaseError);
        }
    }
}