import { GenericAppError } from '../../../../shared/core/AppError';
import { MailerRepository } from '../../../../shared/core/mailer/MailerRepository';
import { Either, left, Result, right } from '../../../../shared/core/Result';
import { UseCase } from '../../../../shared/domain/UseCase';
import { UniqueEntityID } from '../../../../shared/domain/valueObjects/UniqueEntityID';
import { UserEntity } from '../../../domain/UserEntity';
import { UserAddress } from '../../../domain/valueObjects/UserAddress';
import { UserDisplayName } from '../../../domain/valueObjects/UserDisplayName';
import { UserDni } from '../../../domain/valueObjects/UserDni';
import { UserEmail } from '../../../domain/valueObjects/UserEmail';
import { UserId } from '../../../domain/valueObjects/UserId';
import { UserPassword } from '../../../domain/valueObjects/UserPassword';
import { UserPhoneNumber } from '../../../domain/valueObjects/UserPhoneNumber';
import { UserPhotoURL } from '../../../domain/valueObjects/UserPhotoURL';
import { UserRepository } from '../../../infrastructure/repositories/UserRepository';
import { CreateUserErrors } from './CreateUserErrors';
import { CreateUserDTO } from './dtos/CreateUserDTO';

type Response = Either<
    CreateUserErrors.EmailAlreadyExistsError | GenericAppError.UnexpectedError | Result<any>,
    Result<void>
>;

export class CreateUserUseCase implements UseCase<CreateUserDTO, Promise<Response>> {
    private userRepositoy: UserRepository;
    private mailerRepository: MailerRepository;

    constructor(userRepositoy: UserRepository, mailerRepository: MailerRepository) {
        this.userRepositoy = userRepositoy;
        this.mailerRepository = mailerRepository;
    }

    async execute(request: CreateUserDTO): Promise<Response> {
        const emailOrError = UserEmail.create(request.email);
        const displayNameOrError = UserDisplayName.create();
        const dniOrError = UserDni.create();
        const addressOrError = UserAddress.create();
        const photoURLOrError = UserPhotoURL.create();
        const phoneNumberOrError = UserPhoneNumber.create();

        const combinedPropsResult = Result.combine([
            emailOrError,
            displayNameOrError,
            dniOrError,
            addressOrError,
            photoURLOrError,
            phoneNumberOrError
        ]);

        if (combinedPropsResult.isFailure) {
            return left(Result.fail<void>(combinedPropsResult.error)) as Response;
        }

        const id = new UniqueEntityID(request.id);

        const email: UserEmail = emailOrError.getValue();

        const userExists = await this.userRepositoy.exists(email.value);

        if (userExists) {
            return left(new CreateUserErrors.EmailAlreadyExistsError(email.value)) as Response;
        }

        const userOrError: Result<UserEntity> = UserEntity.create(
            {
                email: emailOrError.getValue(),
                address: addressOrError.getValue(),
                displayName: displayNameOrError.getValue(),
                dni: dniOrError.getValue(),
                phoneNumber: phoneNumberOrError.getValue(),
                photoURL: photoURLOrError.getValue(),
                isFirstTime: true
            },
            id
        );

        if (userOrError.isFailure) {
            return left(Result.fail<UserEntity>(userOrError.error.toString())) as Response;
        }

        try {
            const userEntity = userOrError.getValue();
            await this.userRepositoy.save(userEntity);

            this.mailerRepository.sendEmail(
                userEntity.email.value,
                'Nuevo usuario',
                'Bienvenido, te registraste exitosamente en nuestra plataforma. Esperamos que te guste!'
            );
            
            return right(Result.ok<void>());
        } catch (err) {
            return left(new GenericAppError.UnexpectedError(err.message)) as Response;
        }
    }
}
