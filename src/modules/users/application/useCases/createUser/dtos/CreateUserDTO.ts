export interface CreateUserDTO {
    email: string;
    id: string;
}
