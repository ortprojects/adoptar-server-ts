import { Result } from '../../../../shared/core/Result';
import { UseCaseError } from '../../../../shared/core/UseCaseError';

export namespace CreateUserErrors {
    export class EmailAlreadyExistsError extends Result<UseCaseError> {
        constructor(email: string) {
            super(false, `The email ${email} associated for this account already exists`);
        }
    }
}
