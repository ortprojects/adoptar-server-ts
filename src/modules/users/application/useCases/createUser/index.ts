import { CreateUserUseCase } from './CreateUserUseCase';
import { CreateUserController } from './CreateUserController';
import { userRepositoryImpl } from '../../../infrastructure/repositories';
import { mailerRepositoryImpl } from '../../../../shared/core/mailer';

const createUserUseCase = new CreateUserUseCase(userRepositoryImpl, mailerRepositoryImpl);
const createUserContoller = new CreateUserController(createUserUseCase);

export { createUserUseCase, createUserContoller };
