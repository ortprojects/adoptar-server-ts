import { userRepositoryImpl } from '../../../infrastructure/repositories';
import { GetInterestedInController } from './GetInterestedInController';
import { GetInterestedInUseCase } from './GetInterestedInUseCase';

const getInterestedInUseCase = new GetInterestedInUseCase(userRepositoryImpl);
const getInterestedInContoller = new GetInterestedInController(getInterestedInUseCase);

export { getInterestedInUseCase, getInterestedInContoller };
