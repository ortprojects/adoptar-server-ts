import { PostEntity } from '../../../../posts/domain';
import { GenericAppError } from '../../../../shared/core/AppError';
import { Either, left, Result, right } from '../../../../shared/core/Result';
import { UseCase } from '../../../../shared/domain/UseCase';
import { UserRepository } from '../../../infrastructure/repositories/UserRepository';
import { GetInterestedInDTO } from './dtos/GetInterestedInDTO';
import { GetInterestedInErrors } from './GetInterestedInErrors';

type Response = Either<
    GetInterestedInErrors.UserWithIdNotFoundError | GenericAppError.UnexpectedError,
    Result<PostEntity[]>
>;

export class GetInterestedInUseCase implements UseCase<GetInterestedInDTO, Promise<Response>> {
    private userRepositoy: UserRepository;

    constructor(userRepositoy: UserRepository) {
        this.userRepositoy = userRepositoy;
    }

    async execute(request: GetInterestedInDTO): Promise<Response> {
        const { userId } = request;

        try {
            await this.userRepositoy.getUserByUserId(userId);
        } catch (error) {
            return left(new GetInterestedInErrors.UserWithIdNotFoundError()) as Response;
        }

        try {
            const posts = await this.userRepositoy.getInterestedIn(userId);

            return right(Result.ok<PostEntity[]>(posts));
        } catch (err) {
            return left(new GenericAppError.UnexpectedError(err)) as Response;
        }
    }
}
