import { PostEntity } from '../../../../posts/domain';
import { PostMapper } from '../../../../posts/mappers/PostMapper';
import { BaseController } from '../../../../shared/infrastructure/http/models/BaseController';
import { GetInterestedInDTO } from './dtos/GetInterestedInDTO';
import { GetInterestedInErrors } from './GetInterestedInErrors';
import { GetInterestedInUseCase } from './GetInterestedInUseCase';

export class GetInterestedInController extends BaseController {
    private useCase: GetInterestedInUseCase;

    constructor(useCase: GetInterestedInUseCase) {
        super();
        this.useCase = useCase;
    }

    async executeImpl(): Promise<any> {
        const { userId } = this.req.params;

        let dto: GetInterestedInDTO = {
            userId
        };

        try {
            const result = await this.useCase.execute(dto);

            if (result.isLeft()) {
                const error = result.value;

                switch (error.constructor) {
                    case GetInterestedInErrors.UserWithIdNotFoundError:
                        return this.conflict(error.errorValue().message);
                    default:
                        return this.fail(error.errorValue().message);
                }
            } else {
                const posts = result.value.getValue();
                return this.ok<PostEntity[]>(this.res, posts.map((p) => PostMapper.toJSON(p)));
            }
        } catch (err) {
            return this.fail(err);
        }
    }
}
