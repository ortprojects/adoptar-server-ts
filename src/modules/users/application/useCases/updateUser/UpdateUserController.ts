import { BaseController } from '../../../../shared/infrastructure/http/models/BaseController';
import { UpdateUserDTO } from './dtos/UpdateUserDTO';
import { UpdateUserErrors } from './UpdateUserErrors';
import { UpdateUserUseCase } from './UpdateUserUseCase';

export class UpdateUserController extends BaseController {
    private useCase: UpdateUserUseCase;

    constructor(useCase: UpdateUserUseCase) {
        super();
        this.useCase = useCase;
    }

    async executeImpl(): Promise<any> {
        const dto: UpdateUserDTO = this.req.body as UpdateUserDTO;

        try {
            const result = await this.useCase.execute(dto);
            if (result.isLeft()) {
                const error = result.value;

                switch (error.constructor) {
                    case UpdateUserErrors.UserWithIdNotFoundError:
                        return this.conflict(error.errorValue());
                    default:
                        return this.fail(error.errorValue().message);
                }
            } else {
                return this.ok(this.res);
            }
        } catch (err) {
            return this.fail(err);
        }
    }
}
