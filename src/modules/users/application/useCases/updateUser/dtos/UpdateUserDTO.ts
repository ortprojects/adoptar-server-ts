export interface UpdateUserDTO {
    id: string;
    displayName: string;
    dni: string;
    address: {
        latitude: number;
        longitude: number;
    };
    phoneNumber: string;
}
