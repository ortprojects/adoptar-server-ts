import { Result } from "../../../../shared/core/Result";
import { UseCaseError } from "../../../../shared/core/UseCaseError";

export namespace UpdateUserErrors {
    export class UserWithIdNotFoundError extends Result<UseCaseError> {
        constructor() {
            super(false, `User not found`);
        }
    }
}