import { userRepositoryImpl } from '../../../infrastructure/repositories';
import { UpdateUserController } from './UpdateUserController';
import { UpdateUserUseCase } from './UpdateUserUseCase';

const updateUserUseCase = new UpdateUserUseCase(userRepositoryImpl);
const updateUserContoller = new UpdateUserController(updateUserUseCase);

export { updateUserUseCase, updateUserContoller};
