import { GenericAppError } from '../../../../shared/core/AppError';
import { Either, left, Result, right } from '../../../../shared/core/Result';
import { UseCase } from '../../../../shared/domain/UseCase';
import { UserRepository } from '../../../infrastructure/repositories/UserRepository';
import { UpdateUserErrors } from './UpdateUserErrors';
import { UpdateUserDTO } from './dtos/UpdateUserDTO';
import { UserDni } from '../../../domain/valueObjects/UserDni';
import { UserAddress } from '../../../domain/valueObjects/UserAddress';
import { UserPhoneNumber } from '../../../domain/valueObjects/UserPhoneNumber';
import { UserDisplayName } from '../../../domain/valueObjects/UserDisplayName';
import { UserPhotoURL } from '../../../domain/valueObjects/UserPhotoURL';
import { UserEntity } from '../../../domain/UserEntity';

type Response = Either<
    UpdateUserErrors.UserWithIdNotFoundError | GenericAppError.UnexpectedError | Result<any>,
    Result<void>
>;

export class UpdateUserUseCase implements UseCase<UpdateUserDTO, Promise<Response>> {
    private userRepositoy: UserRepository;

    constructor(userRepositoy: UserRepository) {
        this.userRepositoy = userRepositoy;
    }

    async execute(request: UpdateUserDTO): Promise<Response> {
        const userId = request.id;
        let userEntity: UserEntity;

        try {
            userEntity = await this.userRepositoy.getUserByUserId(userId);
        } catch (error) {
            return left(new UpdateUserErrors.UserWithIdNotFoundError()) as Response;
        }

        const displayNameOrError = UserDisplayName.create(request.displayName);
        const addressOrError = UserAddress.create({
            latitude: request.address.latitude,
            longitude: request.address.longitude
        });
        const dniOrError = UserDni.create(request.dni);
        const phoneNumberOrError = UserPhoneNumber.create(request.phoneNumber);

        const userOrError: Result<UserEntity> = UserEntity.create(
            {
                email: userEntity.email,
                address: addressOrError.getValue(),
                displayName: displayNameOrError.getValue(),
                dni: dniOrError.getValue(),
                phoneNumber: phoneNumberOrError.getValue(),
                photoURL: userEntity.photoURL,
                isFirstTime: false
            },
            userEntity.id
        );

        if (userOrError.isFailure) {
            return left(Result.fail<UserEntity>(userOrError.error.toString())) as Response;
        }

        try {
            await this.userRepositoy.updateUser(userOrError.getValue());

            return right(Result.ok<void>());
        } catch (error) {
            return left(new GenericAppError.UnexpectedError(error.message)) as Response;
        }
    }
}
