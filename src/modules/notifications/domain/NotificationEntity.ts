import { AggregateRoot } from '../../shared/domain/AggregateRoot';
import { UniqueEntityID } from '../../shared/domain/valueObjects/UniqueEntityID';
import { Result } from '../../shared/core/Result';
import { Guard } from '../../shared/core/Guard';
import { NotificationMessage } from './valueObjects/NotificationMessage';

interface NotificationEntityProps {
    message: NotificationMessage;
    userId: UniqueEntityID;
    createdAt: Date;
    isRead: boolean;
}

export class NotificationEntity extends AggregateRoot<NotificationEntityProps> {
    get id(): UniqueEntityID {
        return this._id;
    }

    get message(): NotificationMessage {
        return this.props.message;
    }

    get userId(): UniqueEntityID {
        return this.props.userId;
    }

    get createdAt(): Date {
        return this.props.createdAt;
    }

    get isRead(): boolean {
        return this.props.isRead;
    }

    private constructor(props: NotificationEntityProps, id?: UniqueEntityID) {
        super(props, id);
    }

    public static create(
        props: NotificationEntityProps,
        id?: UniqueEntityID
    ): Result<NotificationEntity> {
        const guardedProps = [
            { argument: props.userId, argumentName: 'userId' },
            { argument: props.message, argumentName: 'message' },
            { argument: props.createdAt, argumentName: 'createdAt' },
            { argument: props.isRead, argumentName: 'isRead' }
        ];

        const guardResult = Guard.againstNullOrUndefinedBulk(guardedProps);

        if (!guardResult.succeeded) {
            return Result.fail<NotificationEntity>(guardResult.message);
        } else {
            const question = new NotificationEntity(
                {
                    ...props,
                    userId: props.userId,
                    message: props.message,
                    createdAt: props.createdAt,
                    isRead: props.isRead
                },
                id
            );

            return Result.ok<NotificationEntity>(question);
        }
    }
}
