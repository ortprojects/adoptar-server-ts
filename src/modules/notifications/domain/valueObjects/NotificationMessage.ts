import { Guard } from "../../../shared/core/Guard";
import { Result } from "../../../shared/core/Result";
import { ValueObject } from "../../../shared/domain/ValueObject";


interface NotificationMessageProps {
    value: string;
}

export class NotificationMessage extends ValueObject<NotificationMessageProps> {
    get value(): string {
        return this.props.value;
    }

    private constructor(props: NotificationMessageProps) {
        super(props);
    }

    public static create(message: string): Result<NotificationMessage> {
        const guardResult = Guard.againstNullOrUndefined(message, 'message');

        if (!guardResult.succeeded) {
            return Result.fail<NotificationMessage>(guardResult.message);
        } else {
            return Result.ok<NotificationMessage>(new NotificationMessage({ value: message }));
        }
    }
}
