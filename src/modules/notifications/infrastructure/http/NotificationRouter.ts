import { Request, Response } from 'express';
import express from 'express';
import { getNotificationsByUserIdContoller } from '../../application/getNotificationsByUserId';
import { RouterPath } from '../../../shared/infrastructure/http/models/RouterPath';
import { LoggerImpl } from '../../../shared/infrastructure';
import { updateNotificationByIdContoller } from '../../application/updateNotificationsById';
import { deleteNotificationByIdContoller } from '../../application/deleteNotificationById';

export class NotificationRouter {
    private static readonly routePath = '/notifications';

    public static create(): RouterPath {
        LoggerImpl.writeInfoLog('[NotificationRouter::create] Created notification routes.');

        const notificationRouter = express.Router();

        notificationRouter.get('/', (req: Request, res: Response) => {
            getNotificationsByUserIdContoller.execute(req, res);
        });

        notificationRouter.put('/:notificationId', (req: Request, res: Response) => {
            updateNotificationByIdContoller.execute(req, res);
        });

        notificationRouter.delete('/:notificationId', (req: Request, res: Response) => {
            deleteNotificationByIdContoller.execute(req, res);
        });

        return { router: notificationRouter, path: this.routePath };
    }
}
