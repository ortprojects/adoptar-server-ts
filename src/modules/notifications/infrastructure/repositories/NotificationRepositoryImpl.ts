import { userRepositoryImpl } from '../../../users/infrastructure/repositories';
import { NotificationEntity } from '../../domain/NotificationEntity';
import { NotificationMapper } from '../mappers/NotificationMapper';
import { NotificationRepository } from './NotificationRepository';

export class NotificationRepositoryImpl implements NotificationRepository {
    private firestore: FirebaseFirestore.Firestore;
    private notificationsCollectionPath = '/notifications';

    constructor(firestore: FirebaseFirestore.Firestore) {
        this.firestore = firestore;
    }

    async deleteNotification(notificationId: string): Promise<boolean> {
        const notificationsCollection = this.firestore.collection(this.notificationsCollectionPath);

        await notificationsCollection
            .doc(notificationId)
            .delete()

        return true;
    }

    async getNotificationsById(notificationId: string): Promise<NotificationEntity> {
        const notificationsCollection = this.firestore.collection(this.notificationsCollectionPath);

        const notification = await notificationsCollection.doc(notificationId).get();

        return NotificationMapper.toDomain(notification.data(), notification.id);
    }

    async updateNotification(notification: NotificationEntity): Promise<boolean> {
        const notificationsCollection = this.firestore.collection(this.notificationsCollectionPath);

        await notificationsCollection
            .doc(notification.id.toString())
            .update(NotificationMapper.toPersistence(notification));

        return true;
    }

    async addNotification(notification: NotificationEntity): Promise<boolean> {
        const notificationCollection = this.firestore.collection(this.notificationsCollectionPath);

        await notificationCollection.doc(notification.id.toString()).set(NotificationMapper.toPersistence(notification));

        return true;
    }

    async getNotificationsByUserId(userId: string): Promise<NotificationEntity[]> {
        const notificationCollectionPath = this.firestore.collection(
            this.notificationsCollectionPath
        );

        const notifications = await notificationCollectionPath
            .where('userId', '==', userId)
            .get();

        const notificationsRet: NotificationEntity[] = [];

        for (const doc of notifications.docs) {
            const notificationEntity = NotificationMapper.toDomain({ ...doc.data() }, doc.id);

            if (notificationEntity != null) {
                notificationsRet.push(notificationEntity);
            }
        }

        return notificationsRet;
    }
}
