import { NotificationEntity } from "../../domain/NotificationEntity";

export interface NotificationRepository{
    getNotificationsByUserId(userId: string): Promise<NotificationEntity[]>;
    getNotificationsById(notificationId: string): Promise<NotificationEntity>;
    addNotification(notification: NotificationEntity): Promise<boolean>;
    updateNotification(notification: NotificationEntity): Promise<boolean>;
    deleteNotification(notificationId: string): Promise<boolean>;
}