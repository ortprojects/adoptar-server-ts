import { FirebaseService } from '../../../shared/core/firebase/FirebaseService';
import { NotificationRepository } from './NotificationRepository';
import { NotificationRepositoryImpl } from './NotificationRepositoryImpl';

const firestore = FirebaseService.getFirestoreReference();

export const notificationRepositoryImpl: NotificationRepository = new NotificationRepositoryImpl(
    firestore
);
