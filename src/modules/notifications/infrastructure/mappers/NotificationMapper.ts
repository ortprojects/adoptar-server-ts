import { UniqueEntityID } from '../../../shared/domain/valueObjects/UniqueEntityID';
import { BaseMapper } from '../../../shared/infrastructure/mappers/BaseMapper';
import { UserMapper } from '../../../users/infrastructure/mappers/UserMapper';
import { NotificationEntity } from '../../domain/NotificationEntity';
import { NotificationMessage } from '../../domain/valueObjects/NotificationMessage';

export class NotificationMapper implements BaseMapper<NotificationEntity> {
    public static toPersistence(notificationEntity: NotificationEntity): any {
        const notification = {
            id: notificationEntity.id.toString(),
            userId: notificationEntity.userId.toString(),
            message: notificationEntity.message.value,
            createdAt: notificationEntity.createdAt,
            isRead: notificationEntity.isRead
        };

        return notification;
    }

    public static toDomain(raw: any, id?: string): NotificationEntity | null {
        const messageOrError = NotificationMessage.create(raw.message);
        const userIdError = new UniqueEntityID(raw.userId);
        const createdAt = raw.createdAt.toDate();
        const isRead = raw.isRead;

        const notificationOrError = NotificationEntity.create(
            {
                userId: userIdError,
                createdAt,
                message: messageOrError.getValue(),
                isRead
            },
            new UniqueEntityID(id)
        );

        return notificationOrError.isSuccess ? notificationOrError.getValue() : null;
    }

    public static toJSON(raw: NotificationEntity): any {
        return {
            id: raw.id.toString(),
            message: raw.message.value,
            userId: raw.userId.toString(),
            createdAt: raw.createdAt,
            isRead: raw.isRead
        };
    }
}
