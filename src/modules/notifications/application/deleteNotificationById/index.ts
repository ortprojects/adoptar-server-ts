import { notificationRepositoryImpl } from '../../infrastructure/repositories';
import { DeleteNotificationByIdController } from './DeleteNotificationByIdController';
import { DeleteNotificationByIdUseCase } from './DeleteNotificationByIdUseCase';

const deleteNotificationByIdUseCase = new DeleteNotificationByIdUseCase(notificationRepositoryImpl);
const deleteNotificationByIdContoller = new DeleteNotificationByIdController(deleteNotificationByIdUseCase);

export { deleteNotificationByIdUseCase, deleteNotificationByIdContoller };
