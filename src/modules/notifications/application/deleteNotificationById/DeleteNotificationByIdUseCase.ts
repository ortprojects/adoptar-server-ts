import { GenericAppError } from '../../../shared/core/AppError';
import { Either, left, Result, right } from '../../../shared/core/Result';
import { UseCase } from '../../../shared/domain/UseCase';
import { NotificationEntity } from '../../domain/NotificationEntity';
import { NotificationRepository } from '../../infrastructure/repositories/NotificationRepository';
import { DeleteNotificationByIdErrors } from './DeleteNotificationByIdErrors';
import { DeleteNotificationByIdDTO } from './dtos/DeleteNotificationByIdDTO';

type Response = Either<
    GenericAppError.UnexpectedError | DeleteNotificationByIdErrors.NotificationByIdNotFound,
    Result<void>
>;

export class DeleteNotificationByIdUseCase
    implements UseCase<DeleteNotificationByIdDTO, Promise<Response>> {
    private notificationRepositoy: NotificationRepository;

    constructor(notificationRepositoy: NotificationRepository) {
        this.notificationRepositoy = notificationRepositoy;
    }

    async execute(req: DeleteNotificationByIdDTO): Promise<Response> {
        let notificationEntity;

        try {
            notificationEntity = await this.notificationRepositoy.getNotificationsById(
                req.notificationId
            );
        } catch (error) {
            return left(new DeleteNotificationByIdErrors.NotificationByIdNotFound()) as Response;
        }

        try {
            await this.notificationRepositoy.deleteNotification(req.notificationId);
        } catch (err) {
            return left(new GenericAppError.UnexpectedError(err)) as Response;
        }

        return right(Result.ok<void>());
    }
}
