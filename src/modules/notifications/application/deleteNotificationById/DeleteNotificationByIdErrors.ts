import { Result } from "../../../shared/core/Result";
import { UseCaseError } from "../../../shared/core/UseCaseError";

export namespace DeleteNotificationByIdErrors {
    export class NotificationByIdNotFound extends Result<UseCaseError> {
        constructor() {
            super(false, {
                message: `Couldn't find a notification`
            } as UseCaseError);
        }
    }
}
