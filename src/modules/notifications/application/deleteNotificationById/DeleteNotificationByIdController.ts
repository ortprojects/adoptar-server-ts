import { BaseController } from '../../../shared/infrastructure/http/models/BaseController';
import { DeleteNotificationByIdErrors } from './DeleteNotificationByIdErrors';
import { DeleteNotificationByIdUseCase } from './DeleteNotificationByIdUseCase';
import { DeleteNotificationByIdDTO } from './dtos/DeleteNotificationByIdDTO';

export class DeleteNotificationByIdController extends BaseController {
    private useCase: DeleteNotificationByIdUseCase;

    constructor(useCase: DeleteNotificationByIdUseCase) {
        super();
        this.useCase = useCase;
    }

    async executeImpl(): Promise<any> {
        
        const { notificationId } = this.req.params;

        let dto: DeleteNotificationByIdDTO = {
            notificationId
        };

        try {
            const result = await this.useCase.execute(dto);

            if (result.isLeft()) {
                const error = result.value;

                switch (error.constructor) {
                    case DeleteNotificationByIdErrors.NotificationByIdNotFound:
                        return this.conflict(error.errorValue().message);
                    default:
                        return this.fail(error.errorValue().message);
                }
            } else {
                return this.ok(this.res);
            }
        } catch (err) {
            return this.fail(err);
        }
    }
}
