import { notificationRepositoryImpl } from '../../infrastructure/repositories';
import { UpdateNotificationByIdController } from './UpdateNotificationByIdController';
import { UpdateNotificationByIdUseCase } from './UpdateNotificationByIdUseCase';

const updateNotificationByIdUseCase = new UpdateNotificationByIdUseCase(notificationRepositoryImpl);
const updateNotificationByIdContoller = new UpdateNotificationByIdController(updateNotificationByIdUseCase);

export { updateNotificationByIdUseCase, updateNotificationByIdContoller };
