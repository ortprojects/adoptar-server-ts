import { GenericAppError } from '../../../shared/core/AppError';
import { Either, left, Result, right } from '../../../shared/core/Result';
import { UseCase } from '../../../shared/domain/UseCase';
import { NotificationEntity } from '../../domain/NotificationEntity';
import { NotificationRepository } from '../../infrastructure/repositories/NotificationRepository';
import { UpdateNotificationByIdDTO } from './dtos/UpdateNotificationByIdDTO';
import { UpdateNotificationByIdErrors } from './UpdateNotificationByIdErrors';

type Response = Either<
    GenericAppError.UnexpectedError | UpdateNotificationByIdErrors.NotificationByIdNotFound,
    Result<void>
>;

export class UpdateNotificationByIdUseCase
    implements UseCase<UpdateNotificationByIdDTO, Promise<Response>> {
    private notificationRepositoy: NotificationRepository;

    constructor(notificationRepositoy: NotificationRepository) {
        this.notificationRepositoy = notificationRepositoy;
    }

    async execute(req: UpdateNotificationByIdDTO): Promise<Response> {
        let notificationEntity;

        try {
            notificationEntity = await this.notificationRepositoy.getNotificationsById(
                req.noticationId
            );
        } catch (error) {
            return left(new UpdateNotificationByIdErrors.NotificationByIdNotFound()) as Response;
        }

        const notificationOrError = NotificationEntity.create(
            {
                createdAt: notificationEntity.createdAt,
                isRead: req.isRead,
                message: notificationEntity.message,
                userId: notificationEntity.userId
            },
            notificationEntity.id
        );

        if (notificationOrError.isFailure) {
            return left(
                Result.fail<NotificationEntity>(notificationOrError.error.toString())
            ) as Response;
        }

        try {
            await this.notificationRepositoy.updateNotification(notificationOrError.getValue());
        } catch (err) {
            return left(new GenericAppError.UnexpectedError(err)) as Response;
        }

        return right(Result.ok<void>());
    }
}
