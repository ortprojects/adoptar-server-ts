import { BaseController } from '../../../shared/infrastructure/http/models/BaseController';
import { UpdateNotificationByIdDTO } from './dtos/UpdateNotificationByIdDTO';
import { UpdateNotificationByIdErrors } from './UpdateNotificationByIdErrors';
import { UpdateNotificationByIdUseCase } from './UpdateNotificationByIdUseCase';

export class UpdateNotificationByIdController extends BaseController {
    private useCase: UpdateNotificationByIdUseCase;

    constructor(useCase: UpdateNotificationByIdUseCase) {
        super();
        this.useCase = useCase;
    }

    async executeImpl(): Promise<any> {
        const dto: UpdateNotificationByIdDTO = this.req.body;

        const { notificationId } = this.req.params;

        dto.noticationId = notificationId;

        try {
            const result = await this.useCase.execute(dto);

            if (result.isLeft()) {
                const error = result.value;

                switch (error.constructor) {
                    case UpdateNotificationByIdErrors.NotificationByIdNotFound:
                        return this.conflict(error.errorValue().message);
                    default:
                        return this.fail(error.errorValue().message);
                }
            } else {
                return this.created(this.res);
            }
        } catch (err) {
            return this.fail(err);
        }
    }
}
