export interface UpdateNotificationByIdDTO{
    noticationId: string;
    isRead: boolean;
}