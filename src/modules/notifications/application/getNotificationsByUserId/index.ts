import { postRepositoryImpl } from "../../../posts/infrastructure/repositories";
import { mailerRepositoryImpl } from "../../../shared/core/mailer";
import { userRepositoryImpl } from "../../../users/infrastructure/repositories";
import { notificationRepositoryImpl } from "../../infrastructure/repositories";
import { GetNotificationsByUserIdController } from "./GetNotificationsByUserIdController";

import { GetNotificationsByUserIdUseCase } from "./GetNotificationsByUserIdUseCase";

const getNotificationsByUserIdUseCase = new GetNotificationsByUserIdUseCase(userRepositoryImpl, notificationRepositoryImpl);
const getNotificationsByUserIdContoller = new GetNotificationsByUserIdController(getNotificationsByUserIdUseCase);

export { getNotificationsByUserIdUseCase, getNotificationsByUserIdContoller };
