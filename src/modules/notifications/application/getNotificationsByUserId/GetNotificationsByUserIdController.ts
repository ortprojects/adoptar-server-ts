import { BaseController } from '../../../shared/infrastructure/http/models/BaseController';
import { GetNotificationsByUserIdUseCase } from './GetNotificationsByUserIdUseCase';
import { GetNotificationsByUserIdDTO } from './dtos/GetNotificationsByUserIdDTO';
import { GetNotificationsByUserIdErrors } from './GetNotificationsByUserIdErrors';
import { NotificationEntity } from '../../domain/NotificationEntity';
import { NotificationMapper } from '../../infrastructure/mappers/NotificationMapper';

export class GetNotificationsByUserIdController extends BaseController {
    private useCase: GetNotificationsByUserIdUseCase;

    constructor(useCase: GetNotificationsByUserIdUseCase) {
        super();
        this.useCase = useCase;
    }

    async executeImpl(): Promise<any> {
        const { userId } = this.req.query;

        const dto: GetNotificationsByUserIdDTO = {
            userId: userId as string
        };

        try {
            const result = await this.useCase.execute(dto);

            if (result.isLeft()) {
                const error = result.value;

                switch (error.constructor) {
                    case GetNotificationsByUserIdErrors.UserByIdNotFound:
                        return this.conflict(error.errorValue().message);
                    default:
                        return this.fail(error.errorValue().message);
                }
            } else {
                const notifications = result.value.getValue();

                return this.ok<NotificationEntity[]>(
                    this.res,
                    notifications.map((n) => NotificationMapper.toJSON(n))
                );
            }
        } catch (err) {
            return this.fail(err);
        }
    }
}
