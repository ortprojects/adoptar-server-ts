import { Either, Result, left, right } from '../../../shared/core/Result';
import { UseCase } from '../../../shared/domain/UseCase';
import { GenericAppError } from '../../../shared/core/AppError';
import { PostRepository } from '../../../posts/infrastructure/repositories/PostRepository';
import { UserRepository } from '../../../users/infrastructure/repositories/UserRepository';
import { PostStateType } from '../../../posts/domain/enums/PostStateType';
import { MailerRepository } from '../../../shared/core/mailer/MailerRepository';
import { GetNotificationsByUserIdDTO } from './dtos/GetNotificationsByUserIdDTO';
import { GetNotificationsByUserIdErrors } from './GetNotificationsByUserIdErrors';
import { NotificationRepository } from '../../infrastructure/repositories/NotificationRepository';
import { NotificationEntity } from '../../domain/NotificationEntity';

type Response = Either<
    GenericAppError.UnexpectedError | GetNotificationsByUserIdErrors.UserByIdNotFound,
    Result<NotificationEntity[]>
>;

export class GetNotificationsByUserIdUseCase
    implements UseCase<GetNotificationsByUserIdDTO, Promise<Response>> {
    private userRepositoy: UserRepository;
    private notificationRepositoy: NotificationRepository;

    constructor(userRepository: UserRepository, notificationRepositoy: NotificationRepository) {
        this.userRepositoy = userRepository;
        this.notificationRepositoy = notificationRepositoy;
    }

    async execute(req: GetNotificationsByUserIdDTO): Promise<Response> {
        try {
            await this.userRepositoy.getUserByUserId(req.userId);
        } catch (error) {
            return left(new GetNotificationsByUserIdErrors.UserByIdNotFound()) as Response;
        }

        try {
            const notifications = await this.notificationRepositoy.getNotificationsByUserId(
                req.userId
            );

            return right(Result.ok<NotificationEntity[]>(notifications));
        } catch (err) {
            return left(new GenericAppError.UnexpectedError(err)) as Response;
        }
    }
}
