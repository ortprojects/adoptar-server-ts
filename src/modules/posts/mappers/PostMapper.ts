import { UniqueEntityID } from '../../shared/domain/valueObjects/UniqueEntityID';
import { PostEntity } from '../domain';
import { PostPetName } from '../domain/valueObjects/PostPetName';
import { PostPetSex } from '../domain/valueObjects/PostPetSex';
import { PostPetType } from '../domain/valueObjects/PostPetType';
import { PostType } from '../domain/valueObjects/PostType';
import { PostPetBreed } from '../domain/valueObjects/PostPetBreed';
import { PostPetAge } from '../domain/valueObjects/PostPetAge';
import { PostPetSize } from '../domain/valueObjects/PostPetSize';
import { PostPetWeigth } from '../domain/valueObjects/PostPetWeigth';
import { PostPetCastrated } from '../domain/valueObjects/PostPetCastrated';
import { PostDescription } from '../domain/valueObjects/PostDescription';
import { PostPetMonitoring } from '../domain/valueObjects/PostPetMonitoring';
import { PostGeohash } from '../domain/valueObjects/PostGeohash';
import { PostDate } from '../domain/valueObjects/PostDate';
import { PostCreatedByUserId } from '../domain/valueObjects/PostCreatedByUserId';
import { UserMapper } from '../../users/infrastructure/mappers/UserMapper';
import { PostTitle } from '../domain/valueObjects/PostTitle';
import { PostState } from '../domain/valueObjects/PostState';

export class PostMapper {
    public static toPersistence(post: PostEntity): any {
        return {
            id: post.id.toString(),
            petName: post.petName.value,
            petSex: post.petSex.value,
            petType: post.petType.value,
            postType: post.postType.value,
            petBreed: post.petBreed.value,
            petAge: post.petAge.value,
            petSize: post.petSize.value,
            petWeigth: post.petWeigth.value,
            petCastrated: post.petCastrated.value,
            postDescription: post.postDescription.value,
            petMonitoring: post.petMonitoring.value,
            imageFiles: post.imageFiles,
            createdAt: post.createdAt.value,
            geohash: post.geohash.value,
            createdByUserId: post.createdByUserId.value,
            title: post.postTitle.value,
            state: post.state.value,
            latitude: post.latitude,
            longitude: post.longitude,
            adoptedByUserId: post.adoptedByUserId ? post.adoptedByUserId.toString() : null
        };
    }

    public static toDomain(raw: any, id?: string): PostEntity | null {
        const postPetNameOrError = PostPetName.create(raw.petName);
        const postPetAgeOrError = PostPetAge.create(raw.petAge);
        const postTypeOrError = PostType.create(raw.postType);
        const postPetSexOrError = PostPetSex.create(raw.petSex);
        const postPetTypeOrError = PostPetType.create(raw.petType);
        const postPetBreedOrError = PostPetBreed.create(raw.petBreed);
        const postPetSizeOrError = PostPetSize.create(raw.petSize);
        const postPetWeigth = PostPetWeigth.create(raw.petWeigth);
        const postPetCastratedOrError = PostPetCastrated.create(raw.petCastrated);
        const postDescriptionOrError = PostDescription.create(raw.postDescription);
        const petMonitoringOrError = PostPetMonitoring.create(raw.petMonitoring);
        const postGeohashOrError = PostGeohash.createFromExisting(raw.geohash);
        const postDateOrError = PostDate.create(raw.createdAt.toDate());
        const postCreatedByUserIdOrError = PostCreatedByUserId.create(raw.createdByUserId);
        const postTitleOrError = PostTitle.create(raw.title);
        const postStateOrError = PostState.create(raw.state)
        const imageFiles = raw.imageFiles;

        const postOrError = PostEntity.create(
            {
                petName: postPetNameOrError.getValue(),
                petSex: postPetSexOrError.getValue(),
                petType: postPetTypeOrError.getValue(),
                postType: postTypeOrError.getValue(),
                petBreed: postPetBreedOrError.getValue(),
                petAge: postPetAgeOrError.getValue(),
                petSize: postPetSizeOrError.getValue(),
                petWeigth: postPetWeigth.getValue(),
                petCastrated: postPetCastratedOrError.getValue(),
                postDescription: postDescriptionOrError.getValue(),
                petMonitoring: petMonitoringOrError.getValue(),
                imageFiles: imageFiles,
                createdAt: postDateOrError.getValue(),
                geohash: postGeohashOrError.getValue(),
                createdByUserId: postCreatedByUserIdOrError.getValue(),
                createdBy: raw.createdBy,
                postTitle: postTitleOrError.getValue(),
                state: postStateOrError.getValue(),
                adoptedBy: raw.adoptedBy,
                adoptedByUserId: raw.adoptedByUserId,
                latitude: raw.latitude,
                longitude: raw.longitude
            },
            new UniqueEntityID(id)
        );

     

        return postOrError.isSuccess ? postOrError.getValue() : null;
    }

    public static toJSON(raw: PostEntity): any {
        let rawUser = {};
        let adoptedBy = {};

        if (raw.props.createdBy) {
            rawUser = UserMapper.toJSON(raw.props.createdBy);
        }

        if (raw.props.adoptedBy?.id) {
            adoptedBy = UserMapper.toJSON(raw.props.adoptedBy);
        }

        return {
            id: raw.id.toString(),
            petName: raw.props.petName.value,
            petSex: raw.props.petSex.value,
            petType: raw.props.petType.value,
            postType: raw.props.postType.value,
            petBreed: raw.props.petBreed.value,
            petAge: raw.props.petAge.value,
            petSize: raw.props.petSize.value,
            petWeigth: raw.props.petWeigth.value,
            petCastrated: raw.props.petCastrated.value,
            postDescription: raw.props.postDescription.value,
            petMonitoring: raw.props.petMonitoring.value,
            imageFiles: raw.props.imageFiles,
            createdAt: raw.props.createdAt.value,
            geohash: raw.props.geohash.value,
            title: raw.props.postTitle.value,
            state: raw.props.state.value,
            createdBy: rawUser,
            latitude: raw.props.latitude,
            longitude: raw.props.longitude,
            adoptedBy
        };
    }
}
