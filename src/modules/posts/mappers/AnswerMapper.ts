import { UniqueEntityID } from '../../shared/domain/valueObjects/UniqueEntityID';
import { UserMapper } from '../../users/infrastructure/mappers/UserMapper';
import { AnswerEntity } from '../domain/AnswerEntity';
import { AnswerMessage } from '../domain/valueObjects/answer/AnswerMessage';

export class AnswerMapper {
    public static toPersistence(answer: AnswerEntity): any {
        return {
            message: answer.message.value,
            createdAt: answer.createdAt,
            createdByUserId: answer.createdByUserId.toString()
        };
    }

    public static toDomain(raw: any, id?: string): AnswerEntity | null {
        const messageOrError = AnswerMessage.create(raw.message);
        const createdByUserId = new UniqueEntityID(raw.createdByUserId);
        const createdAt = raw.createdAt.toDate();
        const createdBy = raw.createdBy;

        const answerOrError = AnswerEntity.create(
            {
                message: messageOrError.getValue(),
                createdAt,
                createdByUserId,
                createdBy
            },
            new UniqueEntityID(id)
        );

        return answerOrError.isSuccess ? answerOrError.getValue() : null;
    }

    public static toJSON(raw: AnswerEntity): any {
        let rawUser;
        if (raw.props.createdBy) {
            rawUser = UserMapper.toJSON(raw.props.createdBy);
        }

        return {
            message: raw.props.message.value,
            createdAt: raw.props.createdAt,
            ...(rawUser ? { createdBy: rawUser } : {})
        };
    }
}
