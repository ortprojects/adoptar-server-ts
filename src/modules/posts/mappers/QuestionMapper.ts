import { UniqueEntityID } from '../../shared/domain/valueObjects/UniqueEntityID';
import { UserMapper } from '../../users/infrastructure/mappers/UserMapper';
import { QuestionEntity } from '../domain/QuestionEntity';
import { QuestionText } from '../domain/valueObjects/question/QuestionText';
import { AnswerMapper } from './AnswerMapper';

export class QuestionMapper {
    public static toPersistence(question: QuestionEntity): any {
        return {
            id: question.id.toString(),
            text: question.text.value,
            createdAt: question.createdAt,
            createdByUserId: question.createdByUserId.toString(),
            postId: question.postId.toString()
        };
    }

    public static toDomain(raw: any, id?: string): QuestionEntity | null {
        const textOrError = QuestionText.create(raw.text);
        const createdByUserId = new UniqueEntityID(raw.createdByUserId);
        const postId = new UniqueEntityID(raw.postId);
        const createdAt = raw.createdAt.toDate();
        const createdBy = raw.createdBy;
        const answer = raw.answer;

        const questionOrError = QuestionEntity.create(
            {
                text: textOrError.getValue(),
                createdAt,
                createdByUserId,
                postId,
                createdBy,
                answer
            },
            new UniqueEntityID(id)
        );

        return questionOrError.isSuccess ? questionOrError.getValue() : null;
    }

    public static toJSON(raw: QuestionEntity): any {
        let rawUser;
        let rawAnswer;

        if (raw.props.createdBy) {
            rawUser = UserMapper.toJSON(raw.props.createdBy);
        }

        if (raw.props.answer) {
            rawAnswer = AnswerMapper.toJSON(raw.props.answer);
        }

        return {
            id: raw.id.toString(),
            text: raw.props.text.value,
            createdAt: raw.props.createdAt,
            ...(rawAnswer ? { answer: rawAnswer } : {}),
            ...(rawUser ? { createdBy: rawUser } : {})
        };
    }
}
