import { UniqueEntityID } from '../../shared/domain/valueObjects/UniqueEntityID';
import { FormAdoptionEntity } from '../domain/FormAdoptionEntity';
import { FormAdoptionAboutPeople } from '../domain/valueObjects/formAdoption/FormAdoptionAboutPeople';
import { FormAdoptionAboutPets } from '../domain/valueObjects/formAdoption/FormAdoptionAboutPets';
import { FormAdoptionAboutPetCastrated } from '../domain/valueObjects/formAdoption/FormAdoptionAboutPetCastrated';
import { FormAdoptionLocationType } from '../domain/valueObjects/formAdoption/FormAdoptionLocationType';
import { FormAdoptionHasNoPet } from '../domain/valueObjects/formAdoption/FormAdoptionHasNoPet';
import { UserMapper } from '../../users/infrastructure/mappers/UserMapper';

export class FormAdoptionMapper {
    public static toPersistence(formAdoption: FormAdoptionEntity): any {
        return {
            id: formAdoption.id.toString(),
            aboutPeople: formAdoption.aboutPeople.value,
            aboutPets: formAdoption.aboutPets.value,
            aboutPetCastrated: formAdoption.aboutPetCastrated.value,
            locationType: formAdoption.locationType.value,
            hasNoPet: formAdoption.hasNoPet.value,
            createdByUserId: formAdoption.createdByUserId,
            postId: formAdoption.postId
        };
    }

    public static toDomain(raw: any, id?: string): FormAdoptionEntity | null {
        const aboutPeopleOrError = FormAdoptionAboutPeople.create(raw.aboutPeople);
        const aboutPetsOrError = FormAdoptionAboutPets.create(raw.aboutPets);
        const aboutPetCastratedOrError = FormAdoptionAboutPetCastrated.create(
            raw.aboutPetCastrated
        );
        const locationTypeOrError = FormAdoptionLocationType.create(raw.locationType);
        const hasNoPetOrError = FormAdoptionHasNoPet.create(raw.hasNoPet);
        const createdByUserId = raw.createdByUserId;
        const postId = raw.postId;

        const formAdoptionOrError = FormAdoptionEntity.create(
            {
                aboutPeople: aboutPeopleOrError.getValue(),
                aboutPets: aboutPetsOrError.getValue(),
                aboutPetCastrated: aboutPetCastratedOrError.getValue(),
                locationType: locationTypeOrError.getValue(),
                hasNoPet: hasNoPetOrError.getValue(),
                createdByUserId: createdByUserId,
                postId: postId
            },
            new UniqueEntityID(id)
        );

        return formAdoptionOrError.isSuccess ? formAdoptionOrError.getValue() : null;
    }

    public static toJSON(raw: FormAdoptionEntity): any {
        let rawUser;
        if (raw.props.createdBy) {
            rawUser = UserMapper.toJSON(raw.props.createdBy);
        }

        return {
            id: raw.id.toString(),
            aboutPeople: raw.props.aboutPeople.value,
            aboutPets: raw.props.aboutPets.value,
            aboutPetCastrated: raw.props.aboutPetCastrated.value,
            locationType: raw.props.locationType.value,
            hasNoPet: raw.props.hasNoPet.value,
            ...(rawUser ? { createdBy: rawUser } : {})
        };
    }
}
