import { Request, Response } from 'express';
import { LoggerImpl } from '../../../../shared/infrastructure';
import { RouterPath } from '../../../../shared/infrastructure/http/models/RouterPath';
import { getPostsContoller } from '../../../aplication/useCases/getPosts';
import { createPostContoller } from '../../../aplication/useCases/createPost';
import express from 'express';
import multer from 'multer';
import { updatePostContoller } from '../../../aplication/useCases/updatePost';
import { createFormAdoptionContoller } from '../../../aplication/useCases/createFormAdoption';
import { createQuestionContoller } from '../../../aplication/useCases/createQuestion';
import { getPostQuestionsAndAnswersContoller } from '../../../aplication/useCases/getPostQuestionsAndAnswers';
import { createAnswerContoller } from '../../../aplication/useCases/createAnswer';
import { getFormAdoptionContoller } from '../../../aplication/useCases/getFormAdoption';
var upload = multer();

export class PostRouter {
    private static readonly routePath = '/posts';

    public static create(): RouterPath {
        LoggerImpl.writeInfoLog('[PostRoute::create] Creating post routes.');

        const postRouter = express.Router();

        postRouter.get('/?', (req: Request, res: Response) => {
            getPostsContoller.execute(req, res);
        });

        postRouter.post('/', upload.array('imageFiles'), (req: Request, res: Response) => {
            createPostContoller.execute(req, res);
        });

        postRouter.put('/:postId', upload.array('imageFiles'), (req: Request, res: Response) => {
            updatePostContoller.execute(req, res);
        });

        postRouter.post('/:postId/form-adoption', (req: Request, res: Response) => {
            createFormAdoptionContoller.execute(req, res);
        });

        postRouter.post('/:postId/questions-answers', (req: Request, res: Response) => {
            createQuestionContoller.execute(req, res);
        });

        postRouter.post('/:postId/questions-answers/:questionId/answer', (req: Request, res: Response) => {
            createAnswerContoller.execute(req, res);
        });

        postRouter.get('/:postId/questions-answers', (req: Request, res: Response) => {
            getPostQuestionsAndAnswersContoller.execute(req, res);
        });

        postRouter.get('/:postId/form-adoption/:createdByUserId', (req: Request, res: Response) => {
            getFormAdoptionContoller.execute(req, res);
        });

        return { router: postRouter, path: this.routePath };
    }
}
