import { PostRepository } from './PostRepository';
import { PostRepositoryImpl } from './PostRepositoryImpl';
import { FirebaseService } from '../../../shared/core/firebase/FirebaseService';

const firestore = FirebaseService.getFirestoreReference();

export const postRepositoryImpl: PostRepository = new PostRepositoryImpl(firestore);
