import { GetPostsDTO } from '../../aplication/useCases/getPosts/dtos/GetPostsDTO';
import { AnswerEntity } from '../../domain/AnswerEntity';
import { FormAdoptionEntity } from '../../domain/FormAdoptionEntity';
import { PostEntity } from '../../domain/PostEntity';
import { QuestionEntity } from '../../domain/QuestionEntity';

export interface PostRepository {
    addPost(post: PostEntity): Promise<boolean>;
    getPosts(queryParams: GetPostsDTO): Promise<PostEntity[]>;
    getPostById(postId: string): Promise<PostEntity>;
    getPostByUserId(userId: string): Promise<PostEntity[]>;
    updatePost(post: PostEntity): Promise<void>;
    addFormAdoption(formAdoption: FormAdoptionEntity): Promise<boolean>;
    addQuestion(question: QuestionEntity): Promise<boolean>;
    getQuestionAndAnswersByPostId(postId: string): Promise<QuestionEntity[]>;
    getPostQuestionById(postId: string, questionId: string): Promise<QuestionEntity>;
    addAnswerToQuestion(answer: AnswerEntity, questionId: string, postId: string): Promise<boolean>;
    getFormAdoption(postId: string, formAdoptionId: string): Promise<FormAdoptionEntity>;
}
