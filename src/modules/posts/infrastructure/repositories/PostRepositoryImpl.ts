import { PostRepository } from './PostRepository';
import { PostEntity } from '../../domain';
import { PostMapper } from '../../mappers/PostMapper';
import { userRepositoryImpl } from '../../../users/infrastructure/repositories';
import { getGeohashRange } from '../../../shared/utils';
import { GetPostsDTO } from '../../aplication/useCases/getPosts/dtos/GetPostsDTO';
import { FormAdoptionEntity } from '../../domain/FormAdoptionEntity';
import { FormAdoptionMapper } from '../../mappers/FormAdoptionMapper';
import { QuestionEntity } from '../../domain/QuestionEntity';
import { QuestionMapper } from '../../mappers/QuestionMapper';
import { AnswerEntity } from '../../domain/AnswerEntity';
import { AnswerMapper } from '../../mappers/AnswerMapper';

export class PostRepositoryImpl implements PostRepository {
    private firestore: FirebaseFirestore.Firestore | null;
    private postCollectionPath = '/posts';
    private formAdoptionCollectionPath = '/formAdoptions';
    private questionCollectionPath = '/postQuestionsAndAnswers';

    constructor(firestore: FirebaseFirestore.Firestore | null) {
        this.firestore = firestore;
    }

    async getFormAdoption(postId: string, createdByUserId: string): Promise<FormAdoptionEntity> {
        const formAdoptionCollectionPath = this.firestore.collection(
            this.formAdoptionCollectionPath
        );

        const form = await formAdoptionCollectionPath
            .doc(postId)
            .collection('/forms')
            .doc(createdByUserId)
            .get();

        return FormAdoptionMapper.toDomain(form.data(), form.id);
    }

    async getPostQuestionById(postId: string, questionId: string): Promise<QuestionEntity> {
        const questionCollectionPath = this.firestore.collection(this.questionCollectionPath);

        const question = await questionCollectionPath
            .doc(postId)
            .collection('/questionsAndAnswers')
            .doc(questionId)
            .get();

        return QuestionMapper.toDomain(question.data(), question.id);
    }

    async addAnswerToQuestion(
        answer: AnswerEntity,
        questionId: string,
        postId: string
    ): Promise<boolean> {
        const questionCollectionPath = this.firestore.collection(this.questionCollectionPath);

        await questionCollectionPath
            .doc(postId)
            .collection('/questionsAndAnswers')
            .doc(questionId)
            .update({ answer: AnswerMapper.toPersistence(answer) });

        return true;
    }

    async getQuestionAndAnswersByPostId(postId: string): Promise<QuestionEntity[]> {
        const questionCollectionPath = this.firestore.collection(this.questionCollectionPath);

        const questionsAndAnswersSnapshot = await questionCollectionPath
            .doc(postId)
            .collection('/questionsAndAnswers')
            .orderBy('createdAt', 'asc')
            .get();

        const questions: QuestionEntity[] = [];

        for (const doc of questionsAndAnswersSnapshot.docs) {
            const user = await userRepositoryImpl.getUserByUserId(doc.data().createdByUserId);

            let answer;
            if (doc.data().answer) {
                const answerUser = await userRepositoryImpl.getUserByUserId(
                    doc.data().answer.createdByUserId
                );

                answer = AnswerMapper.toDomain({ ...doc.data().answer, createdBy: answerUser });
            }

            const questionEntity = QuestionMapper.toDomain(
                { ...doc.data(), createdBy: user, ...(answer ? { answer } : {}) },
                doc.id
            );

            if (questionEntity != null) {
                questions.push(questionEntity);
            }
        }

        return questions;
    }

    async addQuestion(question: QuestionEntity): Promise<boolean> {
        const questionCollectionPath = this.firestore.collection(this.questionCollectionPath);

        await questionCollectionPath
            .doc(question.postId.toString())
            .collection('/questionsAndAnswers')
            .doc(question.id.toString())
            .set(QuestionMapper.toPersistence(question));

        return true;
    }

    async addFormAdoption(formAdoption: FormAdoptionEntity): Promise<boolean> {
        const formAdoptionCollection = this.firestore.collection(this.formAdoptionCollectionPath);

        await formAdoptionCollection
            .doc(formAdoption.postId.toString())
            .collection('/forms')
            .doc(formAdoption.createdByUserId.toString())
            .create(FormAdoptionMapper.toPersistence(formAdoption));

        return true;
    }

    async getPostByUserId(userId: string): Promise<PostEntity[]> {
        const postCollection = this.firestore
            .collection(this.postCollectionPath)
            .where('createdByUserId', '==', userId)
            .orderBy('createdAt', 'asc');

        const posts = await postCollection.get();
        const postsRet: PostEntity[] = [];

        for (const doc of posts.docs) {
            let adoptedBy = {};
            let createdBy = {};

            if (doc.data().adoptedByUserId)
                adoptedBy = await userRepositoryImpl.getUserByUserId(doc.data().adoptedByUserId);

            if (doc.data().createdByUserId)
                createdBy = await userRepositoryImpl.getUserByUserId(doc.data().createdByUserId);

            const postEntity = PostMapper.toDomain({ ...doc.data(), adoptedBy, createdBy }, doc.id);

            if (postEntity != null) {
                postsRet.push(postEntity);
            }
        }

        return postsRet;
    }

    async getPosts(queryParams: GetPostsDTO): Promise<PostEntity[]> {
        let range: {
            lower: string;
            upper: string;
        } = null;

        if (queryParams.latitude && queryParams.longitude && queryParams.radio) {
            range = getGeohashRange(queryParams.latitude, queryParams.longitude, queryParams.radio);
        }

        let query: any = this.firestore.collection(this.postCollectionPath);

        query = query.where('state', '==', 'Activo');

        if (range) {
            query = query.where('geohash', '<=', range.upper).where('geohash', '>=', range.lower);
        }

        if (query.petMonitoring) {
            query = query.where('petMonitoring', '==', queryParams.petMonitoring);
        }

        if (queryParams.petCastratedType) {
            query = query.where('petCastrated', '==', queryParams.petCastratedType);
        }

        if (queryParams.petAgeTypes?.length > 0) {
            query = query.where('petAge', 'in', queryParams.petAgeTypes);
        }

        if (queryParams.petSexType) {
            query = query.where('petSex', '==', queryParams.petSexType);
        }

        if (queryParams.petSizeType) {
            query = query.where('petSize', '==', queryParams.petSizeType);
        }

        if (queryParams.petType) {
            query = query.where('petType', '==', queryParams.petType);
        }

        if (queryParams.postType) {
            query = query.where('postType', '==', queryParams.postType);
        }

        const postsRet: PostEntity[] = [];
        const posts = await query.get();

        for (const doc of posts.docs) {
            let adoptedBy = {};
            const user = await userRepositoryImpl.getUserByUserId(doc.data().createdByUserId);

            if (doc.data().adoptedByUserId)
                adoptedBy = await userRepositoryImpl.getUserByUserId(doc.data().adoptedByUserId);

            const postEntity = PostMapper.toDomain(
                { ...doc.data(), createdBy: user, adoptedBy },
                doc.id
            );

            if (postEntity != null) {
                postsRet.push(postEntity);
            }
        }

        return postsRet;
    }

    async addPost(post: PostEntity): Promise<boolean> {
        const postCollection = this.firestore.collection(this.postCollectionPath);

        await postCollection.doc(post.id.toString()).set(PostMapper.toPersistence(post));

        return true;
    }

    async getPostById(postId: string): Promise<PostEntity> {
        let adoptedBy = {};

        const postCollection = this.firestore.collection(this.postCollectionPath);

        const post = await postCollection.doc(postId).get();

        const createdBy = await userRepositoryImpl.getUserByUserId(post.data().createdByUserId);
        if (post.data().adoptedByUserId)
            adoptedBy = await userRepositoryImpl.getUserByUserId(post.data().adoptedByUserId);

        return PostMapper.toDomain({ ...post.data(), createdBy, adoptedBy }, post.id);
    }

    async updatePost(post: PostEntity): Promise<void> {
        const postCollection = this.firestore.collection(this.postCollectionPath);

        const rawPost = await PostMapper.toPersistence(post);

        await postCollection.doc(rawPost.id).update(rawPost);
    }
}
