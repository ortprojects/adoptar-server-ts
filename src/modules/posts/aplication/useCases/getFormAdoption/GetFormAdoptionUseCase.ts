import { GenericAppError } from '../../../../shared/core/AppError';
import { Either, Result, left, right } from '../../../../shared/core/Result';
import { PostRepository } from '../../../infrastructure/repositories/PostRepository';
import { UseCase } from '../../../../shared/domain/UseCase';
import { LoggerImpl } from '../../../../shared/infrastructure';
import { QuestionEntity } from '../../../domain/QuestionEntity';
import { GetFormAdoptionDTO } from './dtos/GetFormAdoptionDTO';
import { GetFormAdoptionErrors } from './GetFormAdoptionErrors';
import { FormAdoptionEntity } from '../../../domain/FormAdoptionEntity';

type Response = Either<
    GenericAppError.UnexpectedError | GetFormAdoptionErrors.PostByIdNotFound,
    Result<FormAdoptionEntity>
>;

export class GetFormAdoptionUseCase implements UseCase<GetFormAdoptionDTO, Promise<Response>> {
    private postRepository: PostRepository;

    constructor(postRepository: PostRepository) {
        this.postRepository = postRepository;
    }

    async execute(req: GetFormAdoptionDTO): Promise<Response> {
        LoggerImpl.writeInfoLog('[GetFormAdoptionUseCase::execute] Get form adoption.');

        try {
            await this.postRepository.getPostById(req.postId);
        } catch (error) {
            return left(new GetFormAdoptionErrors.PostByIdNotFound()) as Response;
        }

        try {
            const formAdoption = await this.postRepository.getFormAdoption(
                req.postId,
                req.createdByUserId
            );

            return right(Result.ok<FormAdoptionEntity>(formAdoption));
        } catch (err) {
            return left(new GenericAppError.UnexpectedError(err));
        }
    }
}
