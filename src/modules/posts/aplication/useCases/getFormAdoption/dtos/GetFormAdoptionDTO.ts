export interface GetFormAdoptionDTO{
    postId: string;
    createdByUserId: string;
}