import { postRepositoryImpl } from '../../../infrastructure/repositories';
import { GetFormAdoptionController } from './GetFormAdoptionController';
import { GetFormAdoptionUseCase } from './GetFormAdoptionUseCase';

const getFormAdoptionUseCase = new GetFormAdoptionUseCase(postRepositoryImpl);
const getFormAdoptionContoller = new GetFormAdoptionController(getFormAdoptionUseCase);

export { getFormAdoptionUseCase, getFormAdoptionContoller };
