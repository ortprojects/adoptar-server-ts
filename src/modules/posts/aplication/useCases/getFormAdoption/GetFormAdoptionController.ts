import { BaseController } from '../../../../shared/infrastructure/http/models/BaseController';
import { PostEntity } from '../../../domain';
import { FormAdoptionEntity } from '../../../domain/FormAdoptionEntity';
import { FormAdoptionMapper } from '../../../mappers/FormAdoptionMapper';
import { QuestionMapper } from '../../../mappers/QuestionMapper';
import { GetFormAdoptionDTO } from './dtos/GetFormAdoptionDTO';
import { GetFormAdoptionErrors } from './GetFormAdoptionErrors';
import { GetFormAdoptionUseCase } from './GetFormAdoptionUseCase';

export class GetFormAdoptionController extends BaseController {
    private useCase: GetFormAdoptionUseCase;

    constructor(useCase: GetFormAdoptionUseCase) {
        super();
        this.useCase = useCase;
    }

    async executeImpl(): Promise<any> {
        const { postId, createdByUserId } = this.req.params;

        let dto: GetFormAdoptionDTO = {
            postId,
            createdByUserId
        };

        try {
            const result = await this.useCase.execute(dto);

            if (result.isLeft()) {
                const error = result.value;

                switch (error.constructor) {
                    case GetFormAdoptionErrors.PostByIdNotFound:
                        return this.conflict(error.errorValue().message);
                    default:
                        return this.fail(error.errorValue().message);
                }
            } else {
                const formAdoption = result.value.getValue();

                return this.ok<FormAdoptionEntity>(
                    this.res,
                    FormAdoptionMapper.toJSON(formAdoption)
                );
            }
        } catch (err) {
            return this.fail(err);
        }
    }
}
