export interface CreateAnswerDTO{
    questionId: string;
    message: string;
    createdByUserId: string;
    postId: string
}