import { postRepositoryImpl } from '../../../infrastructure/repositories';
import { userRepositoryImpl } from '../../../../users/infrastructure/repositories';
import { CreateAnswerController } from './CreateAnswerController';
import { CreateAnswerUseCase } from './CreateAnswerUseCase';
import { mailerRepositoryImpl } from '../../../../shared/core/mailer';
import { notificationRepositoryImpl } from '../../../../notifications/infrastructure/repositories';

const createAnswerUseCase = new CreateAnswerUseCase(postRepositoryImpl, userRepositoryImpl, mailerRepositoryImpl, notificationRepositoryImpl);
const createAnswerContoller = new CreateAnswerController(createAnswerUseCase);

export { createAnswerUseCase, createAnswerContoller };
