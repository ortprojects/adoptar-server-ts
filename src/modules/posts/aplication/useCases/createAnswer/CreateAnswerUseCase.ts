import { Either, Result, left, right } from '../../../../shared/core/Result';
import { UseCase } from '../../../../shared/domain/UseCase';
import { GenericAppError } from '../../../../shared/core/AppError';
import { PostRepository } from '../../../infrastructure/repositories/PostRepository';
import { CreateAnswerDTO } from './dtos/CreateAnswerDTO';
import { UserRepository } from '../../../../users/infrastructure/repositories/UserRepository';
import { UniqueEntityID } from '../../../../shared/domain/valueObjects/UniqueEntityID';
import { CreateAnswerErrors } from './CreateAnswerErrors';
import { AnswerMessage } from '../../../domain/valueObjects/answer/AnswerMessage';
import { AnswerEntity } from '../../../domain/AnswerEntity';
import { MailerRepository } from '../../../../shared/core/mailer/MailerRepository';
import { NotificationRepository } from '../../../../notifications/infrastructure/repositories/NotificationRepository';
import { NotificationMessage } from '../../../../notifications/domain/valueObjects/NotificationMessage';
import { NotificationEntity } from '../../../../notifications/domain/NotificationEntity';

type Response = Either<
    | GenericAppError.UnexpectedError
    | CreateAnswerErrors.UserByIdNotFound
    | CreateAnswerErrors.PostByIdNotFound
    | CreateAnswerErrors.QuestionByIdNotFound
    | Result<any>,
    Result<void>
>;

export class CreateAnswerUseCase implements UseCase<CreateAnswerDTO, Promise<Response>> {
    private postRepository: PostRepository;
    private userRepositoy: UserRepository;
    private mailerRepository: MailerRepository;
    private notificationRepository: NotificationRepository;

    constructor(
        postRepository: PostRepository,
        userRepository: UserRepository,
        mailerRepository: MailerRepository,
        notificationRepository: NotificationRepository
    ) {
        this.postRepository = postRepository;
        this.userRepositoy = userRepository;
        this.mailerRepository = mailerRepository;
        this.notificationRepository = notificationRepository;
    }

    async execute(req: CreateAnswerDTO): Promise<Response> {
        let user;
        let post;
        let question;
        try {
            post = await this.postRepository.getPostById(req.postId);
        } catch (error) {
            return left(new CreateAnswerErrors.PostByIdNotFound()) as Response;
        }

        try {
            user = await this.userRepositoy.getUserByUserId(req.createdByUserId);
        } catch (error) {
            return left(new CreateAnswerErrors.UserByIdNotFound()) as Response;
        }

        try {
            question = await this.postRepository.getPostQuestionById(req.postId, req.questionId);
        } catch (error) {
            return left(new CreateAnswerErrors.QuestionByIdNotFound()) as Response;
        }

        const answerMessageOrError = AnswerMessage.create(req.message);
        const createdAt = new Date();
        const createdByUserId = new UniqueEntityID(req.createdByUserId);

        const combinedPropsResult = Result.combine([answerMessageOrError]);

        if (combinedPropsResult.isFailure) {
            return left(Result.fail<void>(combinedPropsResult.error)) as Response;
        }

        const answerOrError = AnswerEntity.create({
            message: answerMessageOrError.getValue(),
            createdByUserId,
            createdAt
        });

        if (answerOrError.isFailure) {
            return left(Result.fail<AnswerEntity>(answerOrError.error.toString())) as Response;
        }

        const answerEntity = answerOrError.getValue();

        const notificationMessage = NotificationMessage.create(
            `${user.displayName.value} respondio tu pregunta`
        );

        const notificationOrError = NotificationEntity.create({
            createdAt: new Date(),
            userId: question.createdByUserId,
            message: notificationMessage.getValue(),
            isRead: false
        });

        if (notificationOrError.isFailure) {
            return left(
                Result.fail<NotificationEntity>(notificationOrError.error.toString())
            ) as Response;
        }

        const notificationEntity = notificationOrError.getValue();

        await this.notificationRepository.addNotification(notificationEntity);

        try {
            await this.postRepository.addAnswerToQuestion(answerEntity, req.questionId, req.postId);
        } catch (err) {
            return left(new GenericAppError.UnexpectedError(err)) as Response;
        }

        try {
            this.mailerRepository.sendEmail(
                question.createdBy.email.value,
                'Ey!, tienes una nueva respueta',
                'Parece que respondieron a tu pregunta. Ingresa a la app!'
            );

            await this.notificationRepository.addNotification(notificationEntity);
            return right(Result.ok<void>());
        } catch (error) {
            return left(new GenericAppError.UnexpectedError(error.message)) as Response;
        }
    }
}
