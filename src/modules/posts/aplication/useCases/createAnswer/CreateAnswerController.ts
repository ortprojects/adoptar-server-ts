import { BaseController } from '../../../../shared/infrastructure/http/models/BaseController';
import { CreateAnswerErrors } from './CreateAnswerErrors';
import { CreateAnswerUseCase } from './CreateAnswerUseCase';
import { CreateAnswerDTO } from './dtos/CreateAnswerDTO';

export class CreateAnswerController extends BaseController {
    private useCase: CreateAnswerUseCase;

    constructor(useCase: CreateAnswerUseCase) {
        super();
        this.useCase = useCase;
    }

    async executeImpl(): Promise<any> {
        const dto: CreateAnswerDTO = this.req.body as CreateAnswerDTO;
        const { postId } = this.req.params;
        const { questionId } = this.req.params;

        dto.postId = postId;
        dto.questionId = questionId;

        try {
            const result = await this.useCase.execute(dto);

            if (result.isLeft()) {
                const error = result.value;

                switch (error.constructor) {
                    case CreateAnswerErrors.PostByIdNotFound:
                        return this.conflict(error.errorValue().message);
                    case CreateAnswerErrors.UserByIdNotFound:
                        return this.conflict(error.errorValue().message);
                        case CreateAnswerErrors.QuestionByIdNotFound:
                        return this.conflict(error.errorValue().message);
                    default:
                        return this.fail(error.errorValue().message);
                }
            } else {
                return this.created(this.res);
            }
        } catch (err) {
            return this.fail(err);
        }
    }
}
