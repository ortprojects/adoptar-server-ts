export interface UpdatePostDTO {
    postId: string;
    petName: string;
    petSex: string;
    petType: string;
    postType: string;
    postTitle: string;
    petBreed: string;
    petAge: string;
    petSize: string;
    petWeigth: number;
    petCastrated: string;
    postDescription: string;
    petMonitoring: boolean;
    latitude: number;
    longitude: number;
    imageFiles: string[],
    state: string
}
