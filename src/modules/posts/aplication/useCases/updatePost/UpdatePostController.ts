import { BaseController } from '../../../../shared/infrastructure/http/models/BaseController';
import { UpdatePostDTO } from './dtos/UpdatePostDTO';
import { UpdatePostErrors } from './UpdatePostErrors';
import { UpdatePostUseCase } from './UpdatePostUseCase';

export class UpdatePostController extends BaseController {
    private useCase: UpdatePostUseCase;

    constructor(useCase: UpdatePostUseCase) {
        super();
        this.useCase = useCase;
    }

    async executeImpl(): Promise<any> {
        const dto: UpdatePostDTO = this.req.body as UpdatePostDTO;
        const { postId } = this.req.params;

        dto.postId = postId;

        try {
            const result = await this.useCase.execute(dto);
            if (result.isLeft()) {
                const error = result.value;

                switch (error.constructor) {
                    case UpdatePostErrors.PostByIdNotFound:
                        return this.conflict(error.errorValue().message);
                    default:
                        return this.fail(error.errorValue().message);
                }
            } else {
                return this.created(this.res);
            }
        } catch (err) {
            return this.fail(err);
        }
    }
}
