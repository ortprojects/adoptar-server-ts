import { Result } from "../../../../shared/core/Result";
import { UseCaseError } from "../../../../shared/core/UseCaseError";

export namespace UpdatePostErrors {
    export class PostByIdNotFound extends Result<UseCaseError> {
        constructor(postId: string) {
            super(false, {
                message: `Couldn't find a post by id {${postId}}`
            } as UseCaseError);
        }
    }
}
