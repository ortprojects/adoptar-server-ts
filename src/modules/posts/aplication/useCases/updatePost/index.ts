import { postRepositoryImpl } from '../../../infrastructure/repositories';
import { UpdatePostController } from './UpdatePostController';
import { UpdatePostUseCase } from './UpdatePostUseCase';

const updatePostUseCase = new UpdatePostUseCase(postRepositoryImpl);
const updatePostContoller = new UpdatePostController(updatePostUseCase);

export { updatePostUseCase, updatePostContoller };
