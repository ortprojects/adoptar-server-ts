import { Either, Result, left, right } from '../../../../shared/core/Result';
import { UseCase } from '../../../../shared/domain/UseCase';
import { GenericAppError } from '../../../../shared/core/AppError';
import { PostRepository } from '../../../infrastructure/repositories/PostRepository';
import { PostEntity } from '../../../domain';
import { PostPetName } from '../../../domain/valueObjects/PostPetName';
import { PostPetSex } from '../../../domain/valueObjects/PostPetSex';
import { PostPetType } from '../../../domain/valueObjects/PostPetType';
import { PostType } from '../../../domain/valueObjects/PostType';
import { PostPetBreed } from '../../../domain/valueObjects/PostPetBreed';
import { PostPetAge } from '../../../domain/valueObjects/PostPetAge';
import { PostPetSize } from '../../../domain/valueObjects/PostPetSize';
import { PostPetWeigth } from '../../../domain/valueObjects/PostPetWeigth';
import { PostPetCastrated } from '../../../domain/valueObjects/PostPetCastrated';
import { PostDescription } from '../../../domain/valueObjects/PostDescription';
import { PostPetMonitoring } from '../../../domain/valueObjects/PostPetMonitoring';
import { PostGeohash } from '../../../domain/valueObjects/PostGeohash';
import { UpdatePostDTO } from './dtos/UpdatePostDTO';
import { UpdatePostErrors } from './UpdatePostErrors';
import { PostTitle } from '../../../domain/valueObjects/PostTitle';
import { PostState } from '../../../domain/valueObjects/PostState';

type Response = Either<
    GenericAppError.UnexpectedError | UpdatePostErrors.PostByIdNotFound | Result<any>,
    Result<void>
>;

export class UpdatePostUseCase implements UseCase<UpdatePostDTO, Promise<Response>> {
    private postRepository: PostRepository;

    constructor(postRepository: PostRepository) {
        this.postRepository = postRepository;
    }

    async execute(req: UpdatePostDTO): Promise<Response> {
        const petNameOrError = PostPetName.create(req.petName);
        const petSexOrError = PostPetSex.create(req.petSex);
        const petTypeOrError = PostPetType.create(req.petType);
        const postTypeOrError = PostType.create(req.postType);
        const petBreedOrError = PostPetBreed.create(req.petBreed);
        const petAgeOrError = PostPetAge.create(req.petAge);
        const petSizeOrError = PostPetSize.create(req.petSize);
        const petWeigthOrError = PostPetWeigth.create(req.petWeigth);
        const petCastratedOrError = PostPetCastrated.create(req.petCastrated);
        const postDescriptionOrError = PostDescription.create(req.postDescription);
        const petMonitoringOrError = PostPetMonitoring.create(req.petMonitoring);
        const postGeohashOrError = PostGeohash.create(req.latitude, req.longitude);
        const postTitleOrError = PostTitle.create(req.postTitle);
        const postStateOrError = PostState.create(req.state);
        const postId = req.postId;
        let postEntity: PostEntity;

        const combinedPropsResult = Result.combine([
            petNameOrError,
            petSexOrError,
            petTypeOrError,
            postTypeOrError,
            petBreedOrError,
            petAgeOrError,
            petSizeOrError,
            petWeigthOrError,
            petCastratedOrError,
            postDescriptionOrError,
            petMonitoringOrError,
            postGeohashOrError,
            postTitleOrError,
            postStateOrError
        ]);

        if (combinedPropsResult.isFailure) {
            return left(Result.fail<void>(combinedPropsResult.error)) as Response;
        }

        try {
            postEntity = await this.postRepository.getPostById(postId);
        } catch (error) {
            return left(new UpdatePostErrors.PostByIdNotFound(postId)) as Response;
        }

        const postOrError = PostEntity.create({
            petName: petNameOrError.getValue(),
            petSex: petSexOrError.getValue(),
            petType: petTypeOrError.getValue(),
            postType: postTypeOrError.getValue(),
            petBreed: petBreedOrError.getValue(),
            petAge: petAgeOrError.getValue(),
            petSize: petSizeOrError.getValue(),
            petWeigth: petWeigthOrError.getValue(),
            petCastrated: petCastratedOrError.getValue(),
            postDescription: postDescriptionOrError.getValue(),
            petMonitoring: petMonitoringOrError.getValue(),
            imageFiles: postEntity.imageFiles,
            geohash: postGeohashOrError.getValue(),
            createdAt: postEntity.createdAt,
            createdByUserId: postEntity.createdByUserId,
            postTitle: postTitleOrError.getValue(),
            state: postStateOrError.getValue(),
            latitude: req.latitude,
            longitude: req.longitude
        }, postEntity.id);

        if (postOrError.isFailure) {
            return left(Result.fail<PostEntity>(postOrError.error.toString())) as Response;
        }

        try {
            await this.postRepository.updatePost(postOrError.getValue());
        } catch (err) {
            return left(new GenericAppError.UnexpectedError(err)) as Response;
        }

        return right(Result.ok<void>());
    }
}
