import { BaseController } from '../../../../shared/infrastructure/http/models/BaseController';
import { CreateFormAdoptionErrors } from './CreateFormAdoptionErrors';
import { CreateFormAdoptionUseCase } from './CreateFormAdoptionUseCase';
import { CreateFormAdoptionDTO } from './dtos/CreateFormAdoptionDTO';

export class CreateFormAdoptionController extends BaseController {
    private useCase: CreateFormAdoptionUseCase;

    constructor(useCase: CreateFormAdoptionUseCase) {
        super();
        this.useCase = useCase;
    }

    async executeImpl(): Promise<any> {
        const dto: CreateFormAdoptionDTO = this.req.body as CreateFormAdoptionDTO;
        const { postId } = this.req.params;

        dto.postId = postId;


        try {
            const result = await this.useCase.execute(dto);
            if (result.isLeft()) {
                const error = result.value;

                switch (error.constructor) {
                    case CreateFormAdoptionErrors.UserByIdNotFound:
                        return this.conflict(error.errorValue().message);
                    case CreateFormAdoptionErrors.PostByIdNotFound:
                        return this.conflict(error.errorValue().message);
                    default:
                        return this.fail(error.errorValue().message);
                }
            } else {
                return this.created(this.res);
            }
        } catch (err) {
            return this.fail(err);
        }
    }
}
