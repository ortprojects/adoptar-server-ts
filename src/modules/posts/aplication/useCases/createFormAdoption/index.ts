
import { postRepositoryImpl } from '../../../infrastructure/repositories';
import { userRepositoryImpl } from '../../../../users/infrastructure/repositories';
import { CreateFormAdoptionUseCase } from './CreateFormAdoptionUseCase';
import { CreateFormAdoptionController } from './CreateFormAdoptionController';
import { mailerRepositoryImpl } from '../../../../shared/core/mailer';
import { notificationRepositoryImpl } from '../../../../notifications/infrastructure/repositories';

const createFormAdoptionUseCase = new CreateFormAdoptionUseCase(postRepositoryImpl, userRepositoryImpl, mailerRepositoryImpl, notificationRepositoryImpl);
const createFormAdoptionContoller = new CreateFormAdoptionController(createFormAdoptionUseCase);

export {
    createFormAdoptionUseCase,
    createFormAdoptionContoller
}
