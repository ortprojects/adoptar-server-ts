import { UseCaseError } from '../../../../shared/core/UseCaseError';
import { Result } from '../../../../shared/core/Result';

export namespace CreateFormAdoptionErrors {
    export class UserByIdNotFound extends Result<UseCaseError> {
        constructor() {
            super(false, {
                message: `User not found`
            } as UseCaseError);
        }
    }

    export class PostByIdNotFound extends Result<UseCaseError> {
        constructor() {
            super(false, {
                message: `Post not found`
            } as UseCaseError);
        }
    }
    
}
