import { Either, Result, left, right } from '../../../../shared/core/Result';
import { UseCase } from '../../../../shared/domain/UseCase';
import { GenericAppError } from '../../../../shared/core/AppError';
import { PostRepository } from '../../../infrastructure/repositories/PostRepository';
import { CreateFormAdoptionDTO } from './dtos/CreateFormAdoptionDTO';

import { UserRepository } from '../../../../users/infrastructure/repositories/UserRepository';
import { CreateFormAdoptionErrors } from './CreateFormAdoptionErrors';
import { FormAdoptionAboutPeople } from '../../../domain/valueObjects/formAdoption/FormAdoptionAboutPeople';
import { FormAdoptionAboutPets } from '../../../domain/valueObjects/formAdoption/FormAdoptionAboutPets';
import { FormAdoptionAboutPetCastrated } from '../../../domain/valueObjects/formAdoption/FormAdoptionAboutPetCastrated';
import { FormAdoptionLocationType } from '../../../domain/valueObjects/formAdoption/FormAdoptionLocationType';
import { FormAdoptionEntity } from '../../../domain/FormAdoptionEntity';
import { FormAdoptionHasNoPet } from '../../../domain/valueObjects/formAdoption/FormAdoptionHasNoPet';
import { MailerRepository } from '../../../../shared/core/mailer/MailerRepository';
import { NotificationEntity } from '../../../../notifications/domain/NotificationEntity';
import { NotificationRepository } from '../../../../notifications/infrastructure/repositories/NotificationRepository';
import { NotificationMessage } from '../../../../notifications/domain/valueObjects/NotificationMessage';
import { UniqueEntityID } from '../../../../shared/domain/valueObjects/UniqueEntityID';

type Response = Either<GenericAppError.UnexpectedError | Result<any>, Result<void>>;

export class CreateFormAdoptionUseCase
    implements UseCase<CreateFormAdoptionDTO, Promise<Response>> {
    private postRepository: PostRepository;
    private userRepositoy: UserRepository;
    private mailerRepository: MailerRepository;
    private notificationRepository: NotificationRepository;

    constructor(
        postRepository: PostRepository,
        userRepository: UserRepository,
        mailerRepository: MailerRepository,
        notificationRepository: NotificationRepository
    ) {
        this.postRepository = postRepository;
        this.userRepositoy = userRepository;
        this.mailerRepository = mailerRepository;
        this.notificationRepository = notificationRepository;
    }

    async execute(req: CreateFormAdoptionDTO): Promise<Response> {
        let createdBy;
        let postEntity;

        try {
            createdBy = await this.userRepositoy.getUserByUserId(req.createdByUserId);
        } catch (error) {
            return left(new CreateFormAdoptionErrors.UserByIdNotFound()) as Response;
        }

        try {
            postEntity = await this.postRepository.getPostById(req.postId);
        } catch (error) {
            return left(new CreateFormAdoptionErrors.PostByIdNotFound()) as Response;
        }

        const aboutPeopleOrError = FormAdoptionAboutPeople.create(req.aboutPeople);
        const aboutPetsOrError = FormAdoptionAboutPets.create(req.aboutPets);
        const aboutPetCastratedOrError = FormAdoptionAboutPetCastrated.create(
            req.aboutPetCastrated
        );
        const locationTypeOrError = FormAdoptionLocationType.create(req.locationType);
        const hasNoPetOrError = FormAdoptionHasNoPet.create(req.hasNoPet);
        const createdByUserId = req.createdByUserId;
        const postId = req.postId;

        const combinedPropsResult = Result.combine([
            aboutPeopleOrError,
            aboutPetsOrError,
            aboutPetCastratedOrError,
            locationTypeOrError,
            hasNoPetOrError
        ]);

        if (combinedPropsResult.isFailure) {
            return left(Result.fail<void>(combinedPropsResult.error)) as Response;
        }

        const formAdoptionOrError = FormAdoptionEntity.create({
            aboutPeople: aboutPeopleOrError.getValue(),
            aboutPets: aboutPetsOrError.getValue(),
            aboutPetCastrated: aboutPetCastratedOrError.getValue(),
            locationType: locationTypeOrError.getValue(),
            hasNoPet: hasNoPetOrError.getValue(),
            createdByUserId: createdByUserId,
            postId: postId
        });

        if (formAdoptionOrError.isFailure) {
            return left(
                Result.fail<FormAdoptionEntity>(formAdoptionOrError.error.toString())
            ) as Response;
        }

        const formAdoptionEntity: FormAdoptionEntity = formAdoptionOrError.getValue();

        try {
            await this.postRepository.addFormAdoption(formAdoptionEntity);
            await this.userRepositoy.addInteresedIn(req.createdByUserId, req.postId);
            await this.userRepositoy.addInteresed(req.createdByUserId, req.postId);
        } catch (err) {
            return left(new GenericAppError.UnexpectedError(err)) as Response;
        }

        const notificationMessage = NotificationMessage.create(
            `${createdBy.displayName.value} quiere adoptar tu mascota`
        );

        const notificationOrError = NotificationEntity.create({
            createdAt: new Date(),
            userId: new UniqueEntityID(postEntity.createdByUserId.value),
            message: notificationMessage.getValue(),
            isRead: false
        });

        if (notificationOrError.isFailure) {
            return left(
                Result.fail<NotificationEntity>(notificationOrError.error.toString())
            ) as Response;
        }

        const notificationEntity = notificationOrError.getValue();

        try {
            this.mailerRepository.sendEmail(
                postEntity.createdBy.email.value,
                'Ey! alguien quiere adoptar tu mascota',
                `Parece que alguien te envio un formulario de adopción. Ingresa a la app!`
            );

            await this.notificationRepository.addNotification(notificationEntity);
            return right(Result.ok<void>());
        } catch (error) {
            return left(new GenericAppError.UnexpectedError(error.message)) as Response;
        }
    }
}
