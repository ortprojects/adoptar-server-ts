export interface CreateFormAdoptionDTO{
    aboutPeople: string;
    aboutPets: string;
    aboutPetCastrated: string;
    locationType: string;
    hasNoPet: string;
    createdByUserId: string;
    postId: string;
}