export interface CreateQuestionDTO{
    question: string;
    createdByUserId: string;
    postId: string
}