import { Either, Result, left, right } from '../../../../shared/core/Result';
import { UseCase } from '../../../../shared/domain/UseCase';
import { GenericAppError } from '../../../../shared/core/AppError';
import { PostRepository } from '../../../infrastructure/repositories/PostRepository';

import { CreateQuestionErrors } from './CreateQuestionErrors';
import { CreateQuestionDTO } from './dtos/CreateQuestionDTO';
import { UserRepository } from '../../../../users/infrastructure/repositories/UserRepository';
import { QuestionText } from '../../../domain/valueObjects/question/QuestionText';
import { UniqueEntityID } from '../../../../shared/domain/valueObjects/UniqueEntityID';
import { QuestionEntity } from '../../../domain/QuestionEntity';
import { MailerRepository } from '../../../../shared/core/mailer/MailerRepository';
import { NotificationRepository } from '../../../../notifications/infrastructure/repositories/NotificationRepository';
import { NotificationMessage } from '../../../../notifications/domain/valueObjects/NotificationMessage';
import { NotificationEntity } from '../../../../notifications/domain/NotificationEntity';

type Response = Either<
    | GenericAppError.UnexpectedError
    | CreateQuestionErrors.UserByIdNotFound
    | CreateQuestionErrors.PostByIdNotFound
    | Result<any>,
    Result<void>
>;

export class CreateQuestionUseCase implements UseCase<CreateQuestionDTO, Promise<Response>> {
    private postRepository: PostRepository;
    private userRepositoy: UserRepository;
    private mailerRepository: MailerRepository;
    private notificationRepository: NotificationRepository;

    constructor(
        postRepository: PostRepository,
        userRepository: UserRepository,
        mailerRepository: MailerRepository,
        notificationRepository: NotificationRepository
    ) {
        this.postRepository = postRepository;
        this.userRepositoy = userRepository;
        this.mailerRepository = mailerRepository;
        this.notificationRepository = notificationRepository;
    }

    async execute(req: CreateQuestionDTO): Promise<Response> {
        let postEntity;
        let userCreatedQuestion;
        try {
            postEntity = await this.postRepository.getPostById(req.postId);
        } catch (error) {
            return left(new CreateQuestionErrors.PostByIdNotFound()) as Response;
        }

        try {
            userCreatedQuestion = await this.userRepositoy.getUserByUserId(req.createdByUserId);
        } catch (error) {
            return left(new CreateQuestionErrors.UserByIdNotFound()) as Response;
        }

        const questionTextOrError = QuestionText.create(req.question);
        const createdAt = new Date();
        const createdByUserId = new UniqueEntityID(req.createdByUserId);
        const postId = new UniqueEntityID(req.postId);

        const combinedPropsResult = Result.combine([questionTextOrError]);

        if (combinedPropsResult.isFailure) {
            return left(Result.fail<void>(combinedPropsResult.error)) as Response;
        }

        const questionOrError = QuestionEntity.create({
            text: questionTextOrError.getValue(),
            createdByUserId,
            createdAt,
            postId
        });

        if (questionOrError.isFailure) {
            return left(Result.fail<QuestionEntity>(questionOrError.error.toString())) as Response;
        }

        const questionEntity = questionOrError.getValue();

        try {
            await this.postRepository.addQuestion(questionEntity);
        } catch (err) {
            return left(new GenericAppError.UnexpectedError(err)) as Response;
        }

        const notificationMessage = NotificationMessage.create(
            `${userCreatedQuestion.displayName.value} realizó una pregunta`
        );

        const notificationOrError = NotificationEntity.create({
            createdAt: new Date(),
            userId: new UniqueEntityID(postEntity.createdByUserId.value),
            message: notificationMessage.getValue(),
            isRead: false
        });

        if (notificationOrError.isFailure) {
            return left(
                Result.fail<NotificationEntity>(notificationOrError.error.toString())
            ) as Response;
        }

        const notificationEntity = notificationOrError.getValue();

        try {
            this.mailerRepository.sendEmail(
                postEntity.createdBy.email.value,
                'Ey!, tienes una nueva pregunta',
                'Parece que alguien te hizo una pregunta. Ingresa a la app!'
            );

            await this.notificationRepository.addNotification(notificationEntity);
            return right(Result.ok<void>());
        } catch (error) {
            return left(new GenericAppError.UnexpectedError(error.message)) as Response;
        }
    }
}
