import { BaseController } from '../../../../shared/infrastructure/http/models/BaseController';
import { CreateQuestionErrors } from './CreateQuestionErrors';
import { CreateQuestionUseCase } from './CreateQuestionUseCase';
import { CreateQuestionDTO } from './dtos/CreateQuestionDTO';

export class CreateQuestionController extends BaseController {
    private useCase: CreateQuestionUseCase;

    constructor(useCase: CreateQuestionUseCase) {
        super();
        this.useCase = useCase;
    }

    async executeImpl(): Promise<any> {
        const dto: CreateQuestionDTO = this.req.body as CreateQuestionDTO;
        const { postId } = this.req.params;

        dto.postId = postId;

        try {
            const result = await this.useCase.execute(dto);

            if (result.isLeft()) {
                const error = result.value;

                switch (error.constructor) {
                    case CreateQuestionErrors.PostByIdNotFound:
                        return this.conflict(error.errorValue().message);
                    case CreateQuestionErrors.UserByIdNotFound:
                        return this.conflict(error.errorValue().message);
                    default:
                        return this.fail(error.errorValue().message);
                }
            } else {
                return this.created(this.res);
            }
        } catch (err) {
            return this.fail(err);
        }
    }
}
