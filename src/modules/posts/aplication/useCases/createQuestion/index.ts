import { CreateQuestionUseCase } from './CreateQuestionUseCase';
import { postRepositoryImpl } from '../../../infrastructure/repositories';
import { userRepositoryImpl } from '../../../../users/infrastructure/repositories';
import { CreateQuestionController } from './CreateQuestionController';
import { mailerRepositoryImpl } from '../../../../shared/core/mailer';
import { notificationRepositoryImpl } from '../../../../notifications/infrastructure/repositories';

const createQuestionUseCase = new CreateQuestionUseCase(postRepositoryImpl, userRepositoryImpl, mailerRepositoryImpl, notificationRepositoryImpl);
const createQuestionContoller = new CreateQuestionController(createQuestionUseCase);

export { createQuestionUseCase, createQuestionContoller };
