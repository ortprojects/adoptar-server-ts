import { Either, Result, left, right } from '../../../../shared/core/Result';
import { UseCase } from '../../../../shared/domain/UseCase';
import { GenericAppError } from '../../../../shared/core/AppError';
import { PostRepository } from '../../../infrastructure/repositories/PostRepository';
import { PostEntity } from '../../../domain';
import { CreatePostDTO } from './dtos/CreatePostDTO';

import { PostPetName } from '../../../domain/valueObjects/PostPetName';
import { PostPetSex } from '../../../domain/valueObjects/PostPetSex';
import { PostPetType } from '../../../domain/valueObjects/PostPetType';
import { PostType } from '../../../domain/valueObjects/PostType';
import { PostPetBreed } from '../../../domain/valueObjects/PostPetBreed';
import { PostPetAge } from '../../../domain/valueObjects/PostPetAge';
import { PostPetSize } from '../../../domain/valueObjects/PostPetSize';
import { PostPetWeigth } from '../../../domain/valueObjects/PostPetWeigth';
import { PostPetCastrated } from '../../../domain/valueObjects/PostPetCastrated';
import { PostDescription } from '../../../domain/valueObjects/PostDescription';
import { PostPetMonitoring } from '../../../domain/valueObjects/PostPetMonitoring';
import { PostDate } from '../../../domain/valueObjects/PostDate';
import { StorageRepository } from '../../../../shared/infrastructure/storage/StorageReposity';
import { PostGeohash } from '../../../domain/valueObjects/PostGeohash';
import { PostCreatedByUserId } from '../../../domain/valueObjects/PostCreatedByUserId';
import { UserRepository } from '../../../../users/infrastructure/repositories/UserRepository';
import { CreatePostErrors } from './CreatePostErrors';
import { PostTitle } from '../../../domain/valueObjects/PostTitle';
import { PostState } from '../../../domain/valueObjects/PostState';
import { MonitoringRepository } from '../../../../monitoring/infrastructure/repositories/MonitoringRepository';
import { MonitoringEntity } from '../../../../monitoring/domain/MonitorinEntity';

type Response = Either<GenericAppError.UnexpectedError | Result<any>, Result<void>>;

export class CreatePostUseCase implements UseCase<CreatePostDTO, Promise<Response>> {
    private postRepository: PostRepository;
    private userRepositoy: UserRepository;
    private storageReposity: StorageRepository;
    private monitoringReposity: MonitoringRepository;

    constructor(
        postRepository: PostRepository,
        userRepository: UserRepository,
        storageReposity: StorageRepository,
        monitoringReposity: MonitoringRepository
    ) {
        this.postRepository = postRepository;
        this.storageReposity = storageReposity;
        this.userRepositoy = userRepository;
        this.monitoringReposity = monitoringReposity;
    }

    async execute(req: CreatePostDTO): Promise<Response> {
        try {
            await this.userRepositoy.getUserByUserId(req.userId);
        } catch (error) {
            return left(new CreatePostErrors.UserByIdNotFound()) as Response;
        }

        const petNameOrError = PostPetName.create(req.petName);
        const petSexOrError = PostPetSex.create(req.petSex);
        const petTypeOrError = PostPetType.create(req.petType);
        const postTypeOrError = PostType.create(req.postType);
        const petBreedOrError = PostPetBreed.create(req.petBreed);
        const petAgeOrError = PostPetAge.create(req.petAge);
        const petSizeOrError = PostPetSize.create(req.petSize);
        const petWeigth = PostPetWeigth.create(req.petWeigth);
        const petCastratedOrError = PostPetCastrated.create(req.petCastrated);
        const postDescription = PostDescription.create(req.postDescription);
        const petMonitoringOrError = PostPetMonitoring.create(req.petMonitoring);
        const postGeohashOrError = PostGeohash.create(req.latitude, req.longitude);
        const postCreatedAtError = PostDate.create();
        const postCreatedByUserIdOrError = PostCreatedByUserId.create(req.userId);
        const postTitleOrError = PostTitle.create(req.postTitle);
        const postStateOrError = PostState.create();
        const { imageFiles } = req;

        const combinedPropsResult = Result.combine([
            petNameOrError,
            petSexOrError,
            petTypeOrError,
            postTypeOrError,
            petBreedOrError,
            petAgeOrError,
            petSizeOrError,
            petWeigth,
            petCastratedOrError,
            postDescription,
            petMonitoringOrError,
            postGeohashOrError,
            postCreatedAtError,
            postCreatedByUserIdOrError,
            postTitleOrError
        ]);

        if (combinedPropsResult.isFailure) {
            return left(Result.fail<void>(combinedPropsResult.error)) as Response;
        }

        const uploadedPromises = imageFiles.map((i) => this.storageReposity.upload('posts', i));

        let urlImages;
        try {
            urlImages = await Promise.all(uploadedPromises);
        } catch (error) {
            return left(new CreatePostErrors.UploadImages()) as Response;
        }

        const postOrError = PostEntity.create({
            petName: petNameOrError.getValue(),
            petSex: petSexOrError.getValue(),
            petType: petTypeOrError.getValue(),
            postType: postTypeOrError.getValue(),
            petBreed: petBreedOrError.getValue(),
            petAge: petAgeOrError.getValue(),
            petSize: petSizeOrError.getValue(),
            petWeigth: petWeigth.getValue(),
            petCastrated: petCastratedOrError.getValue(),
            postDescription: postDescription.getValue(),
            petMonitoring: petMonitoringOrError.getValue(),
            imageFiles: urlImages,
            geohash: postGeohashOrError.getValue(),
            createdAt: postCreatedAtError.getValue(),
            createdByUserId: postCreatedByUserIdOrError.getValue(),
            postTitle: postTitleOrError.getValue(),
            state: postStateOrError.getValue(),
            latitude: req.latitude,
            longitude: req.longitude
        });

        if (postOrError.isFailure) {
            return left(Result.fail<PostEntity>(postOrError.error.toString())) as Response;
        }

        const post: PostEntity = postOrError.getValue();

        try {
            await this.postRepository.addPost(post);
        } catch (err) {
            return left(new GenericAppError.UnexpectedError(err)) as Response;
        }

        if (post.petMonitoring.value) {
            const monitoringOrError = MonitoringEntity.create({
                daysControlChecked: false,
                isFinished: false,
                newHouserChecked: false,
                petPhotoChecked: false,
                postId: post.id,
                sanitaryNotebookChecked: false
            });

            if (monitoringOrError.isFailure) {
                return left(
                    Result.fail<MonitoringEntity>(monitoringOrError.error.toString())
                ) as Response;
            }

            const monitoring = monitoringOrError.getValue();

            try {
                await this.monitoringReposity.addMonitoring(monitoring);
            } catch (error) {
                return left(new GenericAppError.UnexpectedError(error)) as Response;
            }
        }

        return right(Result.ok<void>());
    }
}
