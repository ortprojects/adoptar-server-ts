export interface CreatePostDTO {
    userId: string;
    petName: string;
    petSex: string;
    petType: string;
    postType: string;
    petBreed: string;
    petAge: string;
    petSize: string;
    petWeigth: number;
    petCastrated: string;
    postDescription: string;
    petMonitoring: boolean;
    imageFiles: Express.Multer.File[],
    latitude: number,
    longitude: number,
    postTitle: string,
}
