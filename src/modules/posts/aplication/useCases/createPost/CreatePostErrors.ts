import { UseCaseError } from '../../../../shared/core/UseCaseError';
import { Result } from '../../../../shared/core/Result';

export namespace CreatePostErrors {
    export class UserByIdNotFound extends Result<UseCaseError> {
        constructor() {
            super(false, {
                message: `User not found`
            } as UseCaseError);
        }
    }

    export class UploadImages extends Result<UseCaseError> {
        constructor() {
            super(false, {
                message: `Could not save the images`
            } as UseCaseError);
        }
    }
}
