import { CreatePostUseCase } from './CreatePostUseCase';
import { BaseController } from '../../../../shared/infrastructure/http/models/BaseController';
import { CreatePostDTO } from './dtos/CreatePostDTO';
import { CreatePostErrors } from './CreatePostErrors';

export class CreatePostController extends BaseController {
    private useCase: CreatePostUseCase;

    constructor(useCase: CreatePostUseCase) {
        super();
        this.useCase = useCase;
    }

    async executeImpl(): Promise<any> {
        const dto: CreatePostDTO = this.req.body as CreatePostDTO;
        dto.imageFiles = this.req.files as Express.Multer.File[]
        
        try {
            const result = await this.useCase.execute(dto);
            if (result.isLeft()) {
                const error = result.value;

                switch (error.constructor) {
                    case CreatePostErrors.UserByIdNotFound:
                        return this.conflict(error.errorValue().message);
                    default:
                        return this.fail(error.errorValue().message);
                }
            } else {
                return this.created(this.res);
            }
        } catch (err) {
            return this.fail(err);
        }
    }
}
