import { CreatePostController } from './CreatePostController';
import { CreatePostUseCase } from './CreatePostUseCase';
import { postRepositoryImpl } from '../../../infrastructure/repositories';
import { storageRepositoryImpl } from '../../../../shared/infrastructure/storage';
import { userRepositoryImpl } from '../../../../users/infrastructure/repositories';
import { monitoringRepositoryImpl } from '../../../../monitoring/infrastructure/repositories';

const createPostUseCase = new CreatePostUseCase(postRepositoryImpl, userRepositoryImpl, storageRepositoryImpl, monitoringRepositoryImpl);
const createPostContoller = new CreatePostController(createPostUseCase);

export {
    createPostUseCase,
    createPostContoller
}
