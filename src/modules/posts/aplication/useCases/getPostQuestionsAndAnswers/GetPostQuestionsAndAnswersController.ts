import { BaseController } from '../../../../shared/infrastructure/http/models/BaseController';
import { PostEntity } from '../../../domain';
import { GetPostQuestionsAndAnswersUseCase } from './GetPostQuestionsAndAnswersUseCase';
import { GetPostQuestionsAndAnswersDTO } from './dtos/GetPostQuestionsAndAnswersDTO';
import { QuestionMapper } from '../../../mappers/QuestionMapper';
import { GetPostQuestionsAndAnswersErrors } from './GetPostQuestionsAndAnswersErrors';

export class GetPostQuestionsAndAnswersController extends BaseController {
    private useCase: GetPostQuestionsAndAnswersUseCase;

    constructor(useCase: GetPostQuestionsAndAnswersUseCase) {
        super();
        this.useCase = useCase;
    }

    async executeImpl(): Promise<any> {
        const { postId } = this.req.params;
        let dto: GetPostQuestionsAndAnswersDTO = {} as GetPostQuestionsAndAnswersDTO;

        dto.postId = postId;

        try {
            const result = await this.useCase.execute(dto);

            if (result.isLeft()) {
                const error = result.value;

                switch (error.constructor) {
                    case GetPostQuestionsAndAnswersErrors.PostByIdNotFound:
                        return this.conflict(error.errorValue().message);
                    default:
                        return this.fail(error.errorValue().message);
                }
            } else {
                const questions = result.value.getValue();

                return this.ok<PostEntity[]>(
                    this.res,
                    questions.map((question) => QuestionMapper.toJSON(question))
                );
            }
        } catch (err) {
            return this.fail(err);
        }
    }
}
