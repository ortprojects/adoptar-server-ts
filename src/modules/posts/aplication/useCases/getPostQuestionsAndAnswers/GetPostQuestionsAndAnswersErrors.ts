import { UseCaseError } from '../../../../shared/core/UseCaseError';
import { Result } from '../../../../shared/core/Result';

export namespace GetPostQuestionsAndAnswersErrors {
    export class PostByIdNotFound extends Result<UseCaseError> {
        constructor() {
            super(false, {
                message: `Post not found`
            } as UseCaseError);
        }
    }
}
