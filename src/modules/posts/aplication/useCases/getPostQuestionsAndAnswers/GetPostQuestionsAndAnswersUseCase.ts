import { GenericAppError } from '../../../../shared/core/AppError';
import { Either, Result, left, right } from '../../../../shared/core/Result';
import { PostRepository } from '../../../infrastructure/repositories/PostRepository';
import { UseCase } from '../../../../shared/domain/UseCase';
import { LoggerImpl } from '../../../../shared/infrastructure';
import { GetPostQuestionsAndAnswersDTO } from './dtos/GetPostQuestionsAndAnswersDTO';
import { GetPostQuestionsAndAnswersErrors } from './GetPostQuestionsAndAnswersErrors';
import { QuestionEntity } from '../../../domain/QuestionEntity';

type Response = Either<GenericAppError.UnexpectedError | GetPostQuestionsAndAnswersErrors.PostByIdNotFound, Result<QuestionEntity[]>>;

export class GetPostQuestionsAndAnswersUseCase
    implements UseCase<GetPostQuestionsAndAnswersDTO, Promise<Response>> {
    private postRepository: PostRepository;

    constructor(postRepository: PostRepository) {
        this.postRepository = postRepository;
    }

    async execute(req: GetPostQuestionsAndAnswersDTO): Promise<Response> {
        LoggerImpl.writeInfoLog('[GetPostQuestionsAndAnswersUseCase::execute] Get post questions and answers.');

        try {
            await this.postRepository.getPostById(req.postId);
        } catch (error) {
            return left(new GetPostQuestionsAndAnswersErrors.PostByIdNotFound()) as Response;
        }

        try {
            const questions = await this.postRepository.getQuestionAndAnswersByPostId(req.postId);

            return right(Result.ok<QuestionEntity[]>(questions));
        } catch (err) {
            return left(new GenericAppError.UnexpectedError(err));
        }
    }
}
