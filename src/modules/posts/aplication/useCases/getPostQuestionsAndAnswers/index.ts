import { postRepositoryImpl } from '../../../infrastructure/repositories';
import { GetPostQuestionsAndAnswersUseCase } from './GetPostQuestionsAndAnswersUseCase';
import { GetPostQuestionsAndAnswersController } from './GetPostQuestionsAndAnswersController';

const getPostQuestionsAndAnswersUseCase = new GetPostQuestionsAndAnswersUseCase(postRepositoryImpl);
const getPostQuestionsAndAnswersContoller = new GetPostQuestionsAndAnswersController(getPostQuestionsAndAnswersUseCase);

export { getPostQuestionsAndAnswersUseCase, getPostQuestionsAndAnswersContoller };
