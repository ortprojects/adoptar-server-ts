import { BaseController } from '../../../../shared/infrastructure/http/models/BaseController';
import { PostMapper } from '../../../mappers/PostMapper';
import { GetPostsByUserIdUseCase } from './GetPostsByUserIdUseCase';
import { GetPostsByUserIdDTO } from './dtos/GetPostsByUserIdDTO';
import { GetPostsByUserIdErrors } from './GetPostByUserIdErrors';
import { PostEntity } from '../../../domain';

export class GetPostsByUserIdController extends BaseController {
    private useCase: GetPostsByUserIdUseCase;

    constructor(useCase: GetPostsByUserIdUseCase) {
        super();
        this.useCase = useCase;
    }

    async executeImpl(): Promise<any> {
        const { userId } = this.req.params;

        const dto: GetPostsByUserIdDTO = {
            userId
        };

        try {
            const result = await this.useCase.execute(dto);

            if (result.isLeft()) {
                const error = result.value;

                switch (error.constructor) {
                    case GetPostsByUserIdErrors.UserIdNotFound:
                        return this.conflict(error.errorValue().message);
                    default:
                        return this.fail(error.errorValue().message);
                }
            } else {
                const posts = result.value.getValue();

                return this.ok<PostEntity[]>(this.res, posts.map(post => PostMapper.toJSON(post)));
            }
        } catch (err) {
            return this.fail(err);
        }
    }
}
