import { PostEntity } from '../../../../posts/domain';
import { GenericAppError } from '../../../../shared/core/AppError';
import { Either, left, Result, right } from '../../../../shared/core/Result';
import { UseCase } from '../../../../shared/domain/UseCase';
import { UserRepository } from '../../../../users/infrastructure/repositories/UserRepository';
import { PostRepository } from '../../../infrastructure/repositories/PostRepository';
import { GetPostsByUserIdDTO } from './dtos/GetPostsByUserIdDTO';
import { GetPostsByUserIdErrors } from './GetPostByUserIdErrors';

type Response = Either<
    GetPostsByUserIdErrors.UserIdNotFound | GenericAppError.UnexpectedError,
    Result<PostEntity[]>
>;

export class GetPostsByUserIdUseCase implements UseCase<GetPostsByUserIdDTO, Promise<Response>> {
    private userRepositoy: UserRepository;
    private postRepositoy: PostRepository;

    constructor(userRepositoy: UserRepository, postRepositoy: PostRepository) {
        this.userRepositoy = userRepositoy;
        this.postRepositoy = postRepositoy;
    }

    async execute(request: GetPostsByUserIdDTO): Promise<Response> {
        const { userId } = request;
        try {
            await this.userRepositoy.getUserByUserId(userId);
        } catch (error) {
            return left(new GetPostsByUserIdErrors.UserIdNotFound()) as Response;
        }

        try {
            const posts = await this.postRepositoy.getPostByUserId(userId);

            return right(Result.ok<PostEntity[]>(posts));
        } catch (err) {
            return left(new GenericAppError.UnexpectedError(err)) as Response;
        }
    }
}
