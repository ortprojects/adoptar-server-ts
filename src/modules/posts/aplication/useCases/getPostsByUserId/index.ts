import { userRepositoryImpl } from '../../../../users/infrastructure/repositories';
import { postRepositoryImpl } from '../../../infrastructure/repositories';
import { GetPostsByUserIdController } from './GetPostByUserIdController';
import { GetPostsByUserIdUseCase } from './GetPostsByUserIdUseCase';

const getPostsByUserIdUseCase = new GetPostsByUserIdUseCase(userRepositoryImpl, postRepositoryImpl);
const getPostsByUserIdContoller = new GetPostsByUserIdController(getPostsByUserIdUseCase);

export { getPostsByUserIdUseCase, getPostsByUserIdContoller };
