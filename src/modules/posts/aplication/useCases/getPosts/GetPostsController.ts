import { BaseController } from '../../../../shared/infrastructure/http/models/BaseController';
import { GetPostsDTO } from './dtos/GetPostsDTO';
import { GetPostsUseCase } from './GetPostsUseCase';
import { GetPostsErrors } from './GetPostsErrors';
import { PostMapper } from '../../../mappers/PostMapper';
import { PostEntity } from '../../../domain';

export class GetPostsController extends BaseController {
    private useCase: GetPostsUseCase;

    constructor(useCase: GetPostsUseCase) {
        super();
        this.useCase = useCase;
    }

    async executeImpl(): Promise<any> {
        const {
            latitude,
            longitude,
            radio,
            petSexType,
            petType,
            petSizeType,
            petCastratedType,
            petMonitoring,
            postType,
            petAgeType
        } = this.req.query;

        const dto: GetPostsDTO = {
            latitude: Number(latitude),
            longitude: Number(longitude),
            radio: Number(radio),
            petSizeType: petSizeType as string,
            petType: petType as string,
            petSexType: petSexType as string,
            petCastratedType: petCastratedType as string,
            petMonitoring: Boolean(petMonitoring),
            petAgeTypes: petAgeType as string[],
            postType: postType as string
        };

        try {
            const result = await this.useCase.execute(dto);

            if (result.isLeft()) {
                const error = result.value;

                console.log(error);
                switch (error.constructor) {
                    case GetPostsErrors.NotFoundByFilter:
                        return this.conflict(error.errorValue().message);
                    default:
                        return this.fail(error.errorValue().message);
                }
            } else {
                const posts = result.value.getValue();

                return this.ok<PostEntity[]>(this.res, posts.map((p) => PostMapper.toJSON(p)));
            }
        } catch (err) {
            return this.fail(err);
        }
    }
}
