import { PostEntity } from "../../../../domain";

export interface GetPostsResponseDTO {
    posts: PostEntity[];
}
