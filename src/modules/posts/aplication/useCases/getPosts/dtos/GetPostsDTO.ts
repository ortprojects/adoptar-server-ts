export interface GetPostsDTO{
    latitude: number,
    longitude: number,
    radio: number,
    petSexType?: string,
    petType?: string,
    petSizeType?: string,
    petCastratedType?: string,
    petMonitoring?: boolean,
    petAgeTypes?: string[],
    postType?: string 
}