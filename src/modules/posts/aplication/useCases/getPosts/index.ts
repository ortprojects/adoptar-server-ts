import { GetPostsUseCase } from './GetPostsUseCase';
import { GetPostsController } from './GetPostsController';
import { postRepositoryImpl } from '../../../infrastructure/repositories';

const getPostsUseCase = new GetPostsUseCase(postRepositoryImpl);
const getPostsContoller = new GetPostsController(getPostsUseCase);

export {
    getPostsUseCase,
    getPostsContoller
}
