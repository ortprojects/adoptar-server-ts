import { GenericAppError } from '../../../../shared/core/AppError';
import { Either, Result, left, right } from '../../../../shared/core/Result';
import { PostRepository } from '../../../infrastructure/repositories/PostRepository';
import { UseCase } from '../../../../shared/domain/UseCase';
import { GetPostsDTO } from './dtos/GetPostsDTO';
import { PostEntity } from '../../../domain';
import { LoggerImpl } from '../../../../shared/infrastructure';

type Response = Either<GenericAppError.UnexpectedError, Result<PostEntity[]>>;

export class GetPostsUseCase implements UseCase<GetPostsDTO, Promise<Response>> {
    private postRepository: PostRepository;

    constructor(postRepository: PostRepository) {
        this.postRepository = postRepository;
    }

    async execute(req: GetPostsDTO): Promise<Response> {
        LoggerImpl.writeInfoLog('[GetPostsUseCase::execute] Get posts.');

        try {
            const posts = await this.postRepository.getPosts(req);
            
            return right(Result.ok<PostEntity[]>(posts));
        } catch (err) {
            return left(new GenericAppError.UnexpectedError(err));
        }
    }
}
