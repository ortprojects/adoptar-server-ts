import { UseCaseError } from '../../../../shared/core/UseCaseError';
import { Result } from '../../../../shared/core/Result';

export namespace GetPostsErrors {
    export class NotFoundByFilter extends Result<UseCaseError> {
        constructor(filter: string) {
            super(false, {
                message: `No posts found using ${filter}`
            } as UseCaseError);
        }
    }
    export class PetSexTypeFilterNotFound extends Result<UseCaseError> {
        constructor() {
            super(false, {
                message: `{petSexType} param cannot be empty`
            } as UseCaseError);
        }
    }
}
