import { Guard } from '../../../../shared/core/Guard';
import { Result } from '../../../../shared/core/Result';
import { ValueObject } from '../../../../shared/domain/ValueObject';

interface QuestionTextProps {
    value: string;
}

export class QuestionText extends ValueObject<QuestionTextProps> {
    get value(): string {
        return this.props.value;
    }

    private constructor(props: QuestionTextProps) {
        super(props);
    }

    public static create(question: string): Result<QuestionText> {
        const guardResult = Guard.againstNullOrUndefined(question, 'question');

        if (!guardResult.succeeded) {
            return Result.fail<QuestionText>(guardResult.message);
        } else {
            return Result.ok<QuestionText>(new QuestionText({ value: question }));
        }
    }
}
