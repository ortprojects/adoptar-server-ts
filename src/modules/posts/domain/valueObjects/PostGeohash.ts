import { ValueObject } from '../../../shared/domain/ValueObject';
import { Result } from '../../../shared/core/Result';
import { Guard } from '../../../shared/core/Guard';
import geohash from 'ngeohash';

interface PostGeohashProps {
    value: string;
}

export class PostGeohash extends ValueObject<PostGeohashProps> {
    get value(): string {
        return this.props.value;
    }

    private constructor(props: PostGeohashProps) {
        super(props);
    }

    public static create(latitude: number, longitude: number): Result<PostGeohash> {
        const guardedProps = [
            { argument: latitude, argumentName: 'latitude' },
            { argument: longitude, argumentName: 'longitude' }
        ];

        const guardResult = Guard.againstNullOrUndefinedBulk(guardedProps);

        if (!guardResult.succeeded) {
            return Result.fail<PostGeohash>(guardResult.message);
        } else {
            const encodeGeohash = geohash.encode(latitude, longitude);

            return Result.ok<PostGeohash>(new PostGeohash({ value: encodeGeohash }));
        }
    }

    public static createFromExisting(hash: string): Result<PostGeohash> {
        return Result.ok<PostGeohash>(new PostGeohash({ value: hash }));
    }
}
