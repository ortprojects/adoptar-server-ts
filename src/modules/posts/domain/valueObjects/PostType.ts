import { ValueObject } from '../../../shared/domain/ValueObject';
import { Result } from '../../../shared/core/Result';
import { Guard } from '../../../shared/core/Guard';

interface PostTypeProps {
    value: string;
}

export class PostType extends ValueObject<PostTypeProps> {
    get value(): string {
        return this.props.value;
    }

    private constructor(props: PostTypeProps) {
        super(props);
    }

    public static create(postType: string): Result<PostType> {
        let guardResult = Guard.againstNullOrUndefined(postType, 'postType');

        if (!guardResult.succeeded) {
            return Result.fail<PostType>(guardResult.message);
        } else {
            return Result.ok<PostType>(new PostType({ value: postType }));
        }
    }
}
