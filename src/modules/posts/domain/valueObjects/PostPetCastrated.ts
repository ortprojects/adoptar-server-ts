import { ValueObject } from '../../../shared/domain/ValueObject';
import { Result } from '../../../shared/core/Result';
import { Guard } from '../../../shared/core/Guard';

interface PostPetCastratedProps {
    value: string;
}

export class PostPetCastrated extends ValueObject<PostPetCastratedProps> {
    get value(): string {
        return this.props.value;
    }

    private constructor(props: PostPetCastratedProps) {
        super(props);
    }

    public static create(petCastrated: string): Result<PostPetCastrated> {
        let guardResult = Guard.againstNullOrUndefined(petCastrated, 'petCastrated');

        if (!guardResult.succeeded) {
            return Result.fail<PostPetCastrated>(guardResult.message);
        } else {
            return Result.ok<PostPetCastrated>(new PostPetCastrated({ value: petCastrated }));
        }
    }
}
