import { ValueObject } from '../../../shared/domain/ValueObject';
import { Result } from '../../../shared/core/Result';
import { Guard } from '../../../shared/core/Guard';

interface PostPetSexProps {
    value: string;
}

export class PostPetSex extends ValueObject<PostPetSexProps> {
    get value(): string {
        return this.props.value;
    }

    private constructor(props: PostPetSexProps) {
        super(props);
    }

    public static create(petSex: string): Result<PostPetSex> {
        let guardResult = Guard.againstNullOrUndefined(petSex, 'petSex');

        if (!guardResult.succeeded) {
            return Result.fail<PostPetSex>(guardResult.message);
        } else {
            return Result.ok<PostPetSex>(new PostPetSex({ value: petSex }));
        }
    }
}
