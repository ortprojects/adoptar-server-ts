import { ValueObject } from '../../../shared/domain/ValueObject';
import { Result } from '../../../shared/core/Result';
import { Guard } from '../../../shared/core/Guard';

interface PostPetMonitoringProps {
    value: boolean;
}

export class PostPetMonitoring extends ValueObject<PostPetMonitoringProps> {
    get value(): boolean {
        return this.props.value;
    }

    private constructor(props: PostPetMonitoringProps) {
        super(props);
    }

    public static create(petMonitoring: boolean | string): Result<PostPetMonitoring> {
        let guardResult = Guard.againstNullOrUndefined(petMonitoring, 'petMonitoring');

        const monitoring = petMonitoring === true || petMonitoring == 'true';

        if (!guardResult.succeeded) {
            return Result.fail<PostPetMonitoring>(guardResult.message);
        } else {
            return Result.ok<PostPetMonitoring>(new PostPetMonitoring({ value: monitoring }));
        }
    }
}
