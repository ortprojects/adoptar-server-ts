import { ValueObject } from '../../../shared/domain/ValueObject';
import { Result } from '../../../shared/core/Result';
import { Guard } from '../../../shared/core/Guard';

interface PostedCreatedByUserIdProps {
    value: string;
}

export class PostCreatedByUserId extends ValueObject<PostedCreatedByUserIdProps> {
    get value(): string {
        return this.props.value;
    }

    private constructor(props: PostedCreatedByUserIdProps) {
        super(props);
    }

    public static create(userId: string): Result<PostCreatedByUserId> {
        const guardResult = Guard.againstNullOrUndefined(userId, 'userId');

        if (!guardResult.succeeded) {
            return Result.fail<PostCreatedByUserId>(guardResult.message);
        } else {
            return Result.ok<PostCreatedByUserId>(new PostCreatedByUserId({ value: userId }));
        }
    }
}
