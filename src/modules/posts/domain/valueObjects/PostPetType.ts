import { ValueObject } from '../../../shared/domain/ValueObject';
import { Result } from '../../../shared/core/Result';
import { Guard } from '../../../shared/core/Guard';

interface PostPetTypeProps {
    value: string;
}

export class PostPetType extends ValueObject<PostPetTypeProps> {
    get value(): string {
        return this.props.value;
    }

    private constructor(props: PostPetTypeProps) {
        super(props);
    }

    public static create(petType: string): Result<PostPetType> {
        let guardResult = Guard.againstNullOrUndefined(petType, 'petType');

        if (!guardResult.succeeded) {
            return Result.fail<PostPetType>(guardResult.message);
        } else {
            return Result.ok<PostPetType>(new PostPetType({ value: petType }));
        }
    }
}
