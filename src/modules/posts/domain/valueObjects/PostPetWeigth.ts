import { ValueObject } from '../../../shared/domain/ValueObject';
import { Result } from '../../../shared/core/Result';
import { Guard } from '../../../shared/core/Guard';

interface PostPetWeigthProps {
    value: number;
}

export class PostPetWeigth extends ValueObject<PostPetWeigthProps> {
    get value(): number {
        return this.props.value;
    }

    private constructor(props: PostPetWeigthProps) {
        super(props);
    }

    public static create(petWeigth: number): Result<PostPetWeigth> {
        const guardResult = Guard.againstNullOrUndefined(petWeigth, 'petWeigth');

        if (!guardResult.succeeded) {
            return Result.fail<PostPetWeigth>(guardResult.message);
        } else {
            return Result.ok<PostPetWeigth>(new PostPetWeigth({ value: petWeigth }));
        }
    }
}
