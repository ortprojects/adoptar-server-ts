import { ValueObject } from '../../../shared/domain/ValueObject';
import { Result } from '../../../shared/core/Result';

interface PostDateProps {
    value: Date;
}

export class PostDate extends ValueObject<PostDateProps> {
    get value(): Date {
        return this.props.value;
    }

    private constructor(props: PostDateProps) {
        super(props);
    }

    public static create(date?: Date): Result<PostDate> {
        return Result.ok<PostDate>(new PostDate({ value: date ? date : new Date() }));
    }
}
