import { ValueObject } from '../../../shared/domain/ValueObject';
import { Result } from '../../../shared/core/Result';
import { Guard } from '../../../shared/core/Guard';

interface PostUserIdProps {
    value: string;
}

export class PostUserId extends ValueObject<PostUserIdProps> {
    get value(): string {
        return this.props.value;
    }

    private constructor(props: PostUserIdProps) {
        super(props);
    }

    public static create(userId?: string): Result<PostUserId> {
        return Result.ok<PostUserId>(new PostUserId({ value: userId ? userId : "" }));
    }
}
