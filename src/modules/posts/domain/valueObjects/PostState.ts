import { ValueObject } from '../../../shared/domain/ValueObject';
import { Result } from '../../../shared/core/Result';
import { Guard } from '../../../shared/core/Guard';
import { PostStateType } from '../enums/PostStateType';

interface PostStateProps {
    value: PostStateType;
}

export class PostState extends ValueObject<PostStateProps> {
    get value(): string {
        return this.props.value;
    }

    private constructor(props: PostStateProps) {
        super(props);
    }

    public static create(postState?: string): Result<PostState> {
        let state;

        switch (postState) {
            case 'Pausado':
                state = PostStateType.PAUSE;
                break;
            case 'Finalizado':
                state = PostStateType.CLOSE;
                break;
            default:
                state = PostStateType.ACTIVE;
                break;
        }

        return Result.ok<PostState>(new PostState({ value: state }));
    }
}
