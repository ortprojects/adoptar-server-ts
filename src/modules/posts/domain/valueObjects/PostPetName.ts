import { ValueObject } from '../../../shared/domain/ValueObject';
import { Result } from '../../../shared/core/Result';
import { Guard } from '../../../shared/core/Guard';

interface PostPetNameProps {
    value: string;
}

export class PostPetName extends ValueObject<PostPetNameProps> {
    get value(): string {
        return this.props.value;
    }

    private constructor(props: PostPetNameProps) {
        super(props);
    }

    public static create(petName: string): Result<PostPetName> {
        let guardResult = Guard.againstNullOrUndefined(petName, 'petName');

        if (!guardResult.succeeded) {
            return Result.fail<PostPetName>(guardResult.message);
        } else {
            return Result.ok<PostPetName>(new PostPetName({ value: petName }));
        }
    }
}
