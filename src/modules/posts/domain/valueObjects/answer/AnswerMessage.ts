import { Guard } from '../../../../shared/core/Guard';
import { Result } from '../../../../shared/core/Result';
import { ValueObject } from '../../../../shared/domain/ValueObject';

interface AnswerMessageProps {
    value: string;
}

export class AnswerMessage extends ValueObject<AnswerMessageProps> {
    get value(): string {
        return this.props.value;
    }

    private constructor(props: AnswerMessageProps) {
        super(props);
    }

    public static create(message: string): Result<AnswerMessage> {
        const guardResult = Guard.againstNullOrUndefined(message, 'message');

        if (!guardResult.succeeded) {
            return Result.fail<AnswerMessage>(guardResult.message);
        } else {
            return Result.ok<AnswerMessage>(new AnswerMessage({ value: message }));
        }
    }
}
