import { ValueObject } from '../../../shared/domain/ValueObject';
import { Result } from '../../../shared/core/Result';
import { Guard } from '../../../shared/core/Guard';

interface PostPetBreedProps {
    value: string;
}

export class PostPetBreed extends ValueObject<PostPetBreedProps> {
    get value(): string {
        return this.props.value;
    }

    private constructor(props: PostPetBreedProps) {
        super(props);
    }

    public static create(petBreed: string): Result<PostPetBreed> {
        let guardResult = Guard.againstNullOrUndefined(petBreed, 'petBreed');

        if (!guardResult.succeeded) {
            return Result.fail<PostPetBreed>(guardResult.message);
        } else {
            return Result.ok<PostPetBreed>(new PostPetBreed({ value: petBreed }));
        }
    }
}
