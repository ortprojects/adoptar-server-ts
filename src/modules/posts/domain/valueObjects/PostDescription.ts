import { ValueObject } from '../../../shared/domain/ValueObject';
import { Result } from '../../../shared/core/Result';
import { Guard } from '../../../shared/core/Guard';

interface PostDescriptionProps {
    value: string;
}

export class PostDescription extends ValueObject<PostDescriptionProps> {
    get value(): string {
        return this.props.value;
    }

    private constructor(props: PostDescriptionProps) {
        super(props);
    }

    public static create(description: string): Result<PostDescription> {
        const guardResult = Guard.againstNullOrUndefined(description, 'description');

        if (!guardResult.succeeded) {
            return Result.fail<PostDescription>(guardResult.message);
        } else {
            return Result.ok<PostDescription>(new PostDescription({ value: description }));
        }
    }
}
