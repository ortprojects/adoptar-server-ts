import { ValueObject } from '../../../shared/domain/ValueObject';
import { Result } from '../../../shared/core/Result';
import { Guard } from '../../../shared/core/Guard';

interface PostPetAgeProps {
    value: string;
}

export class PostPetAge extends ValueObject<PostPetAgeProps> {
    get value(): string {
        return this.props.value;
    }

    private constructor(props: PostPetAgeProps) {
        super(props);
    }

    public static create(petAge: string): Result<PostPetAge> {
        let guardResult = Guard.againstNullOrUndefined(petAge, 'petAge');

        if (!guardResult.succeeded) {
            return Result.fail<PostPetAge>(guardResult.message);
        } else {
            return Result.ok<PostPetAge>(new PostPetAge({ value: petAge }));
        }
    }
}
