import { ValueObject } from '../../../shared/domain/ValueObject';
import { Result } from '../../../shared/core/Result';
import { Guard } from '../../../shared/core/Guard';

interface PostPetSizeProps {
    value: string;
}

export class PostPetSize extends ValueObject<PostPetSizeProps> {
    get value(): string {
        return this.props.value;
    }

    private constructor(props: PostPetSizeProps) {
        super(props);
    }

    public static create(petSize: string): Result<PostPetSize> {
        let guardResult = Guard.againstNullOrUndefined(petSize, 'petSize');

        if (!guardResult.succeeded) {
            return Result.fail<PostPetSize>(guardResult.message);
        } else {
            return Result.ok<PostPetSize>(new PostPetSize({ value: petSize }));
        }
    }
}
