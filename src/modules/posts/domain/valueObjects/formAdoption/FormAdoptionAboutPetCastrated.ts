import { Guard } from "../../../../shared/core/Guard";
import { Result } from "../../../../shared/core/Result";
import { ValueObject } from "../../../../shared/domain/ValueObject";


interface FormAdoptionAboutPetCastratedProps {
    value: string;
}

export class FormAdoptionAboutPetCastrated extends ValueObject<FormAdoptionAboutPetCastratedProps> {
    get value(): string {
        return this.props.value;
    }

    private constructor(props: FormAdoptionAboutPetCastratedProps) {
        super(props);
    }

    public static create(aboutPetCastrated: string): Result<FormAdoptionAboutPetCastrated> {
        const guardResult = Guard.againstNullOrUndefined(aboutPetCastrated, 'aboutPetCastrated');

        if (!guardResult.succeeded) {
            return Result.fail<FormAdoptionAboutPetCastrated>(guardResult.message);
        } else {
            return Result.ok<FormAdoptionAboutPetCastrated>(new FormAdoptionAboutPetCastrated({ value: aboutPetCastrated }));
        }
    }
}
