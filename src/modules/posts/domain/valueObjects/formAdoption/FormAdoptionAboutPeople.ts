import { Guard } from "../../../../shared/core/Guard";
import { Result } from "../../../../shared/core/Result";
import { ValueObject } from "../../../../shared/domain/ValueObject";


interface FormAdoptionAboutPeopleProps {
    value: string;
}

export class FormAdoptionAboutPeople extends ValueObject<FormAdoptionAboutPeopleProps> {
    get value(): string {
        return this.props.value;
    }

    private constructor(props: FormAdoptionAboutPeopleProps) {
        super(props);
    }

    public static create(aboutPeople: string): Result<FormAdoptionAboutPeople> {
        const guardResult = Guard.againstNullOrUndefined(aboutPeople, 'aboutPeople');

        if (!guardResult.succeeded) {
            return Result.fail<FormAdoptionAboutPeople>(guardResult.message);
        } else {
            return Result.ok<FormAdoptionAboutPeople>(new FormAdoptionAboutPeople({ value: aboutPeople }));
        }
    }
}
