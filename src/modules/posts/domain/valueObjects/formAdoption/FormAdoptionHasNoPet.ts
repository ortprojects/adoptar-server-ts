import { Guard } from "../../../../shared/core/Guard";
import { Result } from "../../../../shared/core/Result";
import { ValueObject } from "../../../../shared/domain/ValueObject";


interface FormAdoptionHasNoPetProps {
    value: string;
}

export class FormAdoptionHasNoPet extends ValueObject<FormAdoptionHasNoPetProps> {
    get value(): string {
        return this.props.value;
    }

    private constructor(props: FormAdoptionHasNoPetProps) {
        super(props);
    }

    public static create(hasNoPet: string): Result<FormAdoptionHasNoPet> {
        const guardResult = Guard.againstNullOrUndefined(hasNoPet, 'hasNoPet');

        if (!guardResult.succeeded) {
            return Result.fail<FormAdoptionHasNoPet>(guardResult.message);
        } else {
            return Result.ok<FormAdoptionHasNoPet>(new FormAdoptionHasNoPet({ value: hasNoPet }));
        }
    }
}
