import { Guard } from "../../../../shared/core/Guard";
import { Result } from "../../../../shared/core/Result";
import { ValueObject } from "../../../../shared/domain/ValueObject";


interface FormAdoptionAboutPeopleProps {
    value: string;
}

export class FormAdoptionAboutPets extends ValueObject<FormAdoptionAboutPeopleProps> {
    get value(): string {
        return this.props.value;
    }

    private constructor(props: FormAdoptionAboutPeopleProps) {
        super(props);
    }

    public static create(aboutPets: string): Result<FormAdoptionAboutPets> {
        const guardResult = Guard.againstNullOrUndefined(aboutPets, 'aboutPets');

        if (!guardResult.succeeded) {
            return Result.fail<FormAdoptionAboutPets>(guardResult.message);
        } else {
            return Result.ok<FormAdoptionAboutPets>(new FormAdoptionAboutPets({ value: aboutPets }));
        }
    }
}
