import { Guard } from "../../../../shared/core/Guard";
import { Result } from "../../../../shared/core/Result";
import { ValueObject } from "../../../../shared/domain/ValueObject";


interface FormAdoptionAboutLocationTypeProps {
    value: string;
}

export class FormAdoptionLocationType extends ValueObject<FormAdoptionAboutLocationTypeProps> {
    get value(): string {
        return this.props.value;
    }

    private constructor(props: FormAdoptionAboutLocationTypeProps) {
        super(props);
    }

    public static create(locationType: string): Result<FormAdoptionLocationType> {
        const guardResult = Guard.againstNullOrUndefined(locationType, 'locationType');

        if (!guardResult.succeeded) {
            return Result.fail<FormAdoptionLocationType>(guardResult.message);
        } else {
            return Result.ok<FormAdoptionLocationType>(new FormAdoptionLocationType({ value: locationType }));
        }
    }
}
