import { ValueObject } from '../../../shared/domain/ValueObject';
import { Result } from '../../../shared/core/Result';
import { Guard } from '../../../shared/core/Guard';

interface PostTitleProps {
    value: string;
}

export class PostTitle extends ValueObject<PostTitleProps> {
    get value(): string {
        return this.props.value;
    }

    private constructor(props: PostTitleProps) {
        super(props);
    }

    public static create(postType: string): Result<PostTitle> {
        let guardResult = Guard.againstNullOrUndefined(postType, 'postTitle');

        if (!guardResult.succeeded) {
            return Result.fail<PostTitle>(guardResult.message);
        } else {
            return Result.ok<PostTitle>(new PostTitle({ value: postType }));
        }
    }
}
