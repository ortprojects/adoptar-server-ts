import { AggregateRoot } from '../../shared/domain/AggregateRoot';
import { UniqueEntityID } from '../../shared/domain/valueObjects/UniqueEntityID';
import { Result } from '../../shared/core/Result';
import { Guard } from '../../shared/core/Guard';

import { PostPetName } from './valueObjects/PostPetName';
import { PostPetSex } from './valueObjects/PostPetSex';
import { PostPetType } from './valueObjects/PostPetType';
import { PostType } from './valueObjects/PostType';
import { PostPetBreed } from './valueObjects/PostPetBreed';
import { PostPetAge } from './valueObjects/PostPetAge';
import { PostPetSize } from './valueObjects/PostPetSize';
import { PostPetWeigth } from './valueObjects/PostPetWeigth';
import { PostPetCastrated } from './valueObjects/PostPetCastrated';
import { PostDescription } from './valueObjects/PostDescription';
import { PostPetMonitoring } from './valueObjects/PostPetMonitoring';
import { PostDate } from './valueObjects/PostDate';
import { PostGeohash } from './valueObjects/PostGeohash';
import { UserEntity } from '../../users/domain/UserEntity';
import { PostTitle } from './valueObjects/PostTitle';
import { PostCreatedByUserId } from './valueObjects/PostCreatedByUserId';
import { PostState } from './valueObjects/PostState';

interface PostEntityProps {
    petName: PostPetName;
    petSex: PostPetSex;
    petType: PostPetType;
    postType: PostType;
    petBreed: PostPetBreed;
    petAge: PostPetAge;
    petSize: PostPetSize;
    petWeigth: PostPetWeigth;
    petCastrated: PostPetCastrated;
    postDescription: PostDescription;
    petMonitoring: PostPetMonitoring;
    imageFiles?: string[];
    geohash: PostGeohash;
    createdAt?: PostDate;
    postTitle: PostTitle;
    createdByUserId: PostCreatedByUserId;
    createdBy?: UserEntity;
    adoptedBy?: UserEntity;
    adoptedByUserId?: UniqueEntityID;
    latitude: number;
    longitude: number;
    state: PostState;
}

export class PostEntity extends AggregateRoot<PostEntityProps> {
    get id(): UniqueEntityID {
        return this._id;
    }

    get petName(): PostPetName {
        return this.props.petName;
    }

    get petSex(): PostPetSex {
        return this.props.petSex;
    }

    get petType(): PostPetType {
        return this.props.petType;
    }

    get postType(): PostType {
        return this.props.postType;
    }

    get petBreed(): PostPetBreed {
        return this.props.petBreed;
    }

    get petAge(): PostPetAge {
        return this.props.petAge;
    }

    get petSize(): PostPetSize {
        return this.props.petSize;
    }

    get petWeigth(): PostPetWeigth {
        return this.props.petWeigth;
    }

    get petCastrated(): PostPetCastrated {
        return this.props.petCastrated;
    }

    get postDescription(): PostDescription {
        return this.props.postDescription;
    }

    get petMonitoring(): PostPetMonitoring {
        return this.props.petMonitoring;
    }

    get imageFiles(): string[] {
        return this.props.imageFiles;
    }

    get createdAt(): PostDate {
        return this.props.createdAt;
    }

    get geohash(): PostGeohash {
        return this.props.geohash;
    }

    get postedBy(): PostCreatedByUserId {
        return this.props.createdByUserId;
    }

    get createdBy(): UserEntity {
        return this.props.createdBy;
    }

    get adoptedBy(): UserEntity {
        return this.props.adoptedBy;
    }

    get createdByUserId(): PostCreatedByUserId {
        return this.props.createdByUserId;
    }

    get adoptedByUserId(): UniqueEntityID {
        return this.props.adoptedByUserId;
    }

    get postTitle(): PostTitle {
        return this.props.postTitle;
    }

    get state(): PostState {
        return this.props.state;
    }

    get latitude(): number {
        return this.props.latitude;
    }

    get longitude(): number {
        return this.props.longitude;
    }

    set petName(value: PostPetName) {
        this.props.petName = value;
    }

    set petSex(value: PostPetSex) {
        this.props.petSex = value;
    }

    set petType(value: PostPetType) {
        this.props.petType = value;
    }

    set postType(value: PostType) {
        this.props.postType = value;
    }

    set petBreed(value: PostPetBreed) {
        this.props.petBreed = value;
    }

    set petAge(value: PostPetAge) {
        this.props.petAge = value;
    }

    set petSize(value: PostPetSize) {
        this.props.petSize = value;
    }

    set petWeigth(value: PostPetWeigth) {
        this.props.petWeigth = value;
    }

    set petCastrated(value: PostPetCastrated) {
        this.props.petCastrated = value;
    }

    set postDescription(value: PostDescription) {
        this.props.postDescription = value;
    }

    set petMonitoring(value: PostPetMonitoring) {
        this.props.petMonitoring = value;
    }

    set imageFiles(value: string[]) {
        this.props.imageFiles = value;
    }

    set createdAt(value: PostDate) {
        this.props.createdAt = value;
    }

    set geohash(value: PostGeohash) {
        this.props.geohash = value;
    }

    set postedBy(value: PostCreatedByUserId) {
        this.props.createdByUserId = value;
    }

    set createdBy(value: UserEntity) {
        this.props.createdBy = value;
    }

    set postTitle(value: PostTitle) {
        this.props.postTitle = value;
    }

    set state(value: PostState) {
        this.props.state = value;
    }

    set adoptedByUserId(value: UniqueEntityID) {
        this.props.adoptedByUserId = value;
    }

    private constructor(props: PostEntityProps, id?: UniqueEntityID) {
        super(props, id);
    }

    public static create(props: PostEntityProps, id?: UniqueEntityID): Result<PostEntity> {
        const guardedProps = [
            { argument: props.petName, argumentName: 'petName' },
            { argument: props.petSex, argumentName: 'petSex' },
            { argument: props.petType, argumentName: 'petType' },
            { argument: props.postType, argumentName: 'postType' },
            { argument: props.petBreed, argumentName: 'petBreed' },
            { argument: props.petAge, argumentName: 'petAge' },
            { argument: props.petSize, argumentName: 'petSize' },
            { argument: props.petWeigth, argumentName: 'petWeigth' },
            { argument: props.petCastrated, argumentName: 'petCastrated' },
            { argument: props.postDescription, argumentName: 'postDescription' },
            { argument: props.petMonitoring, argumentName: 'petMonitoring' },
            { argument: props.geohash, argumentName: 'geohash' },
            { argument: props.imageFiles, argumentName: 'imageFiles' },
            { argument: props.createdByUserId, argumentName: 'createdByUserId' },
            { argument: props.postTitle, argumentName: 'postTItle' },
            { argument: props.latitude, argumentName: 'latitude' },
            { argument: props.longitude, argumentName: 'longitude' }
        ];

        const guardResult = Guard.againstNullOrUndefinedBulk(guardedProps);

        if (!guardResult.succeeded) {
            return Result.fail<PostEntity>(guardResult.message);
        } else {
            const post = new PostEntity(
                {
                    ...props,
                    petName: props.petName,
                    petSex: props.petSex,
                    petType: props.petType,
                    postType: props.postType,
                    petBreed: props.petBreed,
                    petAge: props.petAge,
                    petSize: props.petSize,
                    petWeigth: props.petWeigth,
                    petCastrated: props.petCastrated,
                    postDescription: props.postDescription,
                    petMonitoring: props.petMonitoring,
                    createdAt: props.createdAt,
                    geohash: props.geohash,
                    imageFiles: props.imageFiles,
                    createdByUserId: props.createdByUserId,
                    adoptedBy: props.adoptedBy,
                    postTitle: props.postTitle,
                    state: props.state,
                    latitude: props.latitude,
                    longitude: props.longitude,
                    adoptedByUserId: props.adoptedByUserId,
                },
                id
            );

            return Result.ok<PostEntity>(post);
        }
    }
}
