import { AggregateRoot } from '../../shared/domain/AggregateRoot';
import { UniqueEntityID } from '../../shared/domain/valueObjects/UniqueEntityID';
import { Result } from '../../shared/core/Result';
import { Guard } from '../../shared/core/Guard';
import { QuestionText } from './valueObjects/question/QuestionText';
import { UserEntity } from '../../users/domain/UserEntity';
import { AnswerEntity } from './AnswerEntity';

interface QuestionEntityProps {
    text: QuestionText;
    createdByUserId: UniqueEntityID;
    createdAt: Date;
    postId: UniqueEntityID;
    createdBy?: UserEntity;
    answer?: AnswerEntity;
}

export class QuestionEntity extends AggregateRoot<QuestionEntityProps> {
    get id(): UniqueEntityID {
        return this._id;
    }

    get text(): QuestionText {
        return this.props.text;
    }

    get createdByUserId(): UniqueEntityID {
        return this.props.createdByUserId;
    }

    get createdAt(): Date {
        return this.props.createdAt;
    }

    get postId(): UniqueEntityID {
        return this.props.postId;
    }

    get createdBy(): UserEntity {
        return this.props.createdBy;
    }

    get answer(): AnswerEntity {
        return this.props.answer;
    }

    private constructor(props: QuestionEntityProps, id?: UniqueEntityID) {
        super(props, id);
    }

    public static create(props: QuestionEntityProps, id?: UniqueEntityID): Result<QuestionEntity> {
        const guardedProps = [
            { argument: props.text, argumentName: 'question' },
            { argument: props.createdByUserId, argumentName: 'createdByUserId' },
            { argument: props.postId, argumentName: 'postId' }
        ];

        const guardResult = Guard.againstNullOrUndefinedBulk(guardedProps);

        if (!guardResult.succeeded) {
            return Result.fail<QuestionEntity>(guardResult.message);
        } else {
            const question = new QuestionEntity(
                {
                    ...props,
                    text: props.text,
                    createdByUserId: props.createdByUserId,
                    createdAt: props.createdAt,
                    ...(props.answer ? { answer: props.answer } : {})
                },
                id
            );

            return Result.ok<QuestionEntity>(question);
        }
    }
}
