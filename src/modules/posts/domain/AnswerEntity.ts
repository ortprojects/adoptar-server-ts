import { AggregateRoot } from '../../shared/domain/AggregateRoot';
import { UniqueEntityID } from '../../shared/domain/valueObjects/UniqueEntityID';
import { Result } from '../../shared/core/Result';
import { Guard } from '../../shared/core/Guard';
import { UserEntity } from '../../users/domain/UserEntity';
import { AnswerMessage } from './valueObjects/answer/AnswerMessage';

interface AnswerEntityProps {
    message: AnswerMessage;
    createdByUserId: UniqueEntityID;
    createdAt: Date;
    createdBy?: UserEntity;
}

export class AnswerEntity extends AggregateRoot<AnswerEntityProps> {
    get id(): UniqueEntityID {
        return this._id;
    }

    get message(): AnswerMessage {
        return this.props.message;
    }

    get createdByUserId(): UniqueEntityID {
        return this.props.createdByUserId;
    }

    get createdAt(): Date {
        return this.props.createdAt;
    }

    get createdBy(): UserEntity {
        return this.props.createdBy;
    }

    private constructor(props: AnswerEntityProps, id?: UniqueEntityID) {
        super(props, id);
    }

    public static create(props: AnswerEntityProps, id?: UniqueEntityID): Result<AnswerEntity> {
        const guardedProps = [
            { argument: props.message, argumentName: 'question' },
            { argument: props.createdByUserId, argumentName: 'createdByUserId' },
        ];

        const guardResult = Guard.againstNullOrUndefinedBulk(guardedProps);

        if (!guardResult.succeeded) {
            return Result.fail<AnswerEntity>(guardResult.message);
        } else {
            const answer = new AnswerEntity(
                {
                    ...props,
                    message: props.message,
                    createdByUserId: props.createdByUserId,
                    createdAt: props.createdAt
                },
                id
            );

            return Result.ok<AnswerEntity>(answer);
        }
    }
}
