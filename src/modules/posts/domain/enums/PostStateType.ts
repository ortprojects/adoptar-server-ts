export enum PostStateType {
    ACTIVE = "Activo",
    PAUSE = "Pausado",
    CLOSE = "Finalizado"
}