export enum PetSizeType{
    SMALL = "Chico",
    MEDIUM = "Mediano",
    LARGE = "Grande"
}