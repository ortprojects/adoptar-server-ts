export enum PetAgeType{
    PUPPY = "Cachorro",
    YOUNG = "Joven",
    ADUL = "Adulto",
    VETERAN = "Veterano"
}