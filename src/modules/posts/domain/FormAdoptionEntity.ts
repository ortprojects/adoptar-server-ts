import { AggregateRoot } from '../../shared/domain/AggregateRoot';
import { UniqueEntityID } from '../../shared/domain/valueObjects/UniqueEntityID';
import { Result } from '../../shared/core/Result';
import { Guard } from '../../shared/core/Guard';
import { FormAdoptionAboutPeople } from './valueObjects/formAdoption/FormAdoptionAboutPeople';
import { FormAdoptionAboutPets } from './valueObjects/formAdoption/FormAdoptionAboutPets';
import { FormAdoptionAboutPetCastrated } from './valueObjects/formAdoption/FormAdoptionAboutPetCastrated';
import { FormAdoptionLocationType } from './valueObjects/formAdoption/FormAdoptionLocationType';
import { FormAdoptionHasNoPet } from './valueObjects/formAdoption/FormAdoptionHasNoPet';
import { UserEntity } from '../../users/domain/UserEntity';

interface FormAdoptionEntityProps {
    aboutPeople: FormAdoptionAboutPeople;
    aboutPets: FormAdoptionAboutPets;
    aboutPetCastrated: FormAdoptionAboutPetCastrated;
    locationType: FormAdoptionLocationType;
    hasNoPet: FormAdoptionHasNoPet;
    createdByUserId: string;
    postId: string;
    createdBy?: UserEntity
}

export class FormAdoptionEntity extends AggregateRoot<FormAdoptionEntityProps> {
    get id(): UniqueEntityID {
        return this._id;
    }

    get aboutPeople(): FormAdoptionAboutPeople {
        return this.props.aboutPeople;
    }

    get aboutPets(): FormAdoptionAboutPets {
        return this.props.aboutPets;
    }

    get aboutPetCastrated(): FormAdoptionAboutPetCastrated {
        return this.props.aboutPetCastrated;
    }

    get locationType(): FormAdoptionLocationType {
        return this.props.locationType;
    }

    get hasNoPet(): FormAdoptionHasNoPet {
        return this.props.hasNoPet;
    }

    get createdByUserId(): string {
        return this.props.createdByUserId;
    }

    get postId(): string {
        return this.props.postId;
    }

    get createdBy(): UserEntity {
        return this.props.createdBy;
    }

    private constructor(props: FormAdoptionEntityProps, id?: UniqueEntityID) {
        super(props, id);
    }

    public static create(props: FormAdoptionEntityProps, id?: UniqueEntityID): Result<FormAdoptionEntity> {
        const guardedProps = [
            { argument: props.aboutPeople, argumentName: 'aboutPeople' },
            { argument: props.aboutPets, argumentName: 'aboutPets' },
            { argument: props.aboutPetCastrated, argumentName: 'aboutPetCastrated' },
            { argument: props.locationType, argumentName: 'locationType' },
            { argument: props.hasNoPet, argumentName: 'hasNoPet' },
            { argument: props.createdByUserId, argumentName: 'createdByUserId' },
            { argument: props.postId, argumentName: 'postId' },
        ];

        const guardResult = Guard.againstNullOrUndefinedBulk(guardedProps);

        if (!guardResult.succeeded) {
            return Result.fail<FormAdoptionEntity>(guardResult.message);
        } else {
            const post = new FormAdoptionEntity(
                {
                    ...props,
                    aboutPeople: props.aboutPeople,
                    aboutPets: props.aboutPets,
                    aboutPetCastrated: props.aboutPetCastrated,
                    locationType: props.locationType,
                    hasNoPet: props.hasNoPet,
                    createdByUserId: props.createdByUserId,
                    postId: props.postId,
                },
                id
            );

            return Result.ok<FormAdoptionEntity>(post);
        }
    }
}
