import { Either, Result, left, right } from '../../../shared/core/Result';
import { UseCase } from '../../../shared/domain/UseCase';
import { GenericAppError } from '../../../shared/core/AppError';
import { PostRepository } from '../../../posts/infrastructure/repositories/PostRepository';
import { UserRepository } from '../../../users/infrastructure/repositories/UserRepository';
import { ConfirmAdoptionErrors } from './ConfirmAdoptionErrors';
import { ConfirmAdoptionDTO } from './dtos/ConfirmAdoptionDTO';
import { AdoptionEntity } from '../../domain/AdoptionEntity';
import { UniqueEntityID } from '../../../shared/domain/valueObjects/UniqueEntityID';
import { AdoptionRepository } from '../../infrastructure/repositories/AdoptionRepository';
import { PostState } from '../../../posts/domain/valueObjects/PostState';
import { PostStateType } from '../../../posts/domain/enums/PostStateType';
import { MailerRepository } from '../../../shared/core/mailer/MailerRepository';
import { NotificationEntity } from '../../../notifications/domain/NotificationEntity';
import { NotificationRepository } from '../../../notifications/infrastructure/repositories/NotificationRepository';
import { NotificationMessage } from '../../../notifications/domain/valueObjects/NotificationMessage';

type Response = Either<
    | GenericAppError.UnexpectedError
    | ConfirmAdoptionErrors.PostByIdNotFound
    | ConfirmAdoptionErrors.UserByIdSelectedNotFound
    | Result<any>,
    Result<void>
>;

export class ConfirmAdoptionUseCase implements UseCase<ConfirmAdoptionDTO, Promise<Response>> {
    private postRepository: PostRepository;
    private userRepositoy: UserRepository;
    private adoptionRepository: AdoptionRepository;
    private mailerRepository: MailerRepository;
    private notificationRepository: NotificationRepository;

    constructor(
        postRepository: PostRepository,
        userRepository: UserRepository,
        adoptionRepository: AdoptionRepository,
        mailerRepository: MailerRepository,
        notificationRepository: NotificationRepository
    ) {
        this.postRepository = postRepository;
        this.userRepositoy = userRepository;
        this.adoptionRepository = adoptionRepository;
        this.mailerRepository = mailerRepository;
        this.notificationRepository = notificationRepository;
    }

    async execute(req: ConfirmAdoptionDTO): Promise<Response> {
        let postEntity;
        let userSelected;
        try {
            postEntity = await this.postRepository.getPostById(req.postId);
        } catch (error) {
            return left(new ConfirmAdoptionErrors.PostByIdNotFound()) as Response;
        }

        try {
            userSelected = await this.userRepositoy.getUserByUserId(req.userIdSelected);
        } catch (error) {
            return left(new ConfirmAdoptionErrors.UserByIdSelectedNotFound()) as Response;
        }

        const adoptionOrError = AdoptionEntity.create({
            postId: new UniqueEntityID(req.postId),
            userId: new UniqueEntityID(postEntity.createdByUserId.value),
            userIdSelected: new UniqueEntityID(req.userIdSelected)
        });

        if (adoptionOrError.isFailure) {
            return left(Result.fail<AdoptionEntity>(adoptionOrError.error.toString())) as Response;
        }

        const adoptionEntity = adoptionOrError.getValue();

        try {
            await this.adoptionRepository.confirmAdoption(adoptionEntity);

            const state = PostState.create(PostStateType.CLOSE);
            postEntity.adoptedByUserId = userSelected.id;
            postEntity.state = state.getValue();

            await this.postRepository.updatePost(postEntity);
            await this.userRepositoy.deletePostFromInterested(postEntity.id.toString());
            await this.userRepositoy.deletePostFromAllUsersInterestedIn(postEntity.id.toString());
        } catch (err) {
            return left(new GenericAppError.UnexpectedError(err)) as Response;
        }

        const notificationMessage = NotificationMessage.create(
            `${postEntity.createdBy.displayName.value} acepto tu petición`
        );

        const notificationOrError = NotificationEntity.create({
            createdAt: new Date(),
            userId: userSelected.id,
            message: notificationMessage.getValue(),
            isRead: false
        });

        if (notificationOrError.isFailure) {
            return left(
                Result.fail<NotificationEntity>(notificationOrError.error.toString())
            ) as Response;
        }

        const notificationEntity = notificationOrError.getValue();

        try {
            this.mailerRepository.sendEmail(
                userSelected.email.value,
                'Tu peticion de adopción fue aceptada',
                'Parece que alguien acepto tu petición de adopcion. Ingresa a la app!'
            );

            await this.notificationRepository.addNotification(notificationEntity);
            return right(Result.ok<void>());
        } catch (error) {
            return left(new GenericAppError.UnexpectedError(error.message)) as Response;
        }

        return right(Result.ok<void>());
    }
}
