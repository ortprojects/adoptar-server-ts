import { notificationRepositoryImpl } from "../../../notifications/infrastructure/repositories";
import { postRepositoryImpl } from "../../../posts/infrastructure/repositories";
import { mailerRepositoryImpl } from "../../../shared/core/mailer";
import { userRepositoryImpl } from "../../../users/infrastructure/repositories";
import { adoptionRepositoryImpl } from "../../infrastructure/repositories";
import { ConfirmAdoptionController } from "./ConfirmAdoptionController";
import { ConfirmAdoptionUseCase } from "./ConfirmAdoptionUseCase";

const confirmAdoptionUseCase = new ConfirmAdoptionUseCase(postRepositoryImpl, userRepositoryImpl, adoptionRepositoryImpl, mailerRepositoryImpl, notificationRepositoryImpl);
const confirmAdoptionContoller = new ConfirmAdoptionController(confirmAdoptionUseCase);

export { confirmAdoptionUseCase, confirmAdoptionContoller };
