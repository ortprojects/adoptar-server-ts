import { UseCaseError } from '../../../shared/core/UseCaseError';
import { Result } from '../../../shared/core/Result';

export namespace ConfirmAdoptionErrors {
    export class PostByIdNotFound extends Result<UseCaseError> {
        constructor() {
            super(false, {
                message: `Post not found`
            } as UseCaseError);
        }
    }

    export class UserByIdSelectedNotFound extends Result<UseCaseError> {
        constructor() {
            super(false, {
                message: `User selected not found`
            } as UseCaseError);
        }
    }
}
