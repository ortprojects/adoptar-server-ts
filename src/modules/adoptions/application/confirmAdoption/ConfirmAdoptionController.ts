import { BaseController } from "../../../shared/infrastructure/http/models/BaseController";
import { ConfirmAdoptionErrors } from "./ConfirmAdoptionErrors";
import { ConfirmAdoptionUseCase } from "./ConfirmAdoptionUseCase";
import { ConfirmAdoptionDTO } from "./dtos/ConfirmAdoptionDTO";

export class ConfirmAdoptionController extends BaseController {
    private useCase: ConfirmAdoptionUseCase;

    constructor(useCase: ConfirmAdoptionUseCase) {
        super();
        this.useCase = useCase;
    }

    async executeImpl(): Promise<any> {
        const dto: ConfirmAdoptionDTO = this.req.body as ConfirmAdoptionDTO;
        
        try {
            const result = await this.useCase.execute(dto);

            if (result.isLeft()) {
                const error = result.value;

                switch (error.constructor) {
                    case ConfirmAdoptionErrors.PostByIdNotFound:
                        return this.conflict(error.errorValue().message);
                    case ConfirmAdoptionErrors.UserByIdSelectedNotFound:
                        return this.conflict(error.errorValue().message);
                    default:
                        return this.fail(error.errorValue().message);
                }
            } else {
                return this.created(this.res);
            }
        } catch (err) {
            return this.fail(err);
        }
    }
}
