import { FirebaseService } from '../../../shared/core/firebase/FirebaseService';
import { AdoptionRepositoryImpl } from './AdoptionRepositoryImpl';
import { AdoptionRepository } from './AdoptionRepository';

const firestore = FirebaseService.getFirestoreReference();

export const adoptionRepositoryImpl: AdoptionRepository = new AdoptionRepositoryImpl(firestore);
