import { AdoptionEntity } from "../../domain/AdoptionEntity";

export interface AdoptionRepository {
    confirmAdoption(adoptionEntity: AdoptionEntity): Promise<boolean>;
}
