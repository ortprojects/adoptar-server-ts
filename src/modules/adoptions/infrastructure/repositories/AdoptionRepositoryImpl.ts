import { AdoptionEntity } from '../../domain/AdoptionEntity';
import { AdoptionRepository } from './AdoptionRepository';

export class AdoptionRepositoryImpl implements AdoptionRepository {
    private firestore: FirebaseFirestore.Firestore;
    private inAdoptionsCollectionPath = '/inAdoption';
    private adoptedCollection = 'adopted';

    constructor(firestore: FirebaseFirestore.Firestore) {
        this.firestore = firestore;
    }

    async confirmAdoption(adoptionEntity: AdoptionEntity): Promise<boolean> {
        const inAdoptionsCollectionPath = this.firestore.collection(this.inAdoptionsCollectionPath);
        const adoptedCollectionPath = this.firestore.collection(this.adoptedCollection);

        await adoptedCollectionPath
            .doc(adoptionEntity.userIdSelected.toString())
            .collection('/posts')
            .doc(adoptionEntity.postId.toString())
            .create({adopted: true});

        await inAdoptionsCollectionPath
            .doc(adoptionEntity.userId.toString())
            .collection('/posts')
            .doc(adoptionEntity.postId.toString())
            .set({userIdSelected: adoptionEntity.userIdSelected.toString()});

        return true;
    }
}
