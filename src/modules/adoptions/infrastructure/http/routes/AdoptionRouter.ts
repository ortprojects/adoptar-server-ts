import { Request, Response } from 'express';
import express from 'express';
import { RouterPath } from '../../../../shared/infrastructure/http/models/RouterPath';
import { LoggerImpl } from '../../../../shared/infrastructure';
import { confirmAdoptionContoller } from '../../../application/confirmAdoption';

export class AdoptionRouter {
    private static readonly routePath = '/adoptions';

    public static create(): RouterPath {
        LoggerImpl.writeInfoLog('[AdoptionRoute::create] Created adoption routes.');

        const adoptionRouter = express.Router();

        adoptionRouter.post('/', (req: Request, res: Response) => {
            confirmAdoptionContoller.execute(req, res);
        });

        return { router: adoptionRouter, path: this.routePath };
    }
}
