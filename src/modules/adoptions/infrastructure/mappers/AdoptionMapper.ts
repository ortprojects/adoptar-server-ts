import { UniqueEntityID } from '../../../shared/domain/valueObjects/UniqueEntityID';
import { BaseMapper } from '../../../shared/infrastructure/mappers/BaseMapper';
import { UserMapper } from '../../../users/infrastructure/mappers/UserMapper';
import { AdoptionEntity } from '../../domain/AdoptionEntity';

export class AdoptionMapper implements BaseMapper<AdoptionMapper> {
    public static toPersistence(review: AdoptionEntity): any {
        const raw = {
            id: review.id.toString(),
            userId: review.userId.toString()
        };

        return raw;
    }

    // public static toDomain(raw: any, id?: string): AdoptionEntity | null {
    //     const userIdOrError = new UniqueEntityID(raw.userId);

    //     const reviewOrError = AdoptionEntity.create(
    //         {
    //             userIdSelected: userIdOrError
    //         },
    //         new UniqueEntityID(id)
    //     );

    //     return reviewOrError.isSuccess ? reviewOrError.getValue() : null;
    // }

    // public static toJSON(raw: AdoptionEntity): any {
    //     return {
    //         userId: raw.userId.toString()
    //     };
    // }
}
