import { AggregateRoot } from '../../shared/domain/AggregateRoot';
import { UniqueEntityID } from '../../shared/domain/valueObjects/UniqueEntityID';
import { Result } from '../../shared/core/Result';
import { Guard } from '../../shared/core/Guard';

interface AdoptionEntityProps {
    userIdSelected: UniqueEntityID;
    postId: UniqueEntityID;
    userId: UniqueEntityID
}

export class AdoptionEntity extends AggregateRoot<AdoptionEntityProps> {
    get id(): UniqueEntityID {
        return this._id;
    }

    get userId(): UniqueEntityID {
        return this.props.userId;
    }

    get userIdSelected(): UniqueEntityID {
        return this.props.userIdSelected;
    }

    get postId(): UniqueEntityID {
        return this.props.postId;
    }

    private constructor(props: AdoptionEntityProps, id?: UniqueEntityID) {
        super(props, id);
    }

    public static create(props: AdoptionEntityProps, id?: UniqueEntityID): Result<AdoptionEntity> {
        const guardedProps = [
            { argument: props.userIdSelected, argumentName: 'userId' },
            { argument: props.postId, argumentName: 'postId' },
            { argument: props.userId, argumentName: 'userId' },
        ];

        const guardResult = Guard.againstNullOrUndefinedBulk(guardedProps);

        if (!guardResult.succeeded) {
            return Result.fail<AdoptionEntity>(guardResult.message);
        } else {
            const question = new AdoptionEntity(
                {
                    ...props,
                    userIdSelected: props.userIdSelected,
                    postId: props.postId,
                },
                id
            );

            return Result.ok<AdoptionEntity>(question);
        }
    }
}
