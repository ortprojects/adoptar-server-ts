import express from 'express';
import { Application } from 'express';
import { Constant } from './modules/shared/config/Constant';
import { LoggerImpl } from './modules/shared/infrastructure';
import { ApiV1 } from './modules/shared/infrastructure/http/api/ApiV1';

class App {
    public app: Application;
    public port: number;

    constructor(appInit: { port: number; middleWares: any; controllers: any }) {
        this.app = express();
        this.port = appInit.port;
        this.createLogger();
        this.middlewares(appInit.middleWares);
        this.routes();
    }

    private middlewares(middleWares: { forEach: (arg0: (middleWare: any) => void) => void }): void {
        middleWares.forEach((middleWare) => {
            this.app.use(middleWare);
        });
    }

    private routes(): void {
        this.app.use(Constant.API_PATH, ApiV1.create());
    }

    public listen(): void {
        this.app.listen(this.port, () => {
            console.log(`App listening on the http://localhost:${this.port}`);
        });
    }

    public createLogger() {
        new LoggerImpl();
    }
}

export default App;
