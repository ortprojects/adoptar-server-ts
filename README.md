# Adoptar web service

### Install 🔧

_Install packages_

```
npm install
```

### Init development environment

```
npm run start:dev
```
_Check enviroment variables_

### Config enviroment variables

_Create .env file in source project_

```
API_VERSION={versionDeApi}
API_PATH_BASE={pathBase}
PORT=9380
FIREBASE_DATABASE_URL={firebase_database_url}
FIREBASE_STORAGE_BUCKET={firebase_storage_bucket}
FIREBASE_PROJECT_ID={firebase_project_id}
FIREBASE_PRIVATE_KEY={firebase_private_key}
FIREBASE_CLIENT_EMAIL={firebase_client_email}
```